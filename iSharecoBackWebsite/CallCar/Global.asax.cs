﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CallCar
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }

    public static class Helper
    {

        public static string getUrl(string actionName, string controllerName = "")
        {

            Uri uri = HttpContext.Current.Request.Url;

            string baseUrl = uri.Scheme + "://" + uri.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/');

            if (controllerName == "")
            {
                controllerName = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString();
            }
            return baseUrl + "/" + controllerName + "/" + actionName;


        }


        public static string jsonEncode(object aa)
        {
            return JsonConvert.SerializeObject(aa);
        }
        public static dynamic jsonDecode(string text)
        {
            return JsonConvert.DeserializeObject<dynamic>(text);
        }


        public const string PhoneNumber = "01234 567890";

    }


}
