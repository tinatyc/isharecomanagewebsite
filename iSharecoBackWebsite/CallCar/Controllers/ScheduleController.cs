﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Security.Cryptography;

namespace CallCar.Controllers
{
    public class ScheduleController : _AdminController
    {
        // GET: Schedule
        public ActionResult Index()
        {
            return View();
        }
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;

        public JsonResult GetList(int page ,string searchJson = null,  string orderField = "", string orderType = "", int itemPerPage = 10)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                string QSDate = "";

                string Driver = "";



                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        QSDate = search.QSDate;

                        Driver = search.Driver;

                    }
                    catch (Exception e)
                    {

                    }

                }

                DAL.DALDispatch dalr = new DAL.DALDispatch(ConnectionString);
                DAL.DALGeneral mo = new DAL.DALGeneral(ConnectionString);
                int DriverID = 0;
                if (Driver != "")
                {
                    DriverID = mo.GetDriverIDByName(Driver);
                }
                List<OScheduleData> List = dalr.GetScheduleByAll(QSDate, DriverID);

                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;

                List<dynamic> data = new List<dynamic>();

                if (List != null)
                {

                    foreach (OScheduleData or in List)
                    {
                        TotalItem++;

                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                        a.Add("CarNo", or.CarNo.ToString()); //: "CR01E9541", 訂單編號
                        a.Add("CarType", or.CarType.ToString()); //: 100,  會員編號
                        a.Add("DriverID", mo.GetDriversByID(or.DriverID)); //: "O",  出國O,回國 I
                        a.Add("FleetID", mo.GetFleet(or.FleetID).FleetName.ToString()); //: "20170207", 乘車日期
                        a.Add("ScheduleDate", or.ScheduleDate.ToString()); //: "2000", 預約時段
                        a.Add("ScheduleID", or.ScheduleID.ToString()); //: "新北市",  上車城市
                        a.Add("ShiftType", shiftype2name(or.ShiftType.ToString())); //: "新北市",  上車城市


                        data.Add(a);

                    }

                }

                JsonResult.Add("data", data);

                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);



                JsonResult.Add("totalItem", TotalItem);
                JsonResult.Add("pageTotal", PageTotal);
                JsonResult.Add("nowpage", page);
            }
            catch (Exception e)
            {
                e.ToString();
            }


            return Json(JsonResult);

        }

        public string shiftype2name(string code)
        {
            string AAA = "";
            if (code == "0")
                AAA = "全天";
            if (code == "1")
                AAA = "早班";
            if(code =="2")
                AAA = "晚班";

            return AAA;
            
        }

        public ActionResult Item(string id)
        {
            DAL.DALDispatch dalr = new DAL.DALDispatch(ConnectionString);
            OScheduleData R;
            R = dalr.GetScheduleByID(Convert.ToInt32(id));

            DAL.DALGeneral mo = new DAL.DALGeneral(ConnectionString);

            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();


            data.Add("CarNo", R.CarNo.ToString()); //: "CR01E9541", 訂單編號
            data.Add("CarType", R.CarType.ToString()); //: 100,  會員編號
            data.Add("DriverID", mo.GetDriversByID(R.DriverID)); //: "O",  出國O,回國 I
            data.Add("FleetID", mo.GetFleet(R.FleetID).FleetName.ToString()); //: "20170207", 乘車日期
            data.Add("ScheduleDate", R.ScheduleDate.ToString()); //: "2000", 預約時段
            data.Add("ScheduleID", R.ScheduleID.ToString()); //: "新北市",  上車城市

            string dataText = "";

            dataText = Helper.jsonEncode(data);

            ViewBag.dataText = dataText;

            Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();
            dict.Add(shiftype2name(R.ShiftType.ToString()), R.ShiftType.ToString());

            List<SelectListItem> mySelectItemList2 = new List<SelectListItem>();
            if (R.ShiftType.ToString() == "0")
            {
                mySelectItemList2.Add(new SelectListItem()
                { Text = "全天", Value = "0", Selected = true });
                mySelectItemList2.Add(new SelectListItem()
                { Text = "早班", Value = "1", Selected = false });
                mySelectItemList2.Add(new SelectListItem()
                { Text = "晚班", Value = "2", Selected = false });
            }
            else if (R.ShiftType.ToString() == "1")
            {
                mySelectItemList2.Add(new SelectListItem()
                { Text = "全天", Value = "0", Selected = false });
                mySelectItemList2.Add(new SelectListItem()
                { Text = "早班", Value = "1", Selected = true });
                mySelectItemList2.Add(new SelectListItem()
                { Text = "晚班", Value = "2", Selected = false });
            }
            else
            {
                mySelectItemList2.Add(new SelectListItem()
                { Text = "全天", Value = "0", Selected = false });
                mySelectItemList2.Add(new SelectListItem()
                { Text = "早班", Value = "1", Selected = false });
                mySelectItemList2.Add(new SelectListItem()
                { Text = "晚班", Value = "2", Selected = true });
            }

            ViewBag.ShiftType = mySelectItemList2;

            return View();

        }

        public JsonResult ItemEdit(string ScheduleID, string ScheduleDate, string CarNo, string ShiftType, string DriverID, string FleetID, string CarType)
        {

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DAL.DALDispatch dalr = new DAL.DALDispatch(ConnectionString);

            bool rtn = false;
            DAL.DALGeneral mo = new DAL.DALGeneral(ConnectionString);

            rtn = dalr.Upddaily_schedule(ScheduleID, ScheduleDate, CarNo, ShiftType, mo.GetDriverIDByName(DriverID), mo.GetFleet(FleetID).FleetID, CarType);

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }

        }
    }
}