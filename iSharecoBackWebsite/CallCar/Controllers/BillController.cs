﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Security.Cryptography;
using ClosedXML.Excel;
using CallCar.Infrastructure;
using System.Text;
using DbConn;
using MySql.Data.MySqlClient;

namespace CallCar.Controllers
{
    public class BillController : _AdminController
    {
        // GET: Bill
        public ActionResult Index()
        {
            return View();

        }
        public ActionResult Item()
        {
            return View();
        }

        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public JsonResult GetList(int page, string searchJson = null, string orderField = "", string orderType = "", int itemPerPage = 10)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }
            string QSDate = "";
            string QEDate = "";
            string Stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate;
                    QEDate = search.QEDate;
                    Stage = search.Stage;
                    Carteam = search.Carteam;

                }
                catch (Exception e)
                {
                    e.ToString();
                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            RtnStruct r = new RtnStruct();
            BALReservation bal = new BALReservation(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            try
            {
                int fleetid = 0;
                string fstage = "";
                if (Carteam != "")
                    fleetid = balg.GetFleet(Carteam).FleetID;
                if (Stage.IndexOf("成功") != -1)
                    fstage = "1";
                else if (Stage.IndexOf("尚未") != -1)
                    fstage = "0";
                else if (Stage.IndexOf("失敗") != -1)
                    fstage = "X";
                else
                    fstage = "";
                List<OReservation> List = bal.GetReservationByDateAndStage(QSDate, QEDate, fstage, fleetid);
                if (List == null)
                    r.RtnObject = null;
                else
                {
                    List<ReservationFullStruct> rfsList = new List<ReservationFullStruct>();

                    foreach (OReservation or in List)
                    {
                        ReservationFullStruct rfs = new ReservationFullStruct();
                        rfs.bcnt = or.BaggageCnt;
                        rfs.cdatetime = or.OrderTime.ToString("yyyy/MM/dd HH:mm");
                        rfs.coupun = "";// or.Coupon;
                        rfs.credit = "";// or.Credit;
                        rfs.ein = "";//or.EIN;
                        rfs.fdatetime = "";//or.FlightTime.ToString("yyyy/MM/dd HH:mm");
                        rfs.fno = "";//or.FlightNo;
                        rfs.invoice = "";//or.Invoice;
                        rfs.max = "";//or.MaxFlag;
                        rfs.paddress = or.PickupAddress;
                        rfs.pcartype = "";//or.PickCartype;
                        rfs.pcity = or.PickupCity;
                        rfs.pcnt = or.PassengerCnt;
                        rfs.pdistinct = or.PickupDistinct;
                        rfs.pvillage = "";//or.PickupVillage;
                        rfs.rno = or.ReservationNo;
                        rfs.seltype = "";//or.ChooseType;
                        rfs.sertype = or.ServiceType;
                        rfs.stage = or.ProcessStage;
                        rfs.taddress = or.TakeoffAddress;
                        rfs.tcity = or.TakeoffCity;
                        rfs.tdate = or.ServeDate;
                        rfs.tdistinct = or.TakeoffDistinct;
                        rfs.timeseg = or.PreferTime;
                        rfs.trialprice = or.TrailPrice;
                        rfs.tvillage = "";//or.TakeoffVillage;
                        rfs.tlat = or.TakeoffGPS.GetLat();
                        rfs.tlng = or.TakeoffGPS.GetLng();
                        rfs.plat = or.PickupGPS.GetLat();
                        rfs.plng = or.PickupGPS.GetLng();
                        rfs.uid = or.UserID;
                        rfs.plat = or.PickupGPS.GetLat();
                        rfs.plng = or.PickupGPS.GetLng();
                        rfs.tlat = or.TakeoffGPS.GetLat();
                        rfs.tlng = or.TakeoffGPS.GetLng();
                        rfs.stage = or.ProcessStage;
                        rfs.Carteam = balg.GetFleet(or.carteam).FleetName;
                        rfs.realamt = or.realamt;
                        rfsList.Add(rfs);
                    }
                    r.RtnObject = rfsList;
                }
                r.Status = "Success";
            }
            catch (Exception e)
            {
                e.ToString();
            }
            dynamic jsonData = Helper.jsonDecode(JsonConvert.SerializeObject(r));
            string Status = jsonData.Status;
            var RtnObject = jsonData.RtnObject;

            int countSkip = itemPerPage * (page - 1);
            int countTake = itemPerPage;

            int TotalItem = 0;

            //RepStarDateTime = "2016-12-20";
            //RepEndDateTime = "2017-01-04";

            List<dynamic> data = new List<dynamic>();
            var items = RtnObject;

            if (items != null)
            {

                foreach (JObject item in items)
                {
                    TotalItem++;

                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                    //dynamic zzz = Helper.jsonDecode(item);
                    //a.Add("zzz", zzz);

                    a.Add("rno", item.GetValue("rno").ToString()); //:"20170118", //搭乘日期
                    a.Add("cp", item.GetValue("Carteam").ToString()); //:"20170118", //搭乘日期
                    a.Add("money", item.GetValue("trialprice").ToString()); //:"2017/01/18T09:25",//服務時間
                    a.Add("ramt", item.GetValue("realamt").ToString());
                    a.Add("stage", item.GetValue("stage").ToString()); //:"O", //出回國 (I→回國/O→出國)
                    a.Add("tdate", item.GetValue("tdate").ToString()); //:5,//全車人數


                    data.Add(a);
                }

            }

            JsonResult.Add("data", data);


            double PageTotal = (double)TotalItem / itemPerPage;
            PageTotal = Math.Ceiling(PageTotal);

            //PageTotal = 1;

            JsonResult.Add("totalItem", TotalItem);
            JsonResult.Add("pageTotal", PageTotal);
            JsonResult.Add("nowpage", page);
            return Json(JsonResult);

        }

        public JsonResult wtCreditD(string id)
        {
            DAL.DALGeneral ddl = new DAL.DALGeneral(ConnectionString);
            OPay2goResult pdata = new OPay2goResult();
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            if (ddl.Re_Charge(id).ToString() != "")
            {
                OPay2goResult OD = JsonConvert.DeserializeObject<OPay2goResult>(ddl.Re_Charge(id).ToString());
                List<dynamic> data = new List<dynamic>();
                for (int i = 0; i < OD.Result.Count(); i++)
                {

                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                    a.Add("Status", OD.Result[i].Status.ToString()); //:"20170118", //搭乘日期
                    a.Add("Message", OD.Result[i].Message.ToString()); //:"20170118", //搭乘日期

                    data.Add(a);
                }
                JsonResult.Add("data", data);
            }
            else
            {
                JsonResult.Add("data", "nodata");
            }

            return Json(JsonResult);
        }

        [HttpPost]
        public HttpStatusCodeResult CreateExcel(string searchJson)
        {
            XLWorkbook wb = new XLWorkbook(XLEventTracking.Disabled); //create Excel

            string QSDate = "";
            string QEDate = "";
            string Stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate;
                    QEDate = search.QEDate;
                    Stage = search.Stage;
                    Carteam = search.Carteam;

                }
                catch (Exception e)
                {
                    e.ToString();
                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DataTable dt = null;
            RtnStruct r = new RtnStruct();
            //BALReservation bal = new BALReservation(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            try
            {
                int fleetid = 0;
                if (Carteam != "")
                    fleetid = balg.GetFleet(Carteam).FleetID;
                string fstage = "";
                if (Carteam != "")
                    fleetid = balg.GetFleet(Carteam).FleetID;
                if (Stage.IndexOf("成功") != -1)
                    fstage = "1";
                else if (Stage.IndexOf("尚未") != -1)
                    fstage = "0";
                else if (Stage.IndexOf("失敗") != -1)
                    fstage = "X";
                else
                    fstage = "";

                #region 取資料
                StringBuilder sb = new StringBuilder();
                List<MySqlParameter> parms = new List<MySqlParameter>();
                sb.Append("SELECT r.ReservationNo, r.UserID, r.ServiceType, r.TakeDate, r.TimeSegment, r.SelectionType, r.PickCartype, r.FlightNo, r.ScheduleFlightTime, r.PickupCity, ");
                sb.Append("r.PickupDistinct, r.PickupVillage, r.PickupAddress, X(r.PickupGPS) as plng, Y(r.PickupGPS) as plat, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffVillage, r.TakeoffAddress, X(r.TakeoffGPS) as tlng ,Y(r.TakeoffGPS) as tlat , r.PassengerCnt, ");
                sb.Append("r.BaggageCnt, r.MaxFlag, r.Price, r.Coupon, r.InvoiceData, r.CreditData, r.ProcessStage, r.InvoiceNum, r.CreateTime, r.UpdTime,r.PassengerName,r.PassengerPhone , s.DispatchNo, d.DispatchNo, d.ReservationNo, s.DriverID, s.FleetID, rcl.ChargeResult, rcl.RealAmt, ");
                sb.Append("(select username from user_info where user_info.userid = s.DriverID) as 'DriverName' ");
                sb.Append("FROM dispatch_sheet s,dispatch_reservation d,reservation_sheet r ,reservation_charge_list rcl where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo and r.ReservationNo = rcl.ReservationNo   and r.TakeDate between  @sdate AND @edate");
                if (fstage != "")
                {
                    sb.Append(" AND rcl.ChargeResult = @ChargeResult");
                    parms.Add(new MySqlParameter("@ChargeResult", fstage));
                }
                if (fleetid != 0)
                {
                    sb.Append(" AND s.FleetID = @FleetID");
                    parms.Add(new MySqlParameter("@FleetID", fleetid));
                }
                parms.Add(new MySqlParameter("@sdate", QSDate));
                parms.Add(new MySqlParameter("@edate", QEDate));
                DataTable dtData = new DataTable();
                List<OReservation> List = new List<OReservation>();
                try
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                    {
                        dtData = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                if (dtData.Rows.Count > 0)
                {
                    try
                    {
                        for (int i = 0; i < dtData.Rows.Count; i++)
                        {
                            OReservation or = new OReservation();
                            or.BaggageCnt = Convert.ToInt32(dtData.Rows[i]["BaggageCnt"]);
                            or.ChooseType = Convert.ToString(dtData.Rows[i]["SelectionType"]);
                            or.Coupon = Convert.ToString(dtData.Rows[i]["Coupon"]);
                            or.Credit = Convert.ToString(dtData.Rows[i]["CreditData"]);
                            if (dtData.Rows[i]["ScheduleFlightTime"] != DBNull.Value)
                            {
                                or.FlightDate = Convert.ToDateTime(dtData.Rows[i]["ScheduleFlightTime"]).ToString("yyyyMMdd");
                                or.FlightTime = Convert.ToDateTime(dtData.Rows[i]["ScheduleFlightTime"]);
                                or.FlightNo = Convert.ToString(dtData.Rows[i]["FlightNo"]);
                            }
                            else
                            {
                                or.FlightDate = "";
                                or.FlightNo = "";
                                or.FlightTime = Convert.ToDateTime("1911-01-01 00:00:00");
                            }

                            or.Invoice = Convert.ToString(dtData.Rows[i]["InvoiceData"]);
                            or.MaxFlag = Convert.ToString(dtData.Rows[i]["MaxFlag"]);
                            or.OrderTime = Convert.ToDateTime(dtData.Rows[i]["CreateTime"]);
                            or.PassengerCnt = Convert.ToInt32(dtData.Rows[i]["PassengerCnt"]);
                            or.PickCartype = Convert.ToString(dtData.Rows[i]["PickCartype"]);
                            or.PickupAddress = Convert.ToString(dtData.Rows[i]["PickupAddress"]);
                            or.PickupCity = Convert.ToString(dtData.Rows[i]["PickupCity"]);
                            or.PickupDistinct = Convert.ToString(dtData.Rows[i]["PickupDistinct"]);
                            or.PickupVillage = Convert.ToString(dtData.Rows[i]["PickupVillage"]);
                            or.PreferTime = Convert.ToString(dtData.Rows[i]["TimeSegment"]);
                            or.ProcessStage = Convert.ToString(dtData.Rows[i]["ChargeResult"]);
                            or.ReservationNo = Convert.ToString(dtData.Rows[i]["ReservationNo"]);
                            or.ServeDate = Convert.ToString(dtData.Rows[i]["TakeDate"]);
                            or.ServiceType = Convert.ToString(dtData.Rows[i]["ServiceType"]);
                            or.TakeoffAddress = Convert.ToString(dtData.Rows[i]["TakeoffAddress"]);
                            or.TakeoffCity = Convert.ToString(dtData.Rows[i]["TakeoffCity"]);
                            or.TakeoffDistinct = Convert.ToString(dtData.Rows[i]["TakeoffDistinct"]);
                            or.TakeoffVillage = Convert.ToString(dtData.Rows[i]["TakeoffVillage"]);
                            or.UserID = Convert.ToInt32(dtData.Rows[i]["UserID"]);
                            or.PassengerName = Convert.ToString(dtData.Rows[i]["PassengerName"]);
                            or.PassengerPhone = Convert.ToString(dtData.Rows[i]["PassengerPhone"]);
                            or.EIN = Convert.ToString(dtData.Rows[i]["InvoiceNum"]);
                            or.TakeoffGPS = new OSpatialGPS(Convert.ToDouble(dtData.Rows[i]["tlat"]), Convert.ToDouble(dtData.Rows[i]["tlng"]));
                            or.PickupGPS = new OSpatialGPS(Convert.ToDouble(dtData.Rows[i]["plat"]), Convert.ToDouble(dtData.Rows[i]["plng"]));
                            or.carteam = Convert.ToInt32(dtData.Rows[i]["FleetID"]);
                            or.TrailPrice = Convert.ToInt32(dtData.Rows[i]["Price"]);
                            or.realamt = Convert.ToInt32(dtData.Rows[i]["RealAmt"]);
                            List.Add(or);
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                }


                #endregion
                //List<OReservation> List = bal.GetReservationByDateAndStage(QSDate, QEDate, fstage, fleetid);
                if (List != null)
                {
                    dt = new DataTable();
                    dt.Columns.Add("項次");
                    dt.Columns.Add("艾雪單號");
                    dt.Columns.Add("乘車日期");
                    dt.Columns.Add("預約時段");
                    dt.Columns.Add("出回國");
                    dt.Columns.Add("乘車人姓名");
                    dt.Columns.Add("乘車人電話");
                    dt.Columns.Add("人數");
                    dt.Columns.Add("行李數");
                    dt.Columns.Add("優惠碼");
                    dt.Columns.Add("收件人");

                    dt.Columns.Add("所屬年月");
                    dt.Columns.Add("發票號碼");
                    dt.Columns.Add("製單人員");
                    dt.Columns.Add("類別代號");
                    dt.Columns.Add("課稅別");
                    dt.Columns.Add("銷售金額");
                    dt.Columns.Add("實收金額");
                    dt.Columns.Add("發票日期");
                    dt.Columns.Add("發票種類");
                    dt.Columns.Add("統一編號");
                    dt.Columns.Add("寄送地址");

                    int i = 1;
                    foreach (OReservation or in List)
                    {
                        DataRow dataRow = dt.NewRow();

                        dataRow["項次"] = i.ToString();
                        dataRow["艾雪單號"] = or.ReservationNo;
                        dataRow["乘車日期"] = or.ServeDate;
                        dataRow["預約時段"] = or.PreferTime;
                        dataRow["出回國"] = or.ServiceType == "I" ? "回國" : "出國";
                        dataRow["乘車人姓名"] = or.PassengerName;
                        dataRow["乘車人電話"] = or.PassengerPhone;
                        dataRow["人數"] = or.PassengerCnt.ToString();
                        dataRow["行李數"] = or.BaggageCnt.ToString();
                        dataRow["優惠碼"] = or.Coupon;
                        dataRow["收件人"] = or.PassengerName;


                        dataRow["所屬年月"] = DateTime.Now.ToString("yyyyMM");
                        dataRow["發票號碼"] = "";
                        dataRow["製單人員"] = @Session["userName"].ToString();
                        dataRow["類別代號"] = @Session["userID"].ToString();
                        dataRow["課稅別"] = "1";
                        //dataRow["發票號碼"] = or.EIN;
                        dataRow["銷售金額"] = or.TrailPrice;
                        dataRow["實收金額"] = or.realamt;
                        dataRow["發票日期"] = DateTime.Now.ToShortDateString();
                        if (or.Invoice != "")
                        {
                            dynamic InvoiceData = JObject.Parse(or.Invoice);

                            if (InvoiceData.EIN != "")
                                dataRow["發票種類"] = "三聯式";
                            else
                                dataRow["發票種類"] = "二聯式";

                            dataRow["統一編號"] = InvoiceData.EIN;
                            dataRow["寄送地址"] = InvoiceData.InvoiceAddress;
                        }
                        else
                        {
                            dataRow["發票種類"] = "捐贈";
                            dataRow["統一編號"] = "";
                            dataRow["寄送地址"] = "";
                        }

                        dt.Rows.Add(dataRow);
                        i++;
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            wb.AddWorksheet(dt, "Sheet1");
            if (wb != null)
            {
                Session["ExcelResult"] = wb;
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

        }

        [HttpGet]
        public FileContentResult ExcelResult(string reportHeader) //it's your data passed to controller
        {

            byte[] fileBytes = GetExcel((XLWorkbook)Session["ExcelResult"]);
            var exportFileName = string.Concat(
                "Report2Invoice_",
                DateTime.Now.ToString("yyyyMMdd"),
                ".xlsx");
            return File(fileBytes, MediaTypeNames.Application.Octet, exportFileName);
        }
        public static byte[] GetExcel(XLWorkbook wb)
        {
            using (var ms = new MemoryStream())
            {
                wb.SaveAs(ms);
                return ms.ToArray();
            }
        }
        public JsonResult Go2Pay(string paydata, string DATE, string amt, string memo)//授權碼，有效日期更新
        {
            string[] sArray = paydata.Split(',');
            BALReservation bal = new BALReservation(ConnectionString);
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            List<dynamic> data = new List<dynamic>();
            foreach (string i in sArray)
            {
                if (i != "")
                {
                    //Go2test(i, DATE, amt, memo);
                    // OReservation R = bal.GetReservationByNo(i);
                    // if (R != null)
                    // {
                    //dynamic search = JObject.Parse(R.Credit);
                    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                    a.Add("Message", Go2Credit(i, DATE, amt, memo));

                    data.Add(a);

                    //}

                }
            }
            JsonResult.Add("data", data);

            return Json(JsonResult);

        }
        public string Go2Credit(string SNO, string DATE, string amt, string memo)//使用授權碼退款
        {
            DAL.DALGeneral ddl = new DAL.DALGeneral(ConnectionString);
            OPay2goResult pdata = new OPay2goResult();
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            string nowCData = ddl.Re_Charge_Data(SNO).ToString();
            BALReservation bal = new BALReservation(ConnectionString);


            OReservation R = bal.GetReservationByNo(SNO);
            if (nowCData.ToString() != "")
            {
                Charge_Data_S OD = JsonConvert.DeserializeObject<Charge_Data_S>(nowCData.ToString());
                List<dynamic> data = new List<dynamic>();
                try
                {
                    string HashKey = "FMJKUkbQ6pmh7bJyd1iOfRsmJyR0Neys";
                    string HashIV = "0hdVCpLJP81WjLvx";

                    WebRequest tRequest;
                    tRequest = WebRequest.Create("https://core.spgateway.com/API/CreditCard/Close");
                    tRequest.Method = "post";
                    tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
                    string Version = "1.0";
                    string postData = "MerchantID_=MS31451783&PostData_=" + EncryptAES256("RespondType=JSON&TimeStamp=" + DATE + "&Version=" + Version + "&MerchantOrderNo=" + OD.MerchantOrderNo + "&Amt=" + amt + "&IndexType=2&TradeNo=" + OD.TradeNo + "&CloseType=2", HashKey, HashIV);

                    Console.WriteLine(postData);
                    Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    tRequest.ContentLength = byteArray.Length;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    Stream dataStream = tRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    WebResponse tResponse = tRequest.GetResponse();

                    dataStream = tResponse.GetResponseStream();

                    StreamReader tReader = new StreamReader(dataStream);

                    String sResponseFromServer = tReader.ReadToEnd();


                    tReader.Close();
                    dataStream.Close();
                    tResponse.Close();
                    dynamic jsonData = Helper.jsonDecode(sResponseFromServer);
                    if (jsonData.Status == "SUCCESS")
                    {
                        DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);
                        string wtfMemo = dalr.getMemo(SNO);
                        int nowMpney = R.TrailPrice - Convert.ToInt16(amt);
                        string reMemo = DateTime.Now.ToString() + " " + getSession("userName") + " 金額:" + nowMpney + "退款金額: " + amt + " ,原始金額:" + R.TrailPrice;
                        if (wtfMemo != "")
                        {
                            string nwtfMemo = wtfMemo + Environment.NewLine + reMemo;
                            dalr.UpReservation_charge_list(SNO, nowMpney, nwtfMemo);
                        }
                        else
                        {
                            dalr.UpReservation_charge_list(SNO, nowMpney, reMemo);
                        }
                        return "SUCCESS";
                    }
                    else
                    {

                        return jsonData.Message.ToString();
                    }

                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {

            }



            return "無訂單資料";

        }
        public string EncryptAES256(string source, string sSecretKey, string iv)//加密
        {
            byte[] sourceBytes = AddPKCS7Padding(Encoding.UTF8.GetBytes(source),
            32);
            var aes = new RijndaelManaged();
            aes.Key = Encoding.UTF8.GetBytes(sSecretKey);
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.None;
            ICryptoTransform transform = aes.CreateEncryptor();
            return ByteArrayToHex(transform.TransformFinalBlock(sourceBytes, 0,
            sourceBytes.Length)).ToLower();
        }
        private static byte[] AddPKCS7Padding(byte[] data, int iBlockSize)
        {
            int iLength = data.Length;
            byte cPadding = (byte)(iBlockSize - (iLength % iBlockSize));
            var output = new byte[iLength + cPadding];
            Buffer.BlockCopy(data, 0, output, 0, iLength);
            for (var i = iLength; i < output.Length; i++)
                output[i] = (byte)cPadding;
            return output;
        }
        private static string ByteArrayToHex(byte[] barray)
        {
            char[] c = new char[barray.Length * 2];
            byte b;
            for (int i = 0; i < barray.Length; ++i)
            {
                b = ((byte)(barray[i] >> 4));
                c[i * 2] = (char)(b > 9 ? b + 0x37 : b + 0x30);
                b = ((byte)(barray[i] & 0xF));
                c[i * 2 + 1] = (char)(b > 9 ? b + 0x37 : b + 0x30);
            }
            return new string(c);
        }

        public void Go2test(string SNO, string DATE, string amt, string memo)//測試退款前要先請款
        {
            DAL.DALGeneral ddl = new DAL.DALGeneral(ConnectionString);
            OPay2goResult pdata = new OPay2goResult();
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            if (ddl.Re_Charge_Data(SNO).ToString() != "")
            {
                Charge_Data_S OD = JsonConvert.DeserializeObject<Charge_Data_S>(ddl.Re_Charge_Data(SNO).ToString());
                List<dynamic> data = new List<dynamic>();
                try
                {
                    string HashKey = "ax9hhIc6W3oOCEn0GzxzMW7zwKKczmNJ";
                    string HashIV = "UW1bFeiChGrTlOU1";

                    WebRequest tRequest;
                    tRequest = WebRequest.Create("https://ccore.spgateway.com/API/CreditCard/Close");
                    tRequest.Method = "post";
                    tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
                    string Version = "1.0";
                    string postData = "MerchantID_=MS3446346&PostData_=" + EncryptAES256("RespondType=JSON&TimeStamp=" + DATE + "&Version=" + Version + "&MerchantOrderNo=" + OD.MerchantOrderNo + "&Amt=" + amt + "&IndexType=2&TradeNo=" + OD.TradeNo + "&CloseType=1", HashKey, HashIV);

                    Console.WriteLine(postData);

                    Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    tRequest.ContentLength = byteArray.Length;

                    Stream dataStream = tRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    WebResponse tResponse = tRequest.GetResponse();

                    dataStream = tResponse.GetResponseStream();

                    StreamReader tReader = new StreamReader(dataStream);

                    String sResponseFromServer = tReader.ReadToEnd();


                    tReader.Close();
                    dataStream.Close();
                    tResponse.Close();
                    dynamic jsonData = Helper.jsonDecode(sResponseFromServer);

                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
            else
            {

            }

        }

        /*public void go2Excel(string searchJson = null)
{


    string QSDate = "";
    string QEDate = "";
    string Stage = "";
    string Carteam = "";

    if (searchJson != null)
    {
        try
        {

            dynamic search = JObject.Parse(searchJson);

            QSDate = search.QSDate;
            QEDate = search.QEDate;
            Stage = search.Stage;
            Carteam = search.Carteam;

        }
        catch (Exception e)
        {
            e.ToString();
        }

    }
    Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
    DataTable dt = null;
    RtnStruct r = new RtnStruct();
    BALReservation bal = new BALReservation(ConnectionString);
    BALGeneral balg = new BALGeneral(ConnectionString);
    try
    {
        int fleetid = 0;
        if (Carteam != "")
            fleetid = balg.GetFleet(Carteam).FleetID;
        List<OReservation> List = bal.GetReservationByDateAndStage(QSDate, QEDate, Stage, fleetid);
        if (List != null)
        {
            dt = new DataTable();
            dt.Columns.Add("所屬年月");
            dt.Columns.Add("發票號碼");
            dt.Columns.Add("製單人員");
            dt.Columns.Add("類別代號");
            dt.Columns.Add("課稅別");
            dt.Columns.Add("銷售金額");
            dt.Columns.Add("發票日期");
            dt.Columns.Add("統一編號");
            dt.Columns.Add("寄送地址");

            foreach (OReservation or in List)
            {
                DataRow dataRow = dt.NewRow();
                dataRow["所屬年月"] = DateTime.Now.ToShortDateString();
                dataRow["發票號碼"] = "EF59066900";
                dataRow["製單人員"] = "BY";
                dataRow["類別代號"] = "51";
                dataRow["課稅別"] = "1";
                dataRow["銷售金額"] = or.TrailPrice;
                dataRow["發票日期"] = DateTime.Now.ToShortDateString();
                dataRow["統一編號"] = "";
                dataRow["寄送地址"] = "";
                dt.Rows.Add(dataRow);
            }
        }
    }
    catch (Exception e)
    {
        e.ToString();
    }
    string filePath = @"D:\testReport.xlsx";
    XLWorkbook workbook = new XLWorkbook();
    workbook.AddWorksheet(dt, "Sheet1");
    /*workbook.SaveAs(filePath);
    workbook.Dispose();
    string myName = Server.UrlEncode("Sample.xls");
    MemoryStream stream = GetStream(workbook);
    //Response.ClearHeaders();
    Response.Clear();
    //Response.ClearContent();
    //Response.Expires = 0;

    Response.Buffer = true;
    //Response.BufferOutput = true;
    Response.ContentType = "application/vnd.ms-excel";
    Response.AddHeader("content-disposition", "attachment; filename=" + myName);
    Response.BinaryWrite(stream.ToArray());
    //Response.Flush();
    Response.End();
    //workbook.Dispose();

    //return Json(JsonResult);
}*/

        /*public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }*/

        /*public ActionResult SendFile(string searchJson = null)
        {

            var exportFileName = string.Concat(
                "TaiwanZipCode_",
                DateTime.Now.ToString("yyyyMMddHHmmss"),
                ".xlsx");
            string QSDate = "";
            string QEDate = "";
            string Stage = "";
            string Carteam = "";

            if (searchJson != null)
            {
                try
                {

                    dynamic search = JObject.Parse(searchJson);

                    QSDate = search.QSDate;
                    QEDate = search.QEDate;
                    Stage = search.Stage;
                    Carteam = search.Carteam;

                }
                catch (Exception e)
                {
                    e.ToString();
                }

            }
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DataTable dt = null;
            RtnStruct r = new RtnStruct();
            BALReservation bal = new BALReservation(ConnectionString);
            BALGeneral balg = new BALGeneral(ConnectionString);
            try
            {
                int fleetid = 0;
                if (Carteam != "")
                    fleetid = balg.GetFleet(Carteam).FleetID;
                List<OReservation> List = bal.GetReservationByDateAndStage(QSDate, QEDate, Stage, fleetid);
                if (List != null)
                {
                    dt = new DataTable();
                    dt.Columns.Add("所屬年月");
                    dt.Columns.Add("發票號碼");
                    dt.Columns.Add("製單人員");
                    dt.Columns.Add("類別代號");
                    dt.Columns.Add("課稅別");
                    dt.Columns.Add("銷售金額");
                    dt.Columns.Add("發票日期");
                    dt.Columns.Add("統一編號");
                    dt.Columns.Add("寄送地址");

                    foreach (OReservation or in List)
                    {
                        DataRow dataRow = dt.NewRow();
                        dataRow["所屬年月"] = DateTime.Now.ToShortDateString();
                        dataRow["發票號碼"] = "EF59066900";
                        dataRow["製單人員"] = "BY";
                        dataRow["類別代號"] = "51";
                        dataRow["課稅別"] = "1";
                        dataRow["銷售金額"] = or.TrailPrice;
                        dataRow["發票日期"] = DateTime.Now.ToShortDateString();
                        dataRow["統一編號"] = "";
                        dataRow["寄送地址"] = "";
                        dt.Rows.Add(dataRow);
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return new ExportExcelResult
            {
                SheetName = "臺灣郵遞區號",
                FileName = exportFileName,
                ExportData = dt
            };
        }*/
    }
}