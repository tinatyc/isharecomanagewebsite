﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using CallCar.BAL;
using CallCar.Models;
using Newtonsoft.Json;
namespace CallCar.Controllers {
    public class LoginController : _BaseController {
        public ActionResult Index() {
            return View("Index", "_LayoutEmpty");
        }
        public ActionResult reLog()
        {
            setSession("isLogin", false);
            return View("Index", "_LayoutEmpty");
        }
        public ActionResult editPw()
        {
            return View("editPw", "_LayoutEmpty");

        }
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        [HttpPost]
        //public JsonResult GetList(int page = 1, int itemPerPage = 20)
        public JsonResult LoginDo(string username, string password) {


            string acc = username;
            string pw = password;


            Dictionary<string, dynamic> dataParameters = new Dictionary<string, dynamic>();


            dataParameters.Add("acc", acc);
            dataParameters.Add("pw", pw);
            string dataString = Helper.jsonEncode(dataParameters);
            RtnStruct r = new RtnStruct();
            //string url = Constant.apiBaseUrl + "vAccPW?data=" + dataString;
            string url = Constant.apiBaseUrl + "vAccPW";
            BALEmployee bal = new BALEmployee(ConnectionString);
            EmployeInfoStruct eis = bal.ChkAccPWandGetInfoBackWeb(dataString);
            if (eis != null)
                r.RtnObject = eis;
            else
                r.RtnObject = "False";
            r.Status = "Success";
           string responseText =  JsonConvert.SerializeObject(r);
            //string responseText = sendHttpRequest(url);
            //string responseText = sendPOST(url, dataString);
            bool isLogin = false;

            try {

                dynamic jsonData = Helper.jsonDecode(responseText);
                dynamic RtnObject = jsonData.RtnObject;


                if (RtnObject.active == 1) {
                    isLogin = true;

                }
                if (isLogin) {

                    /*
                    string id = jsonData.id; //:"20160021", 
                    string pw = jsonData.pw; //:"abcdefgth", //密碼明碼
                    string cid = jsonData.cid; //:"J123569751", //身分證字號
                    string empname = jsonData.empname; //:"王小明", //姓名
                    string phone1 = jsonData.phone1; //:"09XXXXXXXX", //手機
                    string phone2 = jsonData.phone2; //:"0227935546",//市話
                    string role = jsonData.role; //:"D", //角色 司機：D 管理人員：M 帳務：A
                    string company = jsonData.company; //:"84177282", //公司/車隊統編
                    string city = jsonData.city; //:"台北市", //所在地 城市
                    string distinct = jsonData.distinct; //:"北投區", //所在地 區名
                    string address = jsonData.address; //:"承德路七段400號", //所在地 地址
                    string active = jsonData.active; //:"1", //活動帳號 0→停用帳號
                    */

                    setSession("userID", RtnObject.id);
                    setSession("cid", RtnObject.cid);
                    setSession("userName", RtnObject.empname);

                    if (RtnObject.role.ToString() == "S")
                    {
                        setSession("userRole", "系統管理員");
                        setSession("uR", "S");
                    }
                    else if (RtnObject.role.ToString() == "D")
                    {
                        setSession("userRole", "司機");
                            setSession("uR", "D");
                    }
                    else if (RtnObject.role.ToString() == "M")
                    {
                        setSession("userRole", "管理者");
                        setSession("uR", "M");
                    }
                    else if (RtnObject.role.ToString() == "C")
                    {
                        setSession("userRole", "客服");
                        setSession("uR", "C");
                    }
                    else if (RtnObject.role.ToString() == "A")
                    {
                        setSession("userRole", "財務人員");
                        setSession("uR", "A");
                    }
                    else if (RtnObject.role.ToString() == "R")
                    {
                        setSession("userRole", "調度");
                        setSession("uR", "R");
                    }
                    else
                    {
                        setSession("userRole", RtnObject.role);
                        setSession("uR", "unKnow");
                    }
                        
                    setSession("userEmail", RtnObject.email);
                    //setSession("userName", userName);

                    setSession("isLogin", true);
                }

            } catch (Exception e) {

            }


            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            JsonResult.Add("result", isLogin);

            return Json(JsonResult);

        }



        public ActionResult LogoutDo() {

            setSession("isLogin", false);


            return RedirectToAction("Index", "Login");


        }

        [HttpPost]
        //public JsonResult GetList(int page = 1, int itemPerPage = 20)
        public JsonResult PwChange(string username, string password, string passwordN)
        {


            string acc = username;
            string pw = password;
            string pwNew = passwordN;

            Dictionary<string, dynamic> dataParameters = new Dictionary<string, dynamic>();


            dataParameters.Add("acc", acc);
            dataParameters.Add("pw", pw);
            string dataString = Helper.jsonEncode(dataParameters);

            //string url = Constant.apiBaseUrl + "vAccPW?data=" + dataString;
            string url = Constant.apiBaseUrl + "vAccPW";


            //string responseText = sendHttpRequest(url);
            string responseText = sendPOST(url, dataString);
            bool isLogin = false;
            bool rtn = false;
            try
            {

                dynamic jsonData = Helper.jsonDecode(responseText);
                dynamic RtnObject = jsonData.RtnObject;


                if (RtnObject.active == 1)
                {
                    isLogin = true;

                }
                if (isLogin)
                {

                    DAL.DALUserAccount dalr = new DAL.DALUserAccount(ConnectionString);
                    rtn = dalr.ChangePassword(acc, pwNew);

                }

            }
            catch (Exception e)
            {

            }


            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            JsonResult.Add("result", rtn);

            return Json(JsonResult);

        }


    }
}