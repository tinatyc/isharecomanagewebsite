﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbConn;
using System.Data;
using CallCar.Models;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using CallCar.Common;
using System.Security.Cryptography;

namespace CallCar.DAL
{
    public class CallcarDAL
    {
        protected string _ConnectionString = string.Empty;
        public CallcarDAL(string conn)
        {
            this._ConnectionString = conn;
        }
    }
    /// <summary>
    /// 內部人員的DAL
    /// </summary>
    public class DALEmployee : CallcarDAL
    {

        public DALEmployee(string conn)
            : base(conn)
        {
        }

        public bool AddAccount(int uid, string pw, string Name, string cid, string phone1, string phone2, string role, string cno, string fno, string city, string distinct, string address)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO user_info (UserID, Password, UserName, CitizenID, MobilePhone, HomePhone, UserRole, CompanyNo,FleetID, City, DistinctName, Address)");
            sb.Append("VALUES (@UserID, @Password,@UserName , @CitizenID, @MobilePhone, @HomePhone, @UserRole,@CompanyNo, @FleetID , @City, @Distinct, @Address)");
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("@UserID", uid));
            parms.Add(new MySqlParameter("@Password", Common.MD5Cryptography.Encrypt(pw)));
            parms.Add(new MySqlParameter("@UserName", Name));
            parms.Add(new MySqlParameter("@CitizenID", cid));
            parms.Add(new MySqlParameter("@MobilePhone", phone1));
            parms.Add(new MySqlParameter("@HomePhone", phone2));
            parms.Add(new MySqlParameter("@CompanyNo", cno));
            parms.Add(new MySqlParameter("@UserRole", role));
            parms.Add(new MySqlParameter("@FleetID", fno));
            parms.Add(new MySqlParameter("@City", city));
            parms.Add(new MySqlParameter("@Distinct", distinct));
            parms.Add(new MySqlParameter("@Address", address));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                try
                {
                    return conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public string GetPassword(int Account)
        {
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                DateTime today = DateTime.Now;

                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select Password from user_info  where UserID = @ID ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@ID", Account));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                    return Convert.ToString(dt.Rows[0]["Password"]);
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OEmployee GetEmployeeInfoByID(int ID)
        {
            OEmployee emp = new OEmployee();
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                DateTime today = DateTime.Now;

                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID, UserName, MobilePhone, HomePhone, UserRole,CompanyNo, FleetID, City, DistinctName, Address,ActiveFlag  from user_info  where UserID = @id  And ActiveFlag = true ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@id", ID));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    emp.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    emp.Name = Convert.ToString(dt.Rows[0]["UserName"]);
                    emp.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);
                    emp.HomePhone = Convert.ToString(dt.Rows[0]["HomePhone"]);
                    emp.Role = Convert.ToString(dt.Rows[0]["UserRole"]);
                    emp.CompanyNo = Convert.ToString(dt.Rows[0]["CompanyNo"]);
                    emp.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                    emp.City = Convert.ToString(dt.Rows[0]["City"]);
                    emp.Distinct = Convert.ToString(dt.Rows[0]["DistinctName"]);
                    emp.Address = Convert.ToString(dt.Rows[0]["Address"]);
                    emp.Active = Convert.ToString(dt.Rows[0]["ActiveFlag"]);


                    return emp;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int GetEmployeeIDByEmail(string Email)
        {
            OEmployee oEmployee = new OEmployee();
            int result;
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                DataTable dataTable = new DataTable();
                DateTime now = DateTime.Now;
                using (dbConnectionMySQL dbConnectionMySQL = new dbConnectionMySQL(this._ConnectionString))
                {
                    stringBuilder.Append("select UserID From user_info where Email = @mail ");
                    List<MySqlParameter> list = new List<MySqlParameter>();
                    list.Add(new MySqlParameter("@mail", Email));
                    dataTable = dbConnectionMySQL.executeSelectQuery(stringBuilder.ToString(), list.ToArray());
                }
                if (dataTable.Rows.Count > 0)
                {
                    result = Convert.ToInt32(dataTable.Rows[0]["UserID"]);
                }
                else
                {
                    result = 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }
        public List<OEmployee> GetEmployeeInfo()
        {


            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            List<OEmployee> empList = new List<OEmployee>();
            sb.Append("select UserID, UserName, MobilePhone, HomePhone, UserRole,CompanyNo, FleetID, City, DistinctName, Address,ActiveFlag  from user_info   ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            try
            {

                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OEmployee emp = new OEmployee();
                    emp.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    emp.Name = Convert.ToString(dt.Rows[0]["UserName"]);
                    emp.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);
                    emp.HomePhone = Convert.ToString(dt.Rows[0]["HomePhone"]);
                    emp.Role = Convert.ToString(dt.Rows[0]["UserRole"]);
                    emp.CompanyNo = Convert.ToString(dt.Rows[0]["CompanyNo"]);
                    emp.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                    emp.City = Convert.ToString(dt.Rows[0]["City"]);
                    emp.Distinct = Convert.ToString(dt.Rows[0]["DistinctName"]);
                    emp.Address = Convert.ToString(dt.Rows[0]["Address"]);
                    emp.Active = Convert.ToString(dt.Rows[0]["ActiveFlag"]);

                    empList.Add(emp);
                }
            }
            else
                return null;

            return empList;

        }

        public List<OEmployee> GetEmployeeByCompany(string CompanyNo)
        {
            List<OEmployee> List = new List<OEmployee>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT UserID, UserName, CitizenID, MobilePhone, HomePhone, UserRole, CompanyNo, FleetID, ShiftType, City, DistinctName, Address, ActiveFlag, CreateTime, UpdTime ");
            sb.Append("FROM user_info ");
            sb.Append("where CompanyNo = @no");

            parms.Add(new MySqlParameter("@no", CompanyNo));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OEmployee emp = new OEmployee();
                    emp.ID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    emp.Name = Convert.ToString(dt.Rows[i]["UserName"]);
                    emp.MobilePhone = Convert.ToString(dt.Rows[i]["MobilePhone"]);
                    emp.HomePhone = Convert.ToString(dt.Rows[i]["HomePhone"]);
                    emp.Role = Convert.ToString(dt.Rows[i]["UserRole"]);
                    emp.CompanyNo = Convert.ToString(dt.Rows[i]["CompanyNo"]);
                    emp.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    emp.City = Convert.ToString(dt.Rows[i]["City"]);
                    emp.Distinct = Convert.ToString(dt.Rows[i]["DistinctName"]);
                    emp.Address = Convert.ToString(dt.Rows[i]["Address"]);
                    emp.Active = Convert.ToString(dt.Rows[i]["ActiveFlag"]);
                    List.Add(emp);
                }

                return List;
            }
            else
                return null;
        }

        public string SetEmployeStatus(int uid, string oldStatus, string newStatus)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("update user_info set activeflag = @new where UserID = @UserID and activeflag = @old");
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("@UserID", uid));
            parms.Add(new MySqlParameter("@new", newStatus));
            parms.Add(new MySqlParameter("@old", oldStatus));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeUpdateQuery(sb.ToString(), parms.ToArray()))
                        return newStatus;
                    else
                        throw new Exception("Update Error");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace);
            }
        }
    }


    /// <summary>
    /// 會員帳戶的DAL
    /// </summary>
    public class DALUserAccount : CallcarDAL
    {
        public DALUserAccount(string ConnectionString)
            : base(ConnectionString)
        {
        }

        public int AddAccount(string Source, string SourceID, string Email, string FName, string LName, string MName, int Gender, string MobilePhone, string NickName, string Password)
        {
            int id = 0;
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("INSERT INTO account ");
            sb.Append("(UserID,Source,SourceID,Email,NickName,FirstName,LastName,MidName,Gender,MobilePhone,Password) ");
            sb.Append("VALUES ");
            sb.Append("(@UserID,@Source,@SourceID,@Email,@NickName,@FirstName,@LastName,@MidName,@Gender,@mphone,@Password)  ");

            id = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            parms.Add(new MySqlParameter("@UserID", id));
            parms.Add(new MySqlParameter("@Source", Source));
            parms.Add(new MySqlParameter("@SourceID", SourceID));
            parms.Add(new MySqlParameter("@Email", Email));
            parms.Add(new MySqlParameter("@NickName", NickName));
            parms.Add(new MySqlParameter("@FirstName", FName));
            parms.Add(new MySqlParameter("@LastName", LName));
            parms.Add(new MySqlParameter("@MidName", MName));
            parms.Add(new MySqlParameter("@Gender", Gender));
            parms.Add(new MySqlParameter("@mphone", MobilePhone));
            parms.Add(new MySqlParameter("@Password", Password));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    if (conn.executeInsertQuery(sb.ToString(), parms.ToArray()))
                        return id;
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                //  trn.Rollback();
                throw new Exception(ex.Message);
            }


        }

        public bool UpdAccountByColumn(int id, string Column, string newValue)
        {
            try
            {

                StringBuilder sb = new StringBuilder();
                sb.Append("Update account ");
                sb.Append("Set " + Column + " = @" + Column + " ");
                sb.Append("Where UserID = @UserID");
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("@UserID", id));
                parms.Add(new MySqlParameter("@" + Column, newValue));
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetAccountInfo(int UserID)
        {
            OAccount acc = new OAccount();
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID, Source, SourceID,  NickName, FirstName, LastName, MidName,(CONCAT(LastName,FirstName)) as FullName, Gender, AgeRange, Local, Email, Birthday, MobilePhone from account  where UserID = @id ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@id", UserID));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    acc.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    acc.Source = Convert.ToString(dt.Rows[0]["Source"]);
                    acc.SourceID = Convert.ToString(dt.Rows[0]["SourceID"]);
                    acc.NickName = Convert.ToString(dt.Rows[0]["NickName"]);
                    acc.FName = Convert.ToString(dt.Rows[0]["FirstName"]);
                    acc.LName = Convert.ToString(dt.Rows[0]["LastName"]);
                    acc.MName = Convert.ToString(dt.Rows[0]["MidName"]);
                    acc.Gender = Convert.ToInt32(dt.Rows[0]["Gender"]);
                    acc.AgeRange = Convert.ToString(dt.Rows[0]["AgeRange"]);
                    acc.Local = Convert.ToString(dt.Rows[0]["Local"]);
                    acc.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    acc.Birthday = Convert.ToString(dt.Rows[0]["Birthday"]);
                    acc.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);
                    acc.FullName = Convert.ToString(dt.Rows[0]["FullName"]);

                    return acc;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetID(string UserID)
        {
            OAccount acc = new OAccount();
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID, Source, SourceID,  NickName, FirstName, LastName, MidName, Gender, AgeRange, Local, Email, Birthday, MobilePhone from account  where UserID = @id ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@id", UserID));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    acc.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    acc.Source = Convert.ToString(dt.Rows[0]["Source"]);
                    acc.SourceID = Convert.ToString(dt.Rows[0]["SourceID"]);
                    acc.NickName = Convert.ToString(dt.Rows[0]["NickName"]);
                    acc.FName = Convert.ToString(dt.Rows[0]["FirstName"]);
                    acc.LName = Convert.ToString(dt.Rows[0]["LastName"]);
                    acc.MName = Convert.ToString(dt.Rows[0]["MidName"]);
                    acc.Gender = Convert.ToInt32(dt.Rows[0]["Gender"]);
                    acc.AgeRange = Convert.ToString(dt.Rows[0]["AgeRange"]);
                    acc.Local = Convert.ToString(dt.Rows[0]["Local"]);
                    acc.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    acc.Birthday = Convert.ToString(dt.Rows[0]["Birthday"]);
                    acc.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);


                    return acc;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetAccountInfo(string Source, string SourceID)
        {
            OAccount acc = new OAccount();
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID, Source, SourceID,  NickName, FirstName, LastName, MidName, Gender, AgeRange, Local, Email, Birthday, MobilePhone from account  where Source = @Source and SourceID = @id");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@Source", Source));
                    parms.Add(new MySqlParameter("@id", SourceID));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    acc.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    acc.Source = Convert.ToString(dt.Rows[0]["Source"]);
                    acc.SourceID = Convert.ToString(dt.Rows[0]["SourceID"]);
                    acc.NickName = Convert.ToString(dt.Rows[0]["NickName"]);
                    acc.FName = Convert.ToString(dt.Rows[0]["FirstName"]);
                    acc.LName = Convert.ToString(dt.Rows[0]["LastName"]);
                    acc.MName = Convert.ToString(dt.Rows[0]["MidName"]);
                    acc.Gender = Convert.ToInt32(dt.Rows[0]["Gender"]);
                    acc.AgeRange = Convert.ToString(dt.Rows[0]["AgeRange"]);
                    acc.Local = Convert.ToString(dt.Rows[0]["Local"]);
                    acc.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    acc.Birthday = Convert.ToString(dt.Rows[0]["Birthday"]);
                    acc.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);


                    return acc;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public OAccount GetAccountInfo(string Email)
        {
            OAccount acc = new OAccount();
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select UserID, Source, SourceID,  NickName, FirstName, LastName, MidName, Gender, AgeRange, Local, Email, Birthday, MobilePhone from account  where Email = @mail ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@mail", Email));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    acc.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    acc.Source = Convert.ToString(dt.Rows[0]["Source"]);
                    acc.SourceID = Convert.ToString(dt.Rows[0]["SourceID"]);
                    acc.NickName = Convert.ToString(dt.Rows[0]["NickName"]);
                    acc.FName = Convert.ToString(dt.Rows[0]["FirstName"]);
                    acc.LName = Convert.ToString(dt.Rows[0]["LastName"]);
                    acc.MName = Convert.ToString(dt.Rows[0]["MidName"]);
                    acc.Gender = Convert.ToInt32(dt.Rows[0]["Gender"]);
                    acc.AgeRange = Convert.ToString(dt.Rows[0]["AgeRange"]);
                    acc.Local = Convert.ToString(dt.Rows[0]["Local"]);
                    acc.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    acc.Birthday = Convert.ToString(dt.Rows[0]["Birthday"]);
                    acc.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);

                    return acc;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string GetPassword(string Email)
        {
            try
            {


                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select Password from account  where Email = @mail ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@mail", Email));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                    return Convert.ToString(dt.Rows[0]["Password"]);
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool ChangePassword(string User, string PasswordN)
        {
            MD5 md5 = MD5.Create();
            string resultMd5 = Convert.ToBase64String(md5.ComputeHash(Encoding.Default.GetBytes(PasswordN)));
            try
            {

                StringBuilder sb = new StringBuilder();
                sb.Append("Update account ");
                sb.Append("Set Password = @Password");
                sb.Append("Where UserID = @UserID");
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("@UserID", User));
                parms.Add(new MySqlParameter("@Password", resultMd5));
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }



    /// <summary>
    /// 發票資料的DAL
    /// </summary>
    public class DALInvoice : CallcarDAL
    {
        public DALInvoice(string ConnectionString)
            : base(ConnectionString)
        {
        }

        public string CreatenewInvoice(int UserID, string InvoiceType, string InvoiceTitle, string EIN, string InvoiceAddress)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            string InvoiceID = string.Empty;

            sb.Append(" INSERT INTO profile_invoice_info ");
            sb.Append(" (InvoiceID,UserID, InvoiceType, InvoiceTitle, EIN, InvoiceAddress) ");
            sb.Append(" VALUES (@InvoiceID,@UserID, @InvoiceType, @InvoiceTitle, @EIN, @InvoiceAddress)");

            InvoiceID = Common.MD5Cryptography.Encrypt(UserID.ToString() + InvoiceType + InvoiceTitle + EIN + InvoiceAddress);

            parms.Add(new MySqlParameter("@InvoiceID", InvoiceID));
            parms.Add(new MySqlParameter("@UserID", UserID));
            parms.Add(new MySqlParameter("@InvoiceType", InvoiceType));
            parms.Add(new MySqlParameter("@InvoiceTitle", InvoiceTitle));
            parms.Add(new MySqlParameter("@EIN", EIN));
            parms.Add(new MySqlParameter("@InvoiceAddress", InvoiceAddress));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                    return InvoiceID;
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool RemoveInvoice(int UserID, string InvoiceID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Delete profile_invoice_info  WHERE InvoiceID=@iid and UserID=@uid");

            parms.Add(new MySqlParameter("@iid", InvoiceID));
            parms.Add(new MySqlParameter("@uid", UserID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeDeleteQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OInvoice> QryUserInvoice(int UserID, string InvoiceID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("Select InvoiceID,  InvoiceType, InvoiceTitle, EIN, InvoiceAddress, DefaultFlag From profile_invoice_info  WHERE UserID = @uid ");

            if (InvoiceID != null && InvoiceID != string.Empty && InvoiceID != "")
            {
                sb.Append(" AND InvoiceID=@iid ");
                parms.Add(new MySqlParameter("@iid", InvoiceID));
            }

            parms.Add(new MySqlParameter("@uid", UserID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                List<OInvoice> List = new List<OInvoice>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OInvoice invoice = new OInvoice();
                    invoice.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    invoice.InvoiceID = Convert.ToString(dt.Rows[i]["InvoiceID"]);
                    invoice.InvoiceType = Convert.ToString(dt.Rows[i]["InvoiceType"]);
                    invoice.InvoiceTitle = Convert.ToString(dt.Rows[i]["InvoiceTitle"]);
                    invoice.EIN = Convert.ToString(dt.Rows[i]["EIN"]);
                    invoice.InvoiceAddress = Convert.ToString(dt.Rows[i]["InvoiceAddress"]);
                    invoice.DefaultFlag = Convert.ToString(dt.Rows[i]["DefaultFlag"]);
                    List.Add(invoice);
                }
                return List;
            }
            else
                return null;
        }

        public bool SetDefault(int UserID, string InvoiceID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Update profile_invoice_info  Set DefaultFlag = '0' WHERE UserID=@uid");

            parms.Add(new MySqlParameter("@uid", UserID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }

            parms.Clear();
            sb.Length = 0;

            sb.Append("Update profile_invoice_info  Set DefaultFlag = '1' WHERE UserID=@uid AND InvoiceID = @iid");
            parms.Add(new MySqlParameter("@uid", UserID));
            parms.Add(new MySqlParameter("@iid", InvoiceID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }

        }

        //public bool Insert2InvoiceRecord(string invNum, string RNum, string Status)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    string InvoiceID = string.Empty;
        //    bool rtn = false;
        //    sb.Append(" INSERT INTO einvoice_usage_record ");
        //    sb.Append(" (EIN_Num, ReservationNo, Status) ");
        //    sb.Append(" VALUES (@invNum,@RNum, @Status)");

        //    parms.Add(new MySqlParameter("@invNum", invNum));
        //    parms.Add(new MySqlParameter("@RNum", RNum));
        //    parms.Add(new MySqlParameter("@Status", Status));
        //    using (dbTConnectionMySQL conn = new dbTConnectionMySQL(this._ConnectionString))
        //    {
        //        try
        //        {
        //            rtn = conn.executeInsertQuery(sb.ToString(), parms.ToArray());
        //            if (!rtn)
        //            {
        //                conn.Rollback();
        //                return false;
        //            }
        //            else
        //            {
        //                conn.Commit();
        //                return rtn;
        //            }

        //        }
        //        catch (MySqlException ex)
        //        {
        //            conn.Rollback();
        //            throw new Exception(ex.Message);
        //        }
        //    }
        //}

        //public string GenInvoiceNum(string Oyear, string Omonth)
        //{
        //    OSerialNum o = new OSerialNum();
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("SELECT SeqNo, Year, Month, Prefix, StartNum, EndNum, UsedNum, TotalCnt FROM einvoice_recrod_maintain Where Year = @year and Month Like @month ");
        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    DataTable dt = new DataTable();

        //    try
        //    {
        //        using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
        //        {
        //            parms.Add(new MySqlParameter("@year", Oyear));
        //            if(Omonth.Length < 2)
        //                parms.Add(new MySqlParameter("@month", "%0" + Omonth + "%"));
        //            else
        //                parms.Add(new MySqlParameter("@month", "%" + Omonth + "%"));
        //            dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
        //        }
        //    }
        //    catch (MySqlException ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //    string invNum = "";
        //    if (dt.Rows.Count > 0)
        //    {                
        //        if (Convert.ToInt32(dt.Rows[0]["TotalCnt"]) != 0)
        //        {
        //            if (Convert.ToString(dt.Rows[0]["StartNum"]) != Convert.ToString(dt.Rows[0]["EndNum"]))
        //            {                      
        //                invNum = Convert.ToString(dt.Rows[0]["Prefix"]) + Convert.ToString(dt.Rows[0]["StartNum"]);
        //                parms.Clear();
        //                sb.Length = 0;
        //                bool rtn = false;
        //                sb.Append("Update einvoice_recrod_maintain  Set StartNum = @Snum , UsedNum = @Unum , TotalCnt = @Tcount WHERE SeqNo= @uid ");
        //                string snum = String.Format("{0:00000000}", Convert.ToInt32(dt.Rows[0]["StartNum"]) + 1);
        //                parms.Add(new MySqlParameter("@Snum", snum));
        //                parms.Add(new MySqlParameter("@Unum", Convert.ToInt32(dt.Rows[0]["UsedNum"]) + 1));
        //                parms.Add(new MySqlParameter("@Tcount", Convert.ToInt32(dt.Rows[0]["TotalCnt"]) - 1));
        //                parms.Add(new MySqlParameter("@uid", Convert.ToInt32(dt.Rows[0]["SeqNo"])));

        //                using (dbTConnectionMySQL conn = new dbTConnectionMySQL(this._ConnectionString))
        //                {
        //                    try
        //                    {
        //                        rtn = conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
        //                        if (!rtn)
        //                        {
        //                            conn.Rollback();
        //                            return "";
        //                        }
        //                        else
        //                        {
        //                            conn.Commit();
        //                        }
        //                    }
        //                    catch (MySqlException ex)
        //                    {
        //                        conn.Rollback();
        //                        throw new Exception(ex.Message);
        //                    }
        //                }
        //            }
        //        }
        //        return invNum;
        //    }
        //    else
        //        return invNum;
        //}
    }

    /// <summary>
    /// 取號的DAL
    /// </summary>
    public class DALSerialNum : CallcarDAL
    {
        public DALSerialNum(string ConnectionString)
            : base(ConnectionString)
        {
        }

        public OSerialNum GetSerialNumObject(string Purpose)
        {
            OSerialNum o = new OSerialNum();
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT Prefix, Digit, Current, Used FROM sequence_maintain Where Purpose = @p");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    parms.Add(new MySqlParameter("@p", Purpose));
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                o.Prefix = Convert.ToString(dt.Rows[0]["Prefix"]);
                o.Current = Convert.ToString(dt.Rows[0]["Current"]);
                o.Digit = Convert.ToInt32(dt.Rows[0]["Digit"]);
                o.Used = Convert.ToInt32(dt.Rows[0]["Used"]);
                o.Purpose = Purpose;
                return o;
            }
            else
                return null;
        }

        public string GetSeqNoString(OSerialNum os)
        {
            StringBuilder sb = new StringBuilder();


            int ReturnSeq = 0;
            sb.Append(" UPDATE sequence_maintain SET ");

            string NowPrefix = "";
            try
            {
                NowPrefix = DateTime.Now.ToString(os.Prefix);
            }
            catch (FormatException)
            {
                throw new Exception("Prefix格式不合");
            }

            if (NowPrefix != os.Current)
            {
                ReturnSeq = 1;
                sb.Append(" Current = '" + NowPrefix + "',");
                sb.Append(" Used = " + ReturnSeq.ToString() + " ");
            }
            else
            {
                ReturnSeq = os.Used + 1;
                sb.Append("Used = " + ReturnSeq.ToString() + " ");
            }
            sb.Append(" Where Purpose = @p ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@p", os.Purpose));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException)
            {
                throw new Exception("更新序號失敗");
            }
            os.Used = ReturnSeq;
            return ReturnSeq.ToString().PadLeft(os.Digit, '0');
        }
    }

    /// <summary>
    /// 信用卡 DAL
    /// </summary>
    public class DALCreditCard : CallcarDAL
    {
        public DALCreditCard(string ConnectionString)
            : base(ConnectionString)
        {
        }

        public int CreateNewCard(int UserID, string CardCompany, string CardNo, string DeadLine, string AuthCode, string Pay2goToken, string Pay2goTokenExpire)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append(" INSERT INTO profile_card_info (UserID, CardCompany, CardNo, DeadLine, AuthCode, Pay2goToken, Pay2goTokenExpire, CheckStatus) ");
            sb.Append(" VALUES (@UserID, @CardCompany, @CardNo, @DeadLine, @AuthCode , @Pay2goToken, @Pay2goTokenExpire ,@CheckStatus)");
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@UserID", UserID));
                parms.Add(new MySqlParameter("@CardCompany", CardCompany));
                parms.Add(new MySqlParameter("@CardNo", CardNo));
                parms.Add(new MySqlParameter("@DeadLine", DeadLine));
                parms.Add(new MySqlParameter("@AuthCode", AuthCode));
                parms.Add(new MySqlParameter("@Pay2goToken", Pay2goToken));
                parms.Add(new MySqlParameter("@Pay2goTokenExpire", Pay2goTokenExpire));
                parms.Add(new MySqlParameter("@CheckStatus", 1));

                try
                {
                    conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                parms.Clear();
                sb.Length = 0;
                sb.Append("SELECT LAST_INSERT_ID()");
                DataTable dt = new DataTable();
                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                    return Convert.ToInt32(dt.Rows[0][0]);

                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }

        }

        public bool RemoveCard(int UserID, string CardID)
        {

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Delete From  profile_card_info WHERE CardID=@no and UserID=@id");
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@no", CardID));
                parms.Add(new MySqlParameter("@id", UserID));
                try
                {
                    return conn.executeDeleteQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        public OCard QryUserCards(int UserID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("Select CardID, UserID, CardCompany, CardNo, DeadLine, AuthCode,CheckStatus,Pay2goToken, Pay2goTokenExpire From profile_card_info  WHERE UserID = @uid ");
            parms.Add(new MySqlParameter("@uid", UserID));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {


                OCard Card = new OCard();
                Card.UID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                Card.ID = Convert.ToInt32(dt.Rows[0]["CardID"]);
                Card.CNo = Convert.ToString(dt.Rows[0]["CardNo"]);
                Card.Code = Convert.ToString(dt.Rows[0]["AuthCode"]);
                Card.Company = Convert.ToString(dt.Rows[0]["CardCompany"]);
                Card.Expire = Convert.ToString(dt.Rows[0]["DeadLine"]);
                Card.Check = Convert.ToInt32(dt.Rows[0]["CheckStatus"]);
                Card.t = Convert.ToString(dt.Rows[0]["Pay2goToken"]);
                Card.d = Convert.ToString(dt.Rows[0]["Pay2goTokenExpire"]);


                return Card;
            }
            else
                return null;
        }
    }

    /// <summary>
    /// 訂單的DAL
    /// </summary>
    public class DALReservation : CallcarDAL
    {
        public DALReservation(string ConnectionString) : base(ConnectionString) { }

        /// <summary>
        /// 新增訂單
        /// </summary>
        /// <param name="or">訂單內容</param>
        /// <param name="PickType">預約0/撿車1</param>
        /// <param name="Dno">派車單號</param>
        /// <returns></returns>
        public bool CreateNewReservation(OReservation or, string PickType, string Dno)
        {

            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO reservation_sheet  ");
            sb.Append("(ReservationNo,UserID, ServiceType, TakeDate, TimeSegment, SelectionType,PickCartype, FlightNo, ScheduleFlightTime,  ");
            sb.Append("PickupCity, PickupDistinct,PickupVillage, PickupAddress,PickupGPS, TakeoffCity, TakeoffDistinct,TakeoffVillage, TakeoffAddress, TakeoffGPS, PassengerCnt,   ");
            sb.Append("BaggageCnt,MaxFlag, Price, Coupon, InvoiceData,CreditData, ProcessStage)  ");
            sb.Append("VALUES (@ReservationNo,@UserID, @ServiceType, @TakeDate, @TimeSegment, @SelectionType, @PickCartype,@FlightNo, @ScheduleFlightTime,  ");
            sb.Append("@PickupCity, @PickupDistinct,@PickupVillage, @PickupAddress,GeomFromText(@PickupGPS), @TakeoffCity, @TakeoffDistinct, @TakeoffVillage, @TakeoffAddress, GeomFromText(@TakeoffGPS), @PassengerCnt,   ");
            sb.Append("@BaggageCnt, @MaxFlag,@Price, @Coupon, @InvoiceData,@CreditData, @ProcessStage)  ");

            using (dbTConnectionMySQL conn = new dbTConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@ReservationNo", or.ReservationNo));
                parms.Add(new MySqlParameter("@UserID", or.UserID));
                parms.Add(new MySqlParameter("@ServiceType", or.ServiceType));
                parms.Add(new MySqlParameter("@TakeDate", or.ServeDate));
                parms.Add(new MySqlParameter("@TimeSegment", or.PreferTime));
                parms.Add(new MySqlParameter("@SelectionType", or.ChooseType));

                if (or.ChooseType == "F")
                {
                    parms.Add(new MySqlParameter("@FlightNo", or.FlightNo));
                    parms.Add(new MySqlParameter("@ScheduleFlightTime", or.FlightTime));
                }
                else
                {
                    parms.Add(new MySqlParameter("@FlightNo", ""));
                    parms.Add(new MySqlParameter("@ScheduleFlightTime", null));
                }
                parms.Add(new MySqlParameter("@PickCartype", or.PickCartype));
                parms.Add(new MySqlParameter("@PickupCity", or.PickupCity));
                parms.Add(new MySqlParameter("@PickupDistinct", or.PickupDistinct));
                parms.Add(new MySqlParameter("@PickupVillage", or.PickupVillage));
                parms.Add(new MySqlParameter("@PickupAddress", or.PickupAddress));
                parms.Add(new MySqlParameter("@PickupGPS", "Point(" + or.PickupGPS.GetLng().ToString() + " " + or.PickupGPS.GetLat().ToString() + ")"));
                parms.Add(new MySqlParameter("@TakeoffCity", or.TakeoffCity));
                parms.Add(new MySqlParameter("@TakeoffDistinct", or.TakeoffDistinct));
                parms.Add(new MySqlParameter("@TakeoffVillage", or.TakeoffVillage));
                parms.Add(new MySqlParameter("@TakeoffAddress", or.TakeoffAddress));
                parms.Add(new MySqlParameter("@TakeoffGPS", "Point(" + or.TakeoffGPS.GetLng().ToString() + " " + or.TakeoffGPS.GetLat().ToString() + ")"));
                parms.Add(new MySqlParameter("@PassengerCnt", or.PassengerCnt));
                parms.Add(new MySqlParameter("@BaggageCnt", or.BaggageCnt));
                parms.Add(new MySqlParameter("@MaxFlag", or.MaxFlag));
                parms.Add(new MySqlParameter("@Coupon", or.Coupon));
                parms.Add(new MySqlParameter("@InvoiceData", or.Invoice));
                parms.Add(new MySqlParameter("@CreditData", or.Credit));
                parms.Add(new MySqlParameter("@ProcessStage", or.ProcessStage));
                parms.Add(new MySqlParameter("@Price", or.TrailPrice));

                bool rtn = false;
                try
                {
                    rtn = conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    conn.Rollback();
                    throw new Exception(ex.Message);
                }
                catch (Exception ex)
                {
                    conn.Rollback();
                    throw new Exception(ex.Message);
                }

                if (rtn)
                {
                    if (PickType == "0")
                    {
                        conn.Commit();
                        return true;
                    }
                    else
                    {
                        //撿車的 必須新增派車單訂單資料
                        //新增一筆dispatch_reservation
                        sb.Length = 0;
                        parms.Clear();
                        DateTime LastServiceTime;
                        DALDispatch dald = new DALDispatch(this._ConnectionString);
                        try
                        {
                            LastServiceTime = dald.GetLastScheduleServiceTime(Dno);
                        }
                        catch (Exception ex)
                        {
                            conn.Rollback();
                            throw new Exception(ex.Message);
                        }
                        if (or.ServiceType == "O")
                            LastServiceTime = LastServiceTime.AddMinutes(15);

                        sb.Append("INSERT INTO dispatch_reservation	(DispatchNo, ReservationNo, ServeSeq, ScheduleServeTime, ActualServreTime, LocationGPS, ServiceRemark) ");
                        sb.Append("VALUES (@DispatchNo, @ReservationNo, 0, @ScheduleServeTime,NULL , GeomFromText(@LocationGPS), '0')");
                        parms.Add(new MySqlParameter("@DispatchNo", Dno));
                        parms.Add(new MySqlParameter("@ReservationNo", or.ReservationNo));
                        parms.Add(new MySqlParameter("@ScheduleServeTime", LastServiceTime));
                        if (or.ServiceType == "O")
                            parms.Add(new MySqlParameter("@LocationGPS", "Point(" + or.PickupGPS.GetLng().ToString() + " " + or.PickupGPS.GetLat().ToString() + ")"));
                        else
                            parms.Add(new MySqlParameter("@LocationGPS", "Point(" + or.TakeoffGPS.GetLng().ToString() + " " + or.TakeoffGPS.GetLat().ToString() + ")"));

                        if (!conn.executeInsertQuery(sb.ToString(), parms.ToArray()))
                        {
                            conn.Rollback();
                            return false;
                        }

                        sb.Length = 0;
                        parms.Clear();
                        ODispatchSheet os = dald.GetDispatchSheetBrief(Dno);

                        sb.Append("UPDATE dispatch_sheet SET PassengerCnt=@pcnt,BaggageCnt=@bcnt,StopCnt=StopCnt+1  ");
                        sb.Append("WHERE DispatchNo = @no");
                        parms.Add(new MySqlParameter("@no", Dno));
                        parms.Add(new MySqlParameter("@bcnt", os.BaggageCnt + or.BaggageCnt));
                        parms.Add(new MySqlParameter("@pcnt", os.PassengerCnt + or.PassengerCnt));

                        if (!conn.executeUpdateQuery(sb.ToString(), parms.ToArray()))
                        {
                            conn.Rollback();
                            return false;
                        }

                        conn.Commit();
                        return true;
                    }
                }
                else
                {
                    conn.Rollback();
                    return false;
                }
            }
        }

        /// <summary>
        /// 新增追蹤航班
        /// </summary>
        /// <param name="FlightNo">航班編號</param>
        /// <param name="FlightTime">航班時間</param>
        /// <returns></returns>
        public bool AddTrackFlight(string FlightNo, DateTime FlightTime)
        {
            string AirlineID, FlightNumber;

            AirlineID = FlightNo.Substring(0, 2);
            FlightNumber = FlightNo.Substring(2, FlightNo.Length - 2);
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("INSERT INTO flight_track (FlightDate, AirlineID,FlightNo,FlightTime)VALUES (@FlightDate,@AirlineID,@FlightNo,@FlightTime )");

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@FlightDate", FlightTime.ToString("yyyyMMdd")));
                parms.Add(new MySqlParameter("@AirlineID", AirlineID));
                parms.Add(new MySqlParameter("@FlightNo", FlightNumber));
                parms.Add(new MySqlParameter("@FlightTime", FlightTime));
                try
                {
                    conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException)
                {
                    return true;
                }
            }
            return true;
        }

        /// <summary>
        /// 回傳一筆訂單物件
        /// </summary>
        /// <param name="RNo">訂單編號</param>
        /// <returns></returns>
        public OReservation GetReservation(string RNo)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype, FlightNo, ScheduleFlightTime, PickupCity, ");
            sb.Append("PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , PassengerCnt, ");
            sb.Append("BaggageCnt, MaxFlag, Price, Coupon, InvoiceData, CreditData, ProcessStage, InvoiceNum, CreateTime, UpdTime, PassengerName, PassengerPhone,VanpoolFlag,BabySeatCnt ");
            sb.Append("FROM reservation_sheet where ReservationNo = @no");

            parms.Add(new MySqlParameter("@no", RNo));
            DataTable dt = new DataTable();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {

                OReservation r = new OReservation();
                r.BaggageCnt = Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                r.ChooseType = Convert.ToString(dt.Rows[0]["SelectionType"]);
                r.TrailPrice = Convert.ToInt32(dt.Rows[0]["Price"]);
                if (r.ChooseType == "T")
                {
                    r.FlightDate = "";
                    r.FlightNo = "";
                }
                else
                {
                    r.FlightDate = Convert.ToDateTime(dt.Rows[0]["ScheduleFlightTime"]).ToString("yyyyMMdd");
                    r.FlightNo = Convert.ToString(dt.Rows[0]["FlightNo"]);
                    r.FlightTime = Convert.ToDateTime(dt.Rows[0]["ScheduleFlightTime"]);
                }
                r.Coupon = Convert.ToString(dt.Rows[0]["Coupon"]);
                r.Credit = Convert.ToString(dt.Rows[0]["CreditData"]);
                r.Invoice = Convert.ToString(dt.Rows[0]["InvoiceData"]);
                r.MaxFlag = Convert.ToString(dt.Rows[0]["MaxFlag"]);
                r.OrderTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                r.udpTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);
                r.PassengerCnt = Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                r.PickCartype = Convert.ToString(dt.Rows[0]["PickCartype"]);
                r.PickupAddress = Convert.ToString(dt.Rows[0]["PickupAddress"]);
                r.PickupCity = Convert.ToString(dt.Rows[0]["PickupCity"]);
                r.PickupDistinct = Convert.ToString(dt.Rows[0]["PickupDistinct"]);
                r.PickupVillage = Convert.ToString(dt.Rows[0]["PickupVillage"]);
                r.PreferTime = Convert.ToString(dt.Rows[0]["TimeSegment"]);
                r.ProcessStage = Convert.ToString(dt.Rows[0]["ProcessStage"]);
                r.ReservationNo = Convert.ToString(dt.Rows[0]["ReservationNo"]);
                r.ServeDate = Convert.ToString(dt.Rows[0]["TakeDate"]);
                r.ServiceType = Convert.ToString(dt.Rows[0]["ServiceType"]);
                r.TakeoffAddress = Convert.ToString(dt.Rows[0]["TakeoffAddress"]);
                r.TakeoffCity = Convert.ToString(dt.Rows[0]["TakeoffCity"]);
                r.TakeoffDistinct = Convert.ToString(dt.Rows[0]["TakeoffDistinct"]);
                r.TakeoffVillage = Convert.ToString(dt.Rows[0]["TakeoffVillage"]);
                r.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                r.EIN = Convert.ToString(dt.Rows[0]["InvoiceNum"]);
                r.TakeoffGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[0]["tlat"]), Convert.ToDouble(dt.Rows[0]["tlng"]));
                r.PickupGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[0]["plat"]), Convert.ToDouble(dt.Rows[0]["plng"]));
                r.PassengerName = Convert.ToString(dt.Rows[0]["PassengerName"]);
                r.PassengerPhone = Convert.ToString(dt.Rows[0]["PassengerPhone"]);
                r.VanpoolFlag = Convert.ToString(dt.Rows[0]["VanpoolFlag"]);
                r.BabySeatCnt = Convert.ToInt32(dt.Rows[0]["BabySeatCnt"]);
                return r;

            }
            else
            {
                return null;
            }

        }

        public OReservation_process_log GetReservation_process_log(string ReservationNo)
        {

            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT ReservationNo,ProcessStage,Memo ");
            sb.Append("FROM reservation_process_log  where ReservationNo = @ReservationNo");

            parms.Add(new MySqlParameter("@ReservationNo", ReservationNo));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {

                OReservation_process_log r = new OReservation_process_log();
                r.ReservationNo = Convert.ToString(dt.Rows[0]["ReservationNo"]);
                r.ProcessStage = Convert.ToString(dt.Rows[0]["ProcessStage"]);
                r.Memo = Convert.ToString(dt.Rows[0]["Memo"]);
                return r;

            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 依照日期回傳一批訂單
        /// </summary>
        /// <param name="sdate">查詢起始時間</param>
        /// <param name="edate">查詢結束時間</param>
        /// <returns></returns>
        public List<OReservation> GetReservations(string sdate, string edate)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype, FlightNo, ScheduleFlightTime, PickupCity, ");
            sb.Append("PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , PassengerCnt, ");
            sb.Append("BaggageCnt, MaxFlag, Price, Coupon, InvoiceData, CreditData, ProcessStage, InvoiceNum, CreateTime, UpdTime ");
            sb.Append("FROM reservation_sheet where TakeDate between  @sdate AND @edate");

            parms.Add(new MySqlParameter("@sdate", sdate));
            parms.Add(new MySqlParameter("@edate", edate));
            DataTable dt = new DataTable();
            List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OReservation r = new OReservation();
                    r.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                    r.ChooseType = Convert.ToString(dt.Rows[i]["SelectionType"]);
                    r.Coupon = Convert.ToString(dt.Rows[i]["Coupon"]);
                    r.Credit = Convert.ToString(dt.Rows[i]["CreditData"]);
                    r.FlightDate = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]).ToString("yyyyMMdd");
                    r.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                    r.FlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
                    r.Invoice = Convert.ToString(dt.Rows[i]["InvoiceData"]);
                    r.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                    r.OrderTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    r.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                    r.PickCartype = Convert.ToString(dt.Rows[i]["PickCartype"]);
                    r.PickupAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                    r.PickupCity = Convert.ToString(dt.Rows[i]["PickupCity"]);
                    r.PickupDistinct = Convert.ToString(dt.Rows[i]["PickupDistinct"]);
                    r.PickupVillage = Convert.ToString(dt.Rows[i]["PickupVillage"]);
                    r.PreferTime = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                    r.ProcessStage = Convert.ToString(dt.Rows[i]["ProcessStage"]);
                    r.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    r.ServeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                    r.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                    r.TakeoffAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                    r.TakeoffCity = Convert.ToString(dt.Rows[i]["TakeoffCity"]);
                    r.TakeoffDistinct = Convert.ToString(dt.Rows[i]["TakeoffDistinct"]);
                    r.TakeoffVillage = Convert.ToString(dt.Rows[i]["TakeoffVillage"]);
                    r.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    r.EIN = Convert.ToString(dt.Rows[i]["InvoiceNum"]);
                    r.TakeoffGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["tlat"]), Convert.ToDouble(dt.Rows[i]["tlng"]));
                    r.PickupGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["plat"]), Convert.ToDouble(dt.Rows[i]["plng"]));
                    List.Add(r);
                }
                return List;
            }
            else
            {
                return null;
            }
        }

        public List<OReservation> GetReservationsStage(string sdate, string edate, string stage, int FleetID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT r.ReservationNo, r.UserID, r.ServiceType, r.TakeDate, r.TimeSegment, r.SelectionType, r.PickCartype, r.FlightNo, r.ScheduleFlightTime, r.PickupCity, ");
            sb.Append("r.PickupDistinct, r.PickupVillage, r.PickupAddress, X(r.PickupGPS) as plng, Y(r.PickupGPS) as plat, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffVillage, r.TakeoffAddress, X(r.TakeoffGPS) as tlng ,Y(r.TakeoffGPS) as tlat , r.PassengerCnt, ");
            sb.Append("r.BaggageCnt, r.MaxFlag, r.Price, r.Coupon, r.InvoiceData, r.CreditData, r.ProcessStage, r.InvoiceNum, r.CreateTime, r.UpdTime, s.DispatchNo, d.DispatchNo, d.ReservationNo, s.DriverID, u.UserID, s.FleetID, rcl.ChargeResult, rcl.RealAmt ");
            sb.Append("FROM dispatch_sheet s,dispatch_reservation d,reservation_sheet r ,user_info u ,reservation_charge_list rcl where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo and r.ReservationNo = rcl.ReservationNo and s.DriverID = u.UserID  and r.TakeDate between  @sdate AND @edate");
            if (stage != "")
            {
                sb.Append(" AND rcl.ChargeResult = @ChargeResult");
                parms.Add(new MySqlParameter("@ChargeResult", stage));
            }
            if (FleetID != 0)
            {
                sb.Append(" AND s.FleetID = @FleetID");
                parms.Add(new MySqlParameter("@FleetID", FleetID));
            }
            parms.Add(new MySqlParameter("@sdate", sdate));
            parms.Add(new MySqlParameter("@edate", edate));
            DataTable dt = new DataTable();
            List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                try
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        OReservation r = new OReservation();
                        r.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                        r.ChooseType = Convert.ToString(dt.Rows[i]["SelectionType"]);
                        r.Coupon = Convert.ToString(dt.Rows[i]["Coupon"]);
                        r.Credit = Convert.ToString(dt.Rows[i]["CreditData"]);
                        if (dt.Rows[i]["ScheduleFlightTime"].ToString() != "")
                            r.FlightDate = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]).ToString("yyyyMMdd");
                        else
                            r.FlightDate = "19110101";
                        r.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                        if (dt.Rows[i]["ScheduleFlightTime"].ToString() != "")
                            r.FlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
                        else
                            r.FlightTime = Convert.ToDateTime("1911-01-01 00:00:00");
                        r.Invoice = Convert.ToString(dt.Rows[i]["InvoiceData"]);
                        r.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                        r.OrderTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                        r.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                        r.PickCartype = Convert.ToString(dt.Rows[i]["PickCartype"]);
                        r.PickupAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                        r.PickupCity = Convert.ToString(dt.Rows[i]["PickupCity"]);
                        r.PickupDistinct = Convert.ToString(dt.Rows[i]["PickupDistinct"]);
                        r.PickupVillage = Convert.ToString(dt.Rows[i]["PickupVillage"]);
                        r.PreferTime = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                        r.ProcessStage = Convert.ToString(dt.Rows[i]["ChargeResult"]);
                        r.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        r.ServeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                        r.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                        r.TakeoffAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                        r.TakeoffCity = Convert.ToString(dt.Rows[i]["TakeoffCity"]);
                        r.TakeoffDistinct = Convert.ToString(dt.Rows[i]["TakeoffDistinct"]);
                        r.TakeoffVillage = Convert.ToString(dt.Rows[i]["TakeoffVillage"]);
                        r.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                        r.EIN = Convert.ToString(dt.Rows[i]["InvoiceNum"]);
                        r.TakeoffGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["tlat"]), Convert.ToDouble(dt.Rows[i]["tlng"]));
                        r.PickupGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["plat"]), Convert.ToDouble(dt.Rows[i]["plng"]));
                        r.carteam = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                        r.TrailPrice = Convert.ToInt32(dt.Rows[i]["Price"]);
                        r.realamt = Convert.ToInt32(dt.Rows[i]["RealAmt"]);
                        List.Add(r);
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
                return List;

            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 根據多重條件回傳一批訂單
        /// </summary>
        /// <param name="qs">QOrderCondStruct</param>
        /// <returns></returns>
        public List<OReservation> GetReservationsMultipleCondition(QOrderCondStruct qs, int take, int skip, string ProcessStage, string CarpoolFlag)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT r.ReservationNo, r.UserID, r.ServiceType, r.TakeDate, r.TimeSegment, r.SelectionType, r.PickCartype,r.CarpoolFlag, r.FlightNo, r.ScheduleFlightTime, r.PickupCity,r.PassengerName, ");
            sb.Append("r.PickupDistinct, r.PickupVillage, r.PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffVillage, r.TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , r.PassengerCnt,r.VanpoolFlag, ");
            sb.Append("r.BaggageCnt, r.MaxFlag, r.Price, r.Coupon, r.InvoiceData, r.CreditData, r.ProcessStage, r.InvoiceNum, r.CreateTime, r.UpdTime,(select DATE_FORMAT(d.ScheduleServeTime,'%H:%i') from dispatch_reservation d where d.ReservationNo = r.ReservationNo ) as ScheduleServeTime ");
            sb.Append("FROM reservation_sheet r, account u ");
            sb.Append("where r.UserID = u.UserID  ");

            if (qs.UserID > 0)
            {
                sb.Append(" And r.UserID = @id ");
                parms.Add(new MySqlParameter("@id", qs.UserID));
            }

            if (qs.MobilePhone != "")
            {
                sb.Append(" And u.MobilePhone = @phone ");
                parms.Add(new MySqlParameter("@phone", qs.MobilePhone));
            }

            if (qs.PMobilePhone != "")
            {
                sb.Append(" And r.PassengerPhone = @PMobilePhone ");
                parms.Add(new MySqlParameter("@PMobilePhone", qs.PMobilePhone));
            }

            if (qs.OrderNo != "")
            {
                sb.Append(" And r.ReservationNo = @No ");
                parms.Add(new MySqlParameter("@No", qs.OrderNo));
            }

            if (qs.QEDate != "" && qs.QSDate != "")
            {
                sb.Append(" And r.TakeDate between @sdate and @edate ");
                parms.Add(new MySqlParameter("@sdate", qs.QSDate));
                parms.Add(new MySqlParameter("@edate", qs.QEDate));
            }

            if (ProcessStage == "0")
                sb.Append(" And r.ProcessStage in ('0','A','Z')  ");
            else if (ProcessStage == "1")
                sb.Append(" And r.ProcessStage = 'X'");
            else if (ProcessStage == "2")
                sb.Append(" And r.ProcessStage = 'Y'");
            else if (ProcessStage == "3")
                sb.Append(" And r.ProcessStage = 'W'");

            if (CarpoolFlag == "P")
                sb.Append(" And r.CarpoolFlag = 'P' ");
            else if (CarpoolFlag == "S")
                sb.Append(" And r.CarpoolFlag = 'S'  ");
            else if (CarpoolFlag == "V")
                sb.Append(" And r.CarpoolFlag = 'V'  ");

            if (take != 0)
            {
                sb.Append(" limit " + (1 + skip).ToString() + "," + (1 + skip + take).ToString());
            }

            DataTable dt = new DataTable();
            List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OReservation r = new OReservation();
                    r.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                    r.ChooseType = Convert.ToString(dt.Rows[i]["SelectionType"]);
                    r.Coupon = Convert.ToString(dt.Rows[i]["Coupon"]);
                    r.Credit = Convert.ToString(dt.Rows[i]["CreditData"]);
                    r.ScheduleServeTime = Convert.ToString(dt.Rows[i]["ScheduleServeTime"]);
                    if (r.ChooseType == "F")
                    {
                        r.FlightDate = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]).ToString("yyyyMMdd");
                        r.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                        r.FlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
                    }
                    r.Invoice = Convert.ToString(dt.Rows[i]["InvoiceData"]);
                    r.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                    // r.OrderTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    r.OrderTime = Convert.ToDateTime(dt.Rows[i]["UpdTime"]);
                    r.PassengerName = Convert.ToString(dt.Rows[i]["PassengerName"]);
                    r.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                    r.PickCartype = Convert.ToString(dt.Rows[i]["PickCartype"]);
                    r.PickupAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                    r.PickupCity = Convert.ToString(dt.Rows[i]["PickupCity"]);
                    r.PickupDistinct = Convert.ToString(dt.Rows[i]["PickupDistinct"]);
                    r.PickupVillage = Convert.ToString(dt.Rows[i]["PickupVillage"]);
                    r.PreferTime = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                    r.ProcessStage = Convert.ToString(dt.Rows[i]["ProcessStage"]);
                    r.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    r.ServeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                    r.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                    r.TakeoffAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                    r.TakeoffCity = Convert.ToString(dt.Rows[i]["TakeoffCity"]);
                    r.TakeoffDistinct = Convert.ToString(dt.Rows[i]["TakeoffDistinct"]);
                    r.TakeoffVillage = Convert.ToString(dt.Rows[i]["TakeoffVillage"]);
                    r.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    r.EIN = Convert.ToString(dt.Rows[i]["InvoiceNum"]);
                    r.TakeoffGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["tlat"]), Convert.ToDouble(dt.Rows[i]["tlng"]));
                    r.PickupGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["plat"]), Convert.ToDouble(dt.Rows[i]["plng"]));
                    r.CarpoolFlag = Convert.ToString(dt.Rows[i]["CarpoolFlag"]);
                    r.VanpoolFlag = Convert.ToString(dt.Rows[i]["VanpoolFlag"]);
                    List.Add(r);
                }
                return List;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 更新訂單服務地點
        /// </summary>
        /// <param name="Rno">訂單編號</param>
        /// <param name="City">城市數列</param>
        /// <param name="Distinct">區數列</param>
        /// <param name="Village">里數列</param>
        ///  <param name="lat">緯度數列</param>
        ///  <param name="lng">經度數列</param>
        /// <returns></returns>
        public bool UpdReservationServicePostion(string Rno, string ServiceType, List<string> City, List<string> Distinct, List<string> Village, List<string> Address, List<string> lat, List<string> lng)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("update reservation_sheet set ");
            if (ServiceType == "O")
                sb.Append("PickupCity = @city, PickupDistinct=@distinct, PickupVillage = @vil, PickupAddress = @add, PickupGPS = GEOFROMTEXT(@gps) ");
            else
                sb.Append("TakeoffCity = @city, TakeoffDistinct=@distinct, TakeoffVillage = @vil, TakeoffAddress = @add, TakeoffGPS = GEOFROMTEXT(@gps) ");

            parms.Add(new MySqlParameter("@city", City[1]));
            parms.Add(new MySqlParameter("@distinct", Distinct[1]));
            parms.Add(new MySqlParameter("@vil", Village[1]));
            parms.Add(new MySqlParameter("@add", Address[1]));
            parms.Add(new MySqlParameter("@gps", "POINT( " + lng[1] + " " + lat[1] + ")"));

            sb.Append(" where ReservationNo = @no ");

            if (ServiceType == "O")
                sb.Append(" And PickupCity = @city2 AND PickupDistinct=@distinct2 AND  PickupVillage = @vil2 AND PickupAddress = @add2 AND PickupGPS = GEOFROMTEXT(@gps2) ");
            else
                sb.Append(" AND TakeoffCity = @city2 AND  TakeoffDistinct=@distinct2 AND TakeoffVillage = @vil2 AND TakeoffAddress = @add2 AND TakeoffGPS = GEOFROMTEXT(@gps2) ");

            parms.Add(new MySqlParameter("@city2", City[0]));
            parms.Add(new MySqlParameter("@distinct2", Distinct[0]));
            parms.Add(new MySqlParameter("@vil2", Village[0]));
            parms.Add(new MySqlParameter("@add2", Address[0]));
            parms.Add(new MySqlParameter("@gps2", "POINT( " + lng[0] + " " + lat[0] + ")"));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// 回傳待媒合訂單
        /// </summary>
        /// <param name="sdate">媒合日期</param>
        /// <returns></returns>
        public List<OReservation> GetReservationForMatch(string sdate)
        {
            //配合司機班表
            //媒合時間範圍為 0300~ D+1 0259 
            string edate;
            edate = Common.General.ConvertToDateTime(sdate, 8).AddDays(1).ToString("yyyyMMdd");

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT ReservationNo, UserID, ServiceType, TakeDate, TimeSegment, SelectionType, PickCartype, FlightNo, ScheduleFlightTime, PickupCity, ");
            sb.Append("PickupDistinct, PickupVillage, PickupAddress, X(PickupGPS) as plng, Y(PickupGPS) as plat, TakeoffCity, TakeoffDistinct, TakeoffVillage, TakeoffAddress, X(TakeoffGPS) as tlng ,Y(TakeoffGPS) as tlat , PassengerCnt, ");
            sb.Append("BaggageCnt, MaxFlag, Price, Coupon, InvoiceData, CreditData, ProcessStage, InvoiceNum, CreateTime, UpdTime ");
            sb.Append("FROM reservation_sheet where (TakeDate = @sdate AND TimeSegment >= '0300') or (TakeDate = @edate and TimeSegment <= '0200') AND ProcessStage = '0' order by TakeDate,TimeSegment asc ");

            parms.Add(new MySqlParameter("@sdate", sdate));
            parms.Add(new MySqlParameter("@edate", edate));
            DataTable dt = new DataTable();
            List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    OReservation r = new OReservation();
                    try
                    {
                        r.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                        //r.ChooseType = Convert.ToString(dt.Rows[i]["SelectionType"]);
                        //r.Coupon = Convert.ToString(dt.Rows[i]["Coupon"]);
                        //r.Credit = Convert.ToString(dt.Rows[i]["CreditData"]);

                        //if (r.ChooseType == "T")
                        //{
                        //    r.FlightNo = "";
                        //    r.FlightDate = "";
                        //    r.FlightTime = DateTime.Now;
                        //}
                        //else
                        //{
                        //    r.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                        //    r.FlightDate = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]).ToString("yyyyMMdd");
                        //    r.FlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
                        //}
                        //r.Invoice = Convert.ToString(dt.Rows[i]["InvoiceData"]);
                        r.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                        //r.OrderTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                        r.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                        //r.PickCartype = Convert.ToString(dt.Rows[i]["PickCartype"]);
                        //r.PickupAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                        //r.PickupCity = Convert.ToString(dt.Rows[i]["PickupCity"]);
                        //r.PickupDistinct = Convert.ToString(dt.Rows[i]["PickupDistinct"]);
                        //r.PickupVillage = Convert.ToString(dt.Rows[i]["PickupVillage"]);
                        r.PreferTime = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                        //r.ProcessStage = Convert.ToString(dt.Rows[i]["ProcessStage"]);
                        r.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        r.ServeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                        r.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                        //r.TakeoffAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                        //r.TakeoffCity = Convert.ToString(dt.Rows[i]["TakeoffCity"]);
                        //r.TakeoffDistinct = Convert.ToString(dt.Rows[i]["TakeoffDistinct"]);
                        //r.TakeoffVillage = Convert.ToString(dt.Rows[i]["TakeoffVillage"]);
                        //r.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                        //r.EIN = Convert.ToString(dt.Rows[i]["InvoiceNum"]);
                        r.TakeoffGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["tlat"]), Convert.ToDouble(dt.Rows[i]["tlng"]));
                        r.PickupGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["plat"]), Convert.ToDouble(dt.Rows[i]["plng"]));
                        List.Add(r);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.StackTrace + Environment.NewLine + JsonConvert.SerializeObject(r));
                    }
                }
                return List;
            }
            else
            {
                return null;
            }
        }

        public bool UpdReservationTokenValueandEXP(string cardData, string Sno)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("update reservation_sheet set ");

            sb.Append("CreditData = @cardData");

            parms.Add(new MySqlParameter("@cardData", cardData));

            sb.Append(" where ReservationNo = @no ");
            parms.Add(new MySqlParameter("@no", Sno));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public bool UpdReservationProStage(string Sno)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("update reservation_sheet set ");

            sb.Append("ProcessStage = @ProcessStage");

            parms.Add(new MySqlParameter("@ProcessStage", "Z"));

            sb.Append(" where ReservationNo = @no ");
            parms.Add(new MySqlParameter("@no", Sno));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public bool UpdReservationInvNum(string invNum, string Sno)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            bool rtn = false;
            sb.Append("update reservation_sheet set ");

            sb.Append("InvoiceNum = @invNum ");

            sb.Append(" where ReservationNo = @no ");
            parms.Add(new MySqlParameter("@invNum", invNum));
            parms.Add(new MySqlParameter("@no", Sno));
            using (dbTConnectionMySQL conn = new dbTConnectionMySQL(this._ConnectionString))
            {
                try
                {
                    rtn = conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    if (!rtn)
                    {
                        conn.Rollback();
                        return false;
                    }
                    else
                    {
                        conn.Commit();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    conn.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public bool CancelReservation(string Rno, string OldStatus)
        {
            DALDispatch dald = new DALDispatch(this._ConnectionString);

            bool rtn = false;
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("UPDATE reservation_sheet SET ProcessStage= 'W'  WHERE ReservationNo = @no and ProcessStage = @old");
            parms.Add(new MySqlParameter("@no", Rno));
            parms.Add(new MySqlParameter("@old", OldStatus));

            using (dbTConnectionMySQL conn = new dbTConnectionMySQL(this._ConnectionString))
            {
                try
                {
                    rtn = conn.executeDeleteQuery(sb.ToString(), parms.ToArray());

                    if (!rtn)
                    {
                        conn.Rollback();
                        return false;
                    }
                    else
                    {
                        List<ODpServiceUnit> List = dald.GetServiceUnit(Rno);
                        if (List != null)
                        {
                            if (List.Count > 1)
                            {
                                conn.Rollback();
                                return false;
                            }

                            sb.Length = 0;
                            parms.Clear();

                            sb.Append("UPDATE dispatch_reservation SET ServiceRemark='W' WHERE DispatchNo = @dno and  ReservationNo = @rno and ServiceRemark = @old");
                            parms.Add(new MySqlParameter("@dno", List[0].DispatchNo));
                            parms.Add(new MySqlParameter("@rno", Rno));
                            parms.Add(new MySqlParameter("@old", List[0].ServiceRemark));

                            rtn = conn.executeDeleteQuery(sb.ToString(), parms.ToArray());

                            if (!rtn)
                            {
                                conn.Rollback();
                                return false;
                            }
                            else
                            {
                                conn.Commit();
                                return true;
                            }
                        }
                        else
                        {
                            conn.Commit();
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    conn.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public bool CancelReservation2(string Rno, string OldStatus)
        {
            DALDispatch dald = new DALDispatch(this._ConnectionString);

            bool rtn = false;
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("UPDATE reservation_sheet SET ProcessStage= 'Y'  WHERE ReservationNo = @no and ProcessStage = @old");
            parms.Add(new MySqlParameter("@no", Rno));
            parms.Add(new MySqlParameter("@old", OldStatus));

            using (dbTConnectionMySQL conn = new dbTConnectionMySQL(this._ConnectionString))
            {
                try
                {
                    rtn = conn.executeDeleteQuery(sb.ToString(), parms.ToArray());

                    if (!rtn)
                    {
                        conn.Rollback();
                        return false;
                    }
                    else
                    {
                        List<ODpServiceUnit> List = dald.GetServiceUnit(Rno);
                        if (List != null)
                        {
                            if (List.Count > 1)
                            {
                                conn.Rollback();
                                return false;
                            }

                            sb.Length = 0;
                            parms.Clear();

                            sb.Append("UPDATE dispatch_reservation SET ServiceRemark='Y' WHERE DispatchNo = @dno and  ReservationNo = @rno and ServiceRemark = @old");
                            parms.Add(new MySqlParameter("@dno", List[0].DispatchNo));
                            parms.Add(new MySqlParameter("@rno", Rno));
                            parms.Add(new MySqlParameter("@old", List[0].ServiceRemark));

                            rtn = conn.executeDeleteQuery(sb.ToString(), parms.ToArray());

                            if (!rtn)
                            {
                                conn.Rollback();
                                return false;
                            }
                            else
                            {
                                conn.Commit();
                                return true;
                            }
                        }
                        else
                        {
                            conn.Commit();
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    conn.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public List<ORsChargeList> GetReservation_Charge_List(string Rno)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();

            sb.Append("select ReservationNo, SelectionType, FlightNo, FlightDate, FlightTime, ServiceRemark, FlightRemark, ActualFlightTime, Price, ChargeResult, CreateTime, UpdTime ");
            sb.Append("from reservation_charge_list where ReservationNo = @no and ServiceRemark != 'X' ");

            parms.Add(new MySqlParameter("@no", Rno));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                List<ORsChargeList> List = new List<ORsChargeList>();

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ORsChargeList su = new ORsChargeList();
                        su.ReservationNo = Convert.ToString(dt.Rows[0]["ReservationNo"]);
                        su.SelectionType = Convert.ToString(dt.Rows[0]["SelectionType"]);
                        if (dt.Rows[0]["FlightNo"] != null)
                            su.FlightNo = Convert.ToString(dt.Rows[0]["FlightNo"]);
                        else
                            su.FlightNo = "";
                        if (dt.Rows[0]["FlightDate"] != null)
                            su.FlightDate = Convert.ToString(dt.Rows[0]["FlightDate"]);
                        else
                            su.FlightDate = "";

                        if (dt.Rows[0]["FlightTime"].ToString() != "")
                            su.FlightTime = Convert.ToDateTime(dt.Rows[0]["FlightTime"]);

                        su.ServiceRemark = Convert.ToString(dt.Rows[0]["ServiceRemark"]);
                        su.ChargeResult = Convert.ToString(dt.Rows[0]["ChargeResult"]);
                        if (dt.Rows[0]["FlightRemark"] != null)
                            su.FlightRemark = Convert.ToString(dt.Rows[0]["FlightRemark"]);
                        else
                            su.FlightRemark = "";

                        if (dt.Rows[0]["ActualFlightTime"].ToString() != "")
                            su.ActualFlightTime = Convert.ToDateTime(dt.Rows[0]["ActualFlightTime"]);


                        su.Price = Convert.ToInt32(dt.Rows[0]["Price"]);


                        List.Add(su);
                    }
                    return List;
                }
                return null;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return null;
            }

        }

        public List<OReservation> GetReservationsForIndex()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT r.ReservationNo, r.TakeDate, CONCAT( LastName ,FirstName) as name , c.Price ");
            sb.Append("FROM reservation_charge_list c, reservation_sheet r, account a where c.ReservationNo = r.ReservationNo and r.UserID = a.UserID and c.Price > 0 and c.ChargeResult <> '1' and r.TakeDate < date_format(now(),'%Y%m%d') ");

            DataTable dt = new DataTable();
            List<OReservation> List = new List<OReservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OReservation r = new OReservation();
                    r.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    r.ServeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                    r.PickupCity = Convert.ToString(dt.Rows[i]["name"]);
                    r.Credit = Convert.ToString(dt.Rows[i]["Price"]);
                    List.Add(r);
                }
                return List;
            }
            else
            {
                return null;
            }
        }

        public List<O24Reservation> Get24HrReservations()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select r.ReservationNo,r.ServiceType, r.TakeDate ,r.TimeSegment,r.FlightNo, ");
            sb.Append("(if (r.ServiceType = 'O',r.PickupAddress,r.TakeoffAddress)) as address,(CONCAT(r.Airport,' ' ,r.Ternimal)) as airsTer,a.UserID,(CONCAT(a.LastName,a.FirstName)) as name ");
            sb.Append("from reservation_sheet r,account a ");
            sb.Append("where r.UserID = a.UserID and r.ProcessStage <> 'X' and r.CreateTime >= date_add(now(), interval - 1 DAY) ");

            DataTable dt = new DataTable();
            List<O24Reservation> List = new List<O24Reservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    O24Reservation r = new O24Reservation();
                    r.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    r.TakeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                    r.name = Convert.ToString(dt.Rows[i]["name"]);
                    r.TimeSegment = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                    r.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                    r.address = Convert.ToString(dt.Rows[i]["address"]);
                    r.airsTer = Convert.ToString(dt.Rows[i]["airsTer"]);
                    r.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                    r.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    List.Add(r);
                }
                return List;
            }
            else
            {
                return null;
            }
        }

        public List<O24Reservation> GetUnReservations()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("select r.ReservationNo,r.ServiceType, r.TakeDate ,r.TimeSegment,r.FlightNo, ");
            sb.Append("(if (r.ServiceType = 'O',r.PickupAddress,r.TakeoffAddress)) as address,(CONCAT(r.Airport,' ' ,r.Ternimal)) as airsTer,a.UserID,(CONCAT(a.LastName,a.FirstName)) as name ");
            sb.Append("from reservation_sheet r,account a ");
            sb.Append("where r.UserID = a.UserID and r.ProcessStage = '0' and r.TakeDate >= date_format(now(), '%y%M%d') union all ");
            sb.Append("select r.ReservationNo,r.ServiceType, r.TakeDate,date_format(d.ScheduleServeTime, '%H%i'),r.FlightNo, ");
            sb.Append("(if (r.ServiceType = 'O',r.PickupAddress,r.TakeoffAddress)) as address,(CONCAT(r.Airport,' ' ,r.Ternimal)) as airsTer ,a.UserID,(CONCAT(a.LastName,a.FirstName)) as name ");
            sb.Append("from reservation_sheet r,account a, dispatch_reservation d ");
            sb.Append("where r.UserID = a.UserID and r.ReservationNo = d.ReservationNo  and r.ProcessStage = 'A' and r.TakeDate >= date_format(now(), '%y%M%d') and d.ScheduleServeTime > now() order by 3,2 ");
            DataTable dt = new DataTable();
            List<O24Reservation> List = new List<O24Reservation>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    O24Reservation r = new O24Reservation();
                    r.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    r.TakeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                    r.name = Convert.ToString(dt.Rows[i]["name"]);
                    r.TimeSegment = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                    r.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                    r.address = Convert.ToString(dt.Rows[i]["address"]);
                    r.airsTer = Convert.ToString(dt.Rows[i]["airsTer"]);
                    r.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                    r.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    List.Add(r);
                }
                return List;
            }
            else
            {
                return null;
            }
        }

        public List<OErrorSheet> GetErrorSheets()
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select ReservationNo, TakeDate ,TimeSegment,passengername,passengerphone  ");
            sb.Append("from reservation_sheet ");
            sb.Append("where takedate in (select matchdate from match_record ) and processstage = '0' order by takedate desc, reservationno desc ");

            DataTable dt = new DataTable();
            List<OErrorSheet> List = new List<OErrorSheet>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OErrorSheet r = new OErrorSheet();
                    r.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                    r.TakeDate = Convert.ToString(dt.Rows[i]["TakeDate"]);
                    r.TimeSegment = Convert.ToString(dt.Rows[i]["TimeSegment"]);
                    r.passengername = Convert.ToString(dt.Rows[i]["passengername"]);
                    r.passengerphone = Convert.ToString(dt.Rows[i]["passengerphone"]);

                    List.Add(r);
                }
                return List;
            }
            else
            {
                return null;
            }
        }

        public bool AddReservation_process_log(string ReservationNo, string ProcessStage, string Memo, string Operator)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("INSERT INTO reservation_process_log (ReservationNo, ProcessStage,Memo,Operator)VALUES (@ReservationNo,@ProcessStage,@Memo,@Operator )");

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@ReservationNo", ReservationNo));
                parms.Add(new MySqlParameter("@ProcessStage", ProcessStage));
                parms.Add(new MySqlParameter("@Memo", Memo));
                parms.Add(new MySqlParameter("@Operator", Operator));
                try
                {
                    conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return true;
        }


        public bool UpReservation_charge_list(string ReservationNo, int RealAmt, string Memo)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("update reservation_charge_list set ");

            sb.Append("RealAmt = @RealAmt , Memo = @Memo  ");

            parms.Add(new MySqlParameter("@RealAmt", RealAmt));
            parms.Add(new MySqlParameter("@Memo", Memo));

            sb.Append(" where ReservationNo = @no ");
            parms.Add(new MySqlParameter("@no", ReservationNo));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public bool UpReservation_charge_list2(string ReservationNo, int RealAmt, string Memo)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("update reservation_charge_list set ");

            sb.Append("RealAmt = @RealAmt , Memo = @Memo,ChargeResult = @Result  ");

            parms.Add(new MySqlParameter("@RealAmt", RealAmt));
            parms.Add(new MySqlParameter("@Memo", Memo));
            parms.Add(new MySqlParameter("@Result", "2"));
            sb.Append(" where ReservationNo = @no ");
            parms.Add(new MySqlParameter("@no", ReservationNo));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public string getMemo(string SNO)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    sb.Append("select Memo from reservation_charge_list  where ReservationNo = @ReservationNo ");
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    parms.Add(new MySqlParameter("@ReservationNo", SNO));

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                    return Convert.ToString(dt.Rows[0]["Memo"]);
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetReport(string coupon, string sdate, string edate)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select reservationno as '編號',servicetype as '出回國',takedate as '乘車日', passengercnt as '人數',coupon as '優惠碼',processstage as '進度', CarpoolFlag as '類別',PickupCity as '城市上',TakeoffCity as '城市下' ,Price as '費用' ");
            sb.Append("from reservation_sheet ");
            sb.Append("where coupon like @coupon and processstage not in ('X', 'W', 'Y') and date_format(createtime,'%Y%m%d') between  @sdate AND @edate ");

            parms.Add(new MySqlParameter("@coupon", "%" + coupon + "%"));
            parms.Add(new MySqlParameter("@sdate", sdate));
            parms.Add(new MySqlParameter("@edate", edate));

            DataTable dt = new DataTable();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                return null;
            }
        }

        public List<ORecev> GetRecevs(string sdate, string edate)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select r.takedate as '乘車日期', r.TimeSegment as '乘車時間', s.DispatchNo as '派車單號',r.ReservationNo as '訂單編號',s.PassengerCnt as '總車人數',r.PassengerCnt as '訂單人數',r.BaggageCnt as '訂單行李數',r.AddBaggageCnt as '臨加行李數',r.Coupon as '優惠碼',c.Price as '小計金額',c.RealAmt as '實收金額',c.ChargeResult as '刷卡結果',r.UpdTime as '最後異動時間',c.Memo as '備註'  ");
            sb.Append("from reservation_charge_list c,dispatch_sheet s, dispatch_reservation d,reservation_sheet r ");
            sb.Append("where r.ReservationNo = c.ReservationNo and r.ReservationNo = d.ReservationNo and d.DispatchNo = s.DispatchNo and r.takedate between @sdate and @edate and r.ProcessStage not in ('0','X','W') ");

            parms.Add(new MySqlParameter("@sdate", sdate));
            parms.Add(new MySqlParameter("@edate", edate));
            DataTable dt = new DataTable();
            List<ORecev> List = new List<ORecev>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ORecev r = new ORecev();
                    r.APassengerCnt = Convert.ToInt32(dt.Rows[i]["總車人數"]);
                    r.PassengerCnt = Convert.ToInt32(dt.Rows[i]["訂單人數"]);
                    r.BaggageCnt = Convert.ToInt32(dt.Rows[i]["訂單行李數"]);
                    r.ABaggageCnt = Convert.ToInt32(dt.Rows[i]["臨加行李數"]);
                    r.DispatchNo = Convert.ToString(dt.Rows[i]["派車單號"]);
                    r.ReservationNo = Convert.ToString(dt.Rows[i]["訂單編號"]);
                    r.takedate = Convert.ToString(dt.Rows[i]["乘車日期"]);
                    r.TimeSegment = Convert.ToString(dt.Rows[i]["乘車時間"]);
                    r.Coupon = Convert.ToString(dt.Rows[i]["優惠碼"]);
                    r.Price = Convert.ToInt32(dt.Rows[i]["小計金額"]);
                    r.realamt = Convert.ToInt32(dt.Rows[i]["實收金額"]);
                    r.ChargeResult = Convert.ToString(dt.Rows[i]["刷卡結果"]);
                    r.UpdTime = Convert.ToDateTime(dt.Rows[0]["最後異動時間"]).ToString();
                    r.Memo = Convert.ToString(dt.Rows[i]["備註"]);

                    List.Add(r);
                }
                return List;
            }
            else
            {
                return null;
            }
        }

        public List<OPay> GetPays(string sdate, string edate)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select r.takedate as '乘車日期', r.TimeSegment as '乘車時間',s.DispatchNo as '派車單號',r.ReservationNo as '訂單編號',  ");
            sb.Append("case r.ServiceType when 'O' then r.PickupAddress else r.TakeoffAddress end as '服務地址',  ");
            sb.Append("s.PassengerCnt as '總車人數',s.StopCnt -1 as '停靠數',s.DispatchTime as '夜間',' ' as '安座',s.CarType as '車型',  ");
            sb.Append("(select c.CompanyName from car_company c where c.CompanyNo = s.FleetID) as '廠商',s.CarNo as '車號',  ");
            sb.Append("(select u.UserName from user_info u where u.UserID = s.DriverID) as '服務人員',' ' as '備註'  ");
            sb.Append("from dispatch_sheet s,dispatch_reservation d,reservation_sheet r ");
            sb.Append("where r.ReservationNo = d.ReservationNo  and d.DispatchNo = s.DispatchNo  and r.takedate between @sdate and @edate and r.ProcessStage not in ('0','X','W') ");

            parms.Add(new MySqlParameter("@sdate", sdate));
            parms.Add(new MySqlParameter("@edate", edate));
            DataTable dt = new DataTable();
            List<OPay> List = new List<OPay>();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OPay r = new OPay();
                    r.takedate = Convert.ToString(dt.Rows[i]["乘車日期"]);
                    r.TimeSegment = Convert.ToString(dt.Rows[i]["乘車時間"]);
                    r.APassengerCnt = Convert.ToInt32(dt.Rows[i]["總車人數"]);
                    r.address = Convert.ToString(dt.Rows[i]["服務地址"]);
                    r.stop = Convert.ToInt32(dt.Rows[i]["停靠數"]);
                    r.night = Convert.ToString(dt.Rows[i]["夜間"]);
                    r.DispatchNo = Convert.ToString(dt.Rows[i]["派車單號"]);
                    r.ReservationNo = Convert.ToString(dt.Rows[i]["訂單編號"]);
                    r.baby = Convert.ToString(dt.Rows[i]["安座"]);
                    r.car = Convert.ToString(dt.Rows[i]["車型"]);
                    r.team = Convert.ToString(dt.Rows[i]["廠商"]);
                    r.carnum = Convert.ToString(dt.Rows[i]["車號"]);
                    r.service = Convert.ToString(dt.Rows[i]["服務人員"]);
                    r.Memo = Convert.ToString(dt.Rows[i]["備註"]);

                    List.Add(r);
                }
                return List;
            }
            else
            {
                return null;
            }
        }
    }

    public class DALFlight : CallcarDAL
    {
        public DALFlight(string ConnectionString) : base(ConnectionString) { }

        /// <summary>
        /// 回傳表定班機資訊
        /// </summary>
        /// <param name="FlightDate">航班日期</param>
        /// <param name="FlightNo">航班編號</param>
        /// <returns></returns>
        public OScheduledFlight GetFlightInfo(string FlightDate, string FlightNo)
        {
            DataTable dt = new DataTable();
            DateTime dtFlightDate = Common.General.ConvertToDateTime(FlightDate, 8);
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                List<MySqlParameter> parms = new List<MySqlParameter>();
                StringBuilder sb = new StringBuilder();
                sb.Append("select FlightType, AirlineID, FlightNo, DepartureAirportID, DepartureTime, ArrivalAirportID, ArrivalTime, FlyDay, Terminal ");
                sb.Append("from flight_schedule where @date between StartDate and EndDate and FlightNo = @no ");

                parms.Add(new MySqlParameter("@date", FlightDate));
                parms.Add(new MySqlParameter("@no", FlightNo));
                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }

                OScheduledFlight fs = new OScheduledFlight();
                if (dt.Rows.Count > 0)
                {

                    string fly = Convert.ToString(dt.Rows[0]["FlyDay"]);
                    int dayWeek = (int)dtFlightDate.DayOfWeek;
                    if (dayWeek == 0 && fly.Substring(6, 1) != "1")
                        return null;
                    else if (fly.Substring(dayWeek - 1, 1) == "0")
                        return null;



                    fs.AirlineID = Convert.ToString(dt.Rows[0]["AirlineID"]).Trim();
                    fs.FlightDate = dtFlightDate.ToString("yyyy/MM/dd");
                    fs.ArrivalAirportID = Convert.ToString(dt.Rows[0]["ArrivalAirportID"]).Trim();
                    fs.ArrivalTime = Convert.ToString(dt.Rows[0]["ArrivalTime"]).Trim();
                    fs.DepartureAirportID = Convert.ToString(dt.Rows[0]["DepartureAirportID"]).Trim();
                    fs.DepartureTime = Convert.ToString(dt.Rows[0]["DepartureTime"]).Trim();
                    fs.FlightNo = FlightNo;
                    if (Convert.ToString(dt.Rows[0]["FlightType"]).Trim() == "PA")
                        fs.FlightType = "到站";
                    else
                        fs.FlightType = "離站";

                    fs.Ternimal = Convert.ToString(dt.Rows[0]["Terminal"]).Trim();
                    return fs;
                }
                else
                    return null;

            }
        }
    }

    /// <summary>
    /// 選車次的DAL
    /// </summary>
    public class DALPickCar : CallcarDAL
    {
        public DALPickCar(string ConnectionString) : base(ConnectionString) { }

        /// <summary>
        /// 回傳尚未滿額之車次
        /// </summary>
        /// <param name="Date">搭車日期</param>
        /// <param name="Time">選擇時段</param>
        /// <param name="Type">出回國</param>
        /// <param name="lat">撿車訂單的緯度</param>
        /// <param name="lng">撿車訂單的經度</param>
        /// <param name="pcnt">撿車訂單的人數</param>
        /// <param name="bccnt">撿車訂單的行李數</param>
        /// <param name="FlightTime">航班時間</param>
        /// <returns></returns>
        public List<OPickCarInfo> GetPickCarsList(string Date, string Time, string Type)
        {
            List<OPickCarInfo> List = new List<OPickCarInfo>();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();

            sb.Append("select distinct d.DispatchNo, d.PassengerCnt, d.BaggageCnt, d.StopCnt, d.MaxFlag, d.FleetID, d.CarType, d.CarNo, d.DriverID,r.ServeSeq,Y(r.LocationGPS) as lat, X(r.LocationGPS) as lng,r.ScheduleServeTime,r.ReservationNo ");
            sb.Append("from dispatch_sheet d,dispatch_reservation r ");
            sb.Append("where  d.ServiceType = @type and  d.ServeDate = @date and  d.TimeSegment = @time ");
            sb.Append("and d.DispatchNo = r.DispatchNo and d.StopCnt < 3 ");
            sb.Append("order by d.DispatchNo, r.ScheduleServeTime ASC ");
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@type", Type));
                parms.Add(new MySqlParameter("@date", Date));
                parms.Add(new MySqlParameter("@time", Time));

                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            if (dt.Rows.Count > 0)
            {
                string DispatchNo = "";
                try
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                        {
                            OPickCarInfo c = new OPickCarInfo();
                            c.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            c.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            c.DriverID = Convert.ToString(dt.Rows[i]["DriverID"]);
                            c.RemainBCapacity = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            c.RemainPCapacity = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            c.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            c.StopCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                            c.MaxFlag = Convert.ToString(dt.Rows[i]["MaxFlag"]);
                            c.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                            c.TakeDate = Date;
                            c.TimeRange = Time;
                            c.ServiceType = Type;


                            OPickCarInfo.DispatchUnit u = new OPickCarInfo.DispatchUnit();
                            u.No = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.SeqNo = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.lat = Convert.ToDouble(dt.Rows[i]["lat"]);
                            u.lng = Convert.ToDouble(dt.Rows[i]["lng"]);
                            c.Reservations.Add(u);

                            List.Add(c);

                        }
                        else
                        {
                            OPickCarInfo.DispatchUnit u = new OPickCarInfo.DispatchUnit();
                            u.No = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.SeqNo = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.lat = Convert.ToDouble(dt.Rows[i]["lat"]);
                            u.lng = Convert.ToDouble(dt.Rows[i]["lng"]);
                            List[List.Count - 1].Reservations.Add(u);
                        }
                        DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return List;

            }
            else
                return null;
        }

    }


    public class DALDispatch : CallcarDAL
    {
        public DALDispatch(string ConnectionString) : base(ConnectionString) { }

        /// <summary>
        /// 取得一筆派車單主檔
        /// </summary>
        /// <param name="No">派車單號</param>
        /// <returns></returns>
        public ODispatchSheet GetDispatchSheetBrief(string No)
        {
            ODispatchSheet sheet = new ODispatchSheet();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select s.DispatchNo,s.ServiceType,s.ServeDate,s.TimeSegment,s.PassengerCnt,s.BaggageCnt,s.StopCnt,s.FleetID,s.CarNo,s.CarType,s.DriverID,u.UserName ");
            sb.Append("from dispatch_sheet s,user_info u  ");
            sb.Append("where s.DriverID = u.UserID and s.DispatchNo = @no");

            parms.Add(new MySqlParameter("@no", No));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                if (dt.Rows.Count > 0)
                {
                    sheet.BaggageCnt = Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                    sheet.CarNo = Convert.ToString(dt.Rows[0]["CarNo"]);
                    sheet.CarType = Convert.ToString(dt.Rows[0]["CarType"]);
                    sheet.DispatchNo = Convert.ToString(dt.Rows[0]["DispatchNo"]);
                    sheet.DriverID = Convert.ToInt32(dt.Rows[0]["DriverID"]);
                    sheet.DriverName = Convert.ToString(dt.Rows[0]["UserName"]);
                    sheet.PassengerCnt = Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                    sheet.ServiceCnt = Convert.ToInt32(dt.Rows[0]["StopCnt"]);
                    sheet.ServiceType = Convert.ToString(dt.Rows[0]["ServiceType"]);
                    sheet.TakeDate = Convert.ToString(dt.Rows[0]["ServeDate"]);
                }
            }
            return sheet;
        }

        /// <summary>
        /// 取得派車單所服務的訂單資料
        /// </summary>
        /// <param name="DNo">派車單號</param>
        /// <returns></returns>
        public List<ODpServiceUnit> GetDispatchService(string DNo)
        {
            List<ODpServiceUnit> List = new List<ODpServiceUnit>();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select d.DispatchNo, d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime,X(d.LocationGPS) as lng ,Y(d.LocationGPS) as lat , d.ServiceRemark,r.ServiceType,r.PickupCity,r.PickupDistinct,r.PickupAddress,");
            sb.Append("r.TakeoffCity,r.TakeoffDistinct,r.TakeoffAddress,r.PassengerCnt,r.BaggageCnt ");
            sb.Append("from dispatch_reservation d,reservation_sheet r  ");
            sb.Append("where d.ReservationNo = r.ReservationNo and  d.DispatchNo = @no Order by d.ServeSeq ASC");

            parms.Add(new MySqlParameter("@no", DNo));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ODpServiceUnit sheet = new ODpServiceUnit();
                        sheet.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                        if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                            sheet.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);

                        sheet.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                        sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                        sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                        sheet.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        sheet.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                        sheet.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                        sheet.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                        sheet.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                        string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                        if (ServiceType == "I")
                        {
                            sheet.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffCity"]) + Convert.ToString(dt.Rows[i]["TakeoffDistinct"]) + Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            sheet.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                        }
                        else
                        {
                            sheet.MainAddress = Convert.ToString(dt.Rows[i]["PickupCity"]) + Convert.ToString(dt.Rows[i]["PickupDistinct"]) + Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            sheet.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                        }
                        List.Add(sheet);
                    }
                }
            }
            return List;
        }

        /// <summary>
        /// 取得司機時段內的派車單
        /// </summary>
        /// <param name="EmpID">司機編號</param>
        /// <param name="StartDate">查詢開始日</param>
        /// <param name="EndDate">查詢結束日</param>
        /// <returns></returns>
        public List<ODispatchSheet> GetUnServeDispatchSheets(int EmpID, string StartDate, string sTime, string EndDate, string eTime)
        {
            List<ODispatchSheet> List = new List<ODispatchSheet>();


            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            parms.Add(new MySqlParameter("@id", EmpID));
            parms.Add(new MySqlParameter("@sdate", StartDate));
            parms.Add(new MySqlParameter("@edate", EndDate));
            parms.Add(new MySqlParameter("@stime", sTime));
            parms.Add(new MySqlParameter("@etime", eTime));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(GetDriverDispatchSQL("UnServed"), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    string DispatchNo = "";

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                        {
                            ODispatchSheet sheet = new ODispatchSheet();
                            sheet.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            sheet.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            sheet.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                            sheet.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            sheet.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
                            sheet.DriverName = Convert.ToString(dt.Rows[i]["UserName"]);
                            sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            sheet.ServiceCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                            sheet.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            sheet.TakeDate = Convert.ToString(dt.Rows[i]["ServeDate"]);
                            sheet.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemarkS"]);
                            sheet.UpdFlag = Convert.ToString(dt.Rows[i]["UpdFlag"]);

                            ODpServiceUnit u = new ODpServiceUnit();

                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);

                            if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                                u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffCity"]) + Convert.ToString(dt.Rows[i]["TakeoffDistinct"]) + Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupCity"]) + Convert.ToString(dt.Rows[i]["PickupDistinct"]) + Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            sheet.ServiceList.Add(u);

                            List.Add(sheet);
                        }
                        else
                        {
                            ODpServiceUnit u = new ODpServiceUnit();

                            if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                                u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffCity"]) + Convert.ToString(dt.Rows[i]["TakeoffDistinct"]) + Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupCity"]) + Convert.ToString(dt.Rows[i]["PickupDistinct"]) + Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            List[List.Count - 1].ServiceList.Add(u);
                        }

                        DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);

                    }
                    return List;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Remark">UnServed / Served / Serving</param>
        /// <returns></returns>
        private string GetDriverDispatchSQL(string Remark)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select s.DispatchNo, s.ServiceType, s.ServeDate, s.PassengerCnt, s.BaggageCnt, s.StopCnt, s.CarType, s.CarNo, s.DriverID, ");
            sb.Append("d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime, X(d.LocationGPS)as lng,Y(d.LocationGPS) as lat, d.ServiceRemark, ");
            sb.Append("u.UserName,r.FlightNo, r.PickupCity, r.PickupDistinct, r.PickupAddress, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffAddress, r.PassengerCnt as PassengerCntR,   r.BaggageCnt as BaggageCntR,s.ServiceRemark as ServiceRemarkS,s.UpdFlag ");
            sb.Append("from dispatch_sheet s,dispatch_reservation d,user_info u,reservation_sheet r ");
            sb.Append("where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo and s.DriverID = u.UserID ");
            sb.Append("and s.DriverID = @id and ((s.ServeDate = @sdate and s.TimeSegment >= @stime ) or ( s.ServeDate = @edate and s.TimeSegment <= @etime))");

            if (Remark == "UnServed")
                sb.Append(" and  s.ServiceRemark not in ('C','F')  ");
            else if (Remark == "Served")
                sb.Append(" and  s.ServiceRemark in ('C','F')  ");
            else if (Remark == "Serving")
                sb.Append(" and  s.ServiceRemark = 'S' ");
            else
                sb.Append(" and  s.ServiceRemark = '0' ");

            sb.Append("Order by s.ServeDate , s.DispatchNo,d.ScheduleServeTime ASC");

            return sb.ToString();
        }

        /// <summary>
        /// 取得完整派車單
        /// </summary>
        /// <param name="Dno">派車單編號</param>
        /// <returns></returns>
        public ODispatchSheet GetDispatchSheets(string Dno)
        {
            List<ODispatchSheet> List = new List<ODispatchSheet>();
            BAL.BALGeneral bal = new BAL.BALGeneral(this._ConnectionString);

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("select s.DispatchNo, s.ServiceType, s.ServeDate,s.TimeSegment, s.PassengerCnt, s.BaggageCnt, s.StopCnt, s.CarType, s.CarNo, s.DriverID, s.FleetID, ");
            sb.Append("d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime, X(d.LocationGPS)as lng,Y(d.LocationGPS) as lat, d.ServiceRemark,r.PassengerName,r.PassengerPhone, ");
            sb.Append("r.TakeDate as AST,r.TimeSegment as RTS,r.ScheduleFlightTime, u.UserName,r.FlightNo, r.PickupCity, r.PickupDistinct, r.PickupAddress, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffAddress, r.PassengerCnt as PassengerCntR, r.BaggageCnt as BaggageCntR  ,s.ServiceRemark as ServiceRemarkS ");
            sb.Append("from dispatch_sheet s,dispatch_reservation d,user_info u,reservation_sheet r ");
            sb.Append("where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo and s.DriverID = u.UserID ");
            sb.Append("and   s.DispatchNo = @no Order by d.ScheduleServeTime  ASC");
            parms.Add(new MySqlParameter("@no", Dno));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    string Reservation = string.Empty;
                    ODispatchSheet sheet = new ODispatchSheet();
                    sheet.BaggageCnt = Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                    sheet.CarNo = Convert.ToString(dt.Rows[0]["CarNo"]);
                    sheet.CarType = Convert.ToString(dt.Rows[0]["CarType"]);
                    sheet.DispatchNo = Convert.ToString(dt.Rows[0]["DispatchNo"]);
                    sheet.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                    sheet.FleetName = bal.GetFleet(sheet.FleetID).FleetName;
                    sheet.DriverID = Convert.ToInt32(dt.Rows[0]["DriverID"]);
                    sheet.DriverName = Convert.ToString(dt.Rows[0]["UserName"]);
                    sheet.PassengerCnt = Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                    sheet.ServiceCnt = Convert.ToInt32(dt.Rows[0]["StopCnt"]);
                    sheet.ServiceType = Convert.ToString(dt.Rows[0]["ServiceType"]);
                    sheet.TakeDate = Convert.ToString(dt.Rows[0]["ServeDate"]);
                    sheet.ServiceRemark = Convert.ToString(dt.Rows[0]["ServiceRemarkS"]);
                    sheet.TimeSegment = Convert.ToString(dt.Rows[0]["TimeSegment"]);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ODpServiceUnit u = new ODpServiceUnit();
                        u.PassengerName = Convert.ToString(dt.Rows[i]["PassengerName"]);
                        u.PassengerPhone = Convert.ToString(dt.Rows[i]["PassengerPhone"]);
                        u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                        u.TimeSegment = Convert.ToString(dt.Rows[i]["RTS"]);
                        if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                            u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                        u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                        u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                        if (dt.Rows[i]["ScheduleFlightTime"] != DBNull.Value)
                            u.ScheduleFlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
                        u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                        u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                        u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                        u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                        u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                        if (sheet.ServiceType == "I")
                        {
                            u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffCity"]) + Convert.ToString(dt.Rows[i]["TakeoffDistinct"]) + Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                        }
                        else
                        {
                            u.MainAddress = Convert.ToString(dt.Rows[i]["PickupCity"]) + Convert.ToString(dt.Rows[i]["PickupDistinct"]) + Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                        }
                        sheet.ServiceList.Add(u);
                    }
                    return sheet;

                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public ODispatchSheet GetDispatchSheetsNoUser(string Dno)
        {
            List<ODispatchSheet> List = new List<ODispatchSheet>();
            BAL.BALGeneral bal = new BAL.BALGeneral(this._ConnectionString);

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("select s.DispatchNo, s.ServiceType, s.ServeDate,s.TimeSegment, s.PassengerCnt, s.BaggageCnt, s.StopCnt, s.CarType, s.CarNo, s.DriverID, s.FleetID, ");
            sb.Append("d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime, X(d.LocationGPS)as lng,Y(d.LocationGPS) as lat, d.ServiceRemark,r.PassengerName,r.PassengerPhone, ");
            sb.Append("r.TakeDate as AST,r.TimeSegment as RTS,r.ScheduleFlightTime, r.FlightNo, r.PickupCity, r.PickupDistinct, r.PickupAddress, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffAddress, r.PassengerCnt as PassengerCntR, r.BaggageCnt as BaggageCntR  ,s.ServiceRemark as ServiceRemarkS ");
            sb.Append(",(select UserName from user_info u Where u.UserID = s.DriverID) as UserName ");
            sb.Append("from dispatch_sheet s,dispatch_reservation d,reservation_sheet r ");
            sb.Append("where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo  ");
            sb.Append("and   s.DispatchNo = @no Order by d.ScheduleServeTime  ASC");
            parms.Add(new MySqlParameter("@no", Dno));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    string Reservation = string.Empty;
                    ODispatchSheet sheet = new ODispatchSheet();
                    sheet.BaggageCnt = Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                    sheet.CarNo = Convert.ToString(dt.Rows[0]["CarNo"]);
                    sheet.CarType = Convert.ToString(dt.Rows[0]["CarType"]);
                    sheet.DispatchNo = Convert.ToString(dt.Rows[0]["DispatchNo"]);
                    sheet.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                    sheet.DriverID = Convert.ToInt32(dt.Rows[0]["DriverID"]);
                    sheet.PassengerCnt = Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                    sheet.ServiceCnt = Convert.ToInt32(dt.Rows[0]["StopCnt"]);
                    sheet.ServiceType = Convert.ToString(dt.Rows[0]["ServiceType"]);
                    sheet.TakeDate = Convert.ToString(dt.Rows[0]["ServeDate"]);
                    sheet.ServiceRemark = Convert.ToString(dt.Rows[0]["ServiceRemarkS"]);
                    sheet.TimeSegment = Convert.ToString(dt.Rows[0]["TimeSegment"]);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ODpServiceUnit u = new ODpServiceUnit();
                        u.PassengerName = Convert.ToString(dt.Rows[i]["PassengerName"]);
                        u.PassengerPhone = Convert.ToString(dt.Rows[i]["PassengerPhone"]);
                        u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCntR"]);
                        u.TimeSegment = Convert.ToString(dt.Rows[i]["RTS"]);
                        if (dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                            u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                        u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                        u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                        if (dt.Rows[i]["ScheduleFlightTime"] != DBNull.Value)
                            u.ScheduleFlightTime = Convert.ToDateTime(dt.Rows[i]["ScheduleFlightTime"]);
                        u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCntR"]);
                        u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                        u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                        u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                        u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                        if (sheet.ServiceType == "I")
                        {
                            u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffCity"]) + Convert.ToString(dt.Rows[i]["TakeoffDistinct"]) + Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                        }
                        else
                        {
                            u.MainAddress = Convert.ToString(dt.Rows[i]["PickupCity"]) + Convert.ToString(dt.Rows[i]["PickupDistinct"]) + Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                        }
                        sheet.ServiceList.Add(u);
                    }
                    return sheet;

                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        /// <summary>
        /// 取得完整派車單
        /// </summary>
        /// <param name="StartDate">查詢開始日</param>
        /// <param name="EndDate">查詢結束日</param>
        /// <returns></returns>
        public List<ODispatchSheet> GetDispatchSheets(string StartDate, string EndDate)
        {
            List<ODispatchSheet> List = new List<ODispatchSheet>();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("select s.DispatchNo, s.ServiceType, s.ServeDate, s.PassengerCnt, s.BaggageCnt, s.StopCnt, s.CarType, s.CarNo, s.DriverID,s.FleetID, ");
            sb.Append("d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime, X(d.LocationGPS)as lat,Y(d.LocationGPS) as lng, d.ServiceRemark, ");
            sb.Append("u.UserName,r.FlightNo, r.PickupCity, r.PickupDistinct, r.PickupAddress, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffAddress, r.PassengerCnt, r.BaggageCnt ");
            sb.Append("from dispatch_sheet s,dispatch_reservation d,user_info u,reservation_sheet r ");
            sb.Append("where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo and s.DriverID = u.UserID ");
            sb.Append("and s.ServeDate between @sdate and @edate and d.ServiceRemark  <> 'X' ");
            sb.Append("Order by d.ScheduleServeTime,s.DispatchNo,d.ServeSeq ASC");
            parms.Add(new MySqlParameter("@sdate", StartDate));
            parms.Add(new MySqlParameter("@edate", EndDate));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    string DispatchNo = "";

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                        {
                            ODispatchSheet sheet = new ODispatchSheet();
                            sheet.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            sheet.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            sheet.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                            sheet.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            sheet.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
                            sheet.DriverName = Convert.ToString(dt.Rows[i]["UserName"]);
                            sheet.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                            sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            sheet.ServiceCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                            sheet.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            sheet.TakeDate = Convert.ToString(dt.Rows[i]["ServeDate"]);
                            ODpServiceUnit u = new ODpServiceUnit();

                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            if (Convert.ToString(dt.Rows[i]["ActualServreTime"]) == "")
                                u.ActualTime = Convert.ToDateTime("2017-03-01");
                            else
                                u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffCity"]) + Convert.ToString(dt.Rows[i]["TakeoffDistinct"]) + Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupCity"]) + Convert.ToString(dt.Rows[i]["PickupDistinct"]) + Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            sheet.ServiceList.Add(u);

                            List.Add(sheet);
                        }
                        else
                        {
                            ODpServiceUnit u = new ODpServiceUnit();

                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            if (Convert.ToString(dt.Rows[i]["ActualServreTime"]) == "")
                                u.ActualTime = Convert.ToDateTime("2017-03-01");
                            else
                                u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffCity"]) + Convert.ToString(dt.Rows[i]["TakeoffDistinct"]) + Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupCity"]) + Convert.ToString(dt.Rows[i]["PickupDistinct"]) + Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            List[List.Count - 1].ServiceList.Add(u);
                        }

                        DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);

                    }
                    return List;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<ODispatchSheet> GetDispatchSheetsbyStage(string StartDate, string EndDate, string Stage, int FleetID)
        {
            List<ODispatchSheet> List = new List<ODispatchSheet>();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("select s.DispatchNo, s.ServiceType, s.ServeDate, s.PassengerCnt as allpct, s.BaggageCnt as allbct, s.StopCnt, s.CarType, s.CarNo, s.DriverID,s.FleetID, ");
            sb.Append("d.ReservationNo, d.ServeSeq, d.ScheduleServeTime, d.ActualServreTime, X(d.LocationGPS)as lat,Y(d.LocationGPS) as lng, rcl.ServiceRemark, ");
            sb.Append("u.UserName,r.FlightNo, r.PickupCity, r.PickupDistinct, r.PickupAddress, r.TakeoffCity, r.TakeoffDistinct, r.TakeoffAddress, r.PassengerCnt, r.BaggageCnt,r.UserID,r.AddBaggageCnt,r.Coupon,r.DiscountType  ");
            sb.Append("from dispatch_sheet s,dispatch_reservation d,user_info u,reservation_sheet r,reservation_charge_list rcl ");
            sb.Append("where s.DispatchNo = d.DispatchNo and d.ReservationNo = r.ReservationNo and s.DriverID = u.UserID and d.ReservationNo = rcl.ReservationNo ");
            sb.Append("and s.ServeDate between @sdate and @edate and d.ServiceRemark  <> 'X' and rcl.ChargeResult <> '1' ");
            parms.Add(new MySqlParameter("@sdate", StartDate));
            parms.Add(new MySqlParameter("@edate", EndDate));
            if (Stage != "")
            {
                sb.Append(" and rcl.ServiceRemark = @Stage ");
                parms.Add(new MySqlParameter("@Stage", Stage));
            }

            if (FleetID != 0)
            {
                sb.Append(" AND s.FleetID = @FleetID");
                parms.Add(new MySqlParameter("@FleetID", FleetID));
            }
            sb.Append(" and r.ProcessStage  <> 'E' ");
            sb.Append("Order by d.ScheduleServeTime,s.DispatchNo,d.ServeSeq ASC");
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    string DispatchNo = "";

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (DispatchNo != Convert.ToString(dt.Rows[i]["DispatchNo"]))
                        {
                            ODispatchSheet sheet = new ODispatchSheet();
                            sheet.BaggageCnt = Convert.ToInt32(dt.Rows[i]["allbct"]);
                            sheet.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                            sheet.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                            sheet.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            sheet.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
                            sheet.DriverName = Convert.ToString(dt.Rows[i]["UserName"]);
                            sheet.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                            sheet.PassengerCnt = Convert.ToInt32(dt.Rows[i]["allpct"]);
                            sheet.ServiceCnt = Convert.ToInt32(dt.Rows[i]["StopCnt"]);
                            sheet.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            sheet.TakeDate = Convert.ToString(dt.Rows[i]["ServeDate"]);

                            ODpServiceUnit u = new ODpServiceUnit();
                            u.uid = Convert.ToString(dt.Rows[i]["UserID"]);
                            if (Convert.ToString(dt.Rows[i]["ActualServreTime"]) == "")
                                u.ActualTime = Convert.ToDateTime("2017-03-01");
                            else
                                u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);
                            u.DiscountType = Convert.ToString(dt.Rows[i]["DiscountType"]);
                            u.Coupon = Convert.ToString(dt.Rows[i]["Coupon"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            if (dt.Rows[i]["lat"].ToString() != "" & dt.Rows[i]["lng"].ToString() != "")
                                u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffCity"]) + Convert.ToString(dt.Rows[i]["TakeoffDistinct"]) + Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupCity"]) + Convert.ToString(dt.Rows[i]["PickupDistinct"]) + Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            sheet.ServiceList.Add(u);

                            List.Add(sheet);
                        }
                        else
                        {
                            ODpServiceUnit u = new ODpServiceUnit();
                            u.uid = Convert.ToString(dt.Rows[i]["UserID"]);
                            if (Convert.ToString(dt.Rows[i]["ActualServreTime"]) == "")
                                u.ActualTime = Convert.ToDateTime("2017-03-01");
                            else
                                u.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);
                            u.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                            u.PassengerCnt = Convert.ToInt32(dt.Rows[i]["PassengerCnt"]);
                            u.BaggageCnt = Convert.ToInt32(dt.Rows[i]["BaggageCnt"]);
                            u.AddBaggageCnt = Convert.ToInt32(dt.Rows[i]["AddBaggageCnt"]);
                            u.Coupon = Convert.ToString(dt.Rows[i]["Coupon"]);
                            u.FlightNo = Convert.ToString(dt.Rows[i]["FlightNo"]);
                            u.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                            u.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                            if (dt.Rows[i]["lat"].ToString() != "" & dt.Rows[i]["lng"].ToString() != "")
                                u.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[i]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                            u.ServiceOrder = Convert.ToInt32(dt.Rows[i]["ServeSeq"]);
                            u.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);

                            string ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                            if (ServiceType == "I")
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["TakeoffCity"]) + Convert.ToString(dt.Rows[i]["TakeoffDistinct"]) + Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["PickupAddress"]);
                            }
                            else
                            {
                                u.MainAddress = Convert.ToString(dt.Rows[i]["PickupCity"]) + Convert.ToString(dt.Rows[i]["PickupDistinct"]) + Convert.ToString(dt.Rows[i]["PickupAddress"]);
                                u.AirportAddress = Convert.ToString(dt.Rows[i]["TakeoffAddress"]);
                            }
                            List[List.Count - 1].ServiceList.Add(u);
                        }

                        DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);

                    }
                    return List;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public List<ODpServiceUnit> GetServiceUnit(string Rno)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();

            sb.Append("select DispatchNo, ReservationNo, ServeSeq, ScheduleServeTime, ActualServreTime, X(LocationGPS) as lng ,Y(LocationGPS) as lat, ServiceRemark, CreateTime, UpdTime ");
            sb.Append("from dispatch_reservation where ReservationNo = @no and ServiceRemark != 'X' ");

            parms.Add(new MySqlParameter("@no", Rno));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
            }
            List<ODpServiceUnit> List = new List<ODpServiceUnit>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ODpServiceUnit su = new ODpServiceUnit();
                    if (dt.Rows[0]["ActualServreTime"] != null && dt.Rows[i]["ActualServreTime"] != DBNull.Value)
                        su.ActualTime = Convert.ToDateTime(dt.Rows[i]["ActualServreTime"]);

                    //su.BaggageCnt = Convert.ToInt32(dt.Rows[0]["BaggageCnt"]);
                    su.DispatchNo = Convert.ToString(dt.Rows[i]["DispatchNo"]);
                    //su.PassengerCnt = Convert.ToInt32(dt.Rows[0]["PassengerCnt"]);
                    su.ReservationNo = Rno;
                    su.ScheduleTime = Convert.ToDateTime(dt.Rows[i]["ScheduleServeTime"]);
                    su.ServiceGPS = new OSpatialGPS(Convert.ToDouble(dt.Rows[0]["lat"]), Convert.ToDouble(dt.Rows[i]["lng"]));
                    su.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                    DALReservation dal = new DALReservation(this._ConnectionString);
                    OReservation or = dal.GetReservation(Rno);
                    if (or.ServiceType == "I")
                    {
                        su.MainAddress = or.TakeoffCity + or.TakeoffDistinct + or.TakeoffAddress;
                        su.AirportAddress = or.PickupCity + or.PickupDistinct + or.PickupAddress;
                    }
                    else
                    {
                        su.MainAddress = or.PickupCity + or.PickupDistinct + or.PickupAddress;
                        su.AirportAddress = or.TakeoffCity + or.TakeoffDistinct + or.TakeoffAddress;
                    }
                    List.Add(su);
                }
                return List;
            }
            return null;
        }

        public List<OGoPaylist> GetGoPaylist(string StartDate, string EndDate, string Stage)
        {
            List<OGoPaylist> List = new List<OGoPaylist>();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("select r.ReservationNo,r.UserID,r.ServiceType,r.takedate,r.PassengerCnt,r.BaggageCnt,r.AddBaggageCnt,r.Coupon,r.Price,rcl.ChargeResult,a.LastName,a.FirstName, rcl.ServiceRemark, ");
            sb.Append("rcl.Price, rcl.RealAmt, rcl.PromoteAmt, rcl.CollectAmt, rcl.TotalAmt, rcl.OfficialTotalAmt, rcl.CarFee, rcl.NightFee, rcl.BaggageFee ,rcl.OtherFee, rcl.OtherSaleFee,r.InvoiceData,r.DiscountType,r.CarpoolFlag,r.VanpoolFlag  ");
            sb.Append("from reservation_sheet r,reservation_charge_list rcl,account	a ");
            sb.Append("where r.ReservationNo = rcl.ReservationNo and r.UserID = a.UserID ");
            sb.Append("and r.takedate between @sdate and @edate and r.ProcessStage  <> 'E' and rcl.ChargeResult <> '1'  and rcl.ChargeStatus ='1' ");
            parms.Add(new MySqlParameter("@sdate", StartDate));
            parms.Add(new MySqlParameter("@edate", EndDate));
            if (Stage != "")
            {
                sb.Append(" and rcl.ServiceRemark = @Stage ");
                parms.Add(new MySqlParameter("@Stage", Stage));
            }

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        OGoPaylist sheet = new OGoPaylist();
                        OGoPaylistUnit g = new OGoPaylistUnit();
                        g.AddBaggageCnt = Convert.ToString(dt.Rows[i]["AddBaggageCnt"]);
                        g.BaggageCnt = Convert.ToString(dt.Rows[i]["BaggageCnt"]);
                        g.ChargeResult = Convert.ToString(dt.Rows[i]["ChargeResult"]);
                        g.DiscountType = Convert.ToString(dt.Rows[i]["DiscountType"]);
                        g.Coupon = Convert.ToString(dt.Rows[i]["Coupon"]);
                        g.InvoiceData = Convert.ToString(dt.Rows[i]["InvoiceData"]);
                        g.PassengerCnt = Convert.ToString(dt.Rows[i]["PassengerCnt"]);
                        g.Price = Convert.ToString(dt.Rows[i]["Price"]);
                        g.ReservationNo = Convert.ToString(dt.Rows[i]["ReservationNo"]);
                        g.ServiceRemark = Convert.ToString(dt.Rows[i]["ServiceRemark"]);
                        g.ServiceType = Convert.ToString(dt.Rows[i]["ServiceType"]);
                        g.takedate = Convert.ToString(dt.Rows[i]["takedate"]);
                        g.UserID = Convert.ToString(dt.Rows[i]["UserID"]);
                        g.UserName = Convert.ToString(dt.Rows[i]["LastName"]) + Convert.ToString(dt.Rows[i]["FirstName"]);

                        g.RealAmt = Convert.ToString(dt.Rows[i]["RealAmt"]);
                        g.PromoteAmt = Convert.ToString(dt.Rows[i]["PromoteAmt"]);
                        g.CollectAmt = Convert.ToString(dt.Rows[i]["CollectAmt"]);
                        g.TotalAmt = Convert.ToString(dt.Rows[i]["TotalAmt"]);
                        g.OfficialTotalAmt = Convert.ToString(dt.Rows[i]["OfficialTotalAmt"]);
                        g.CarFee = Convert.ToString(dt.Rows[i]["CarFee"]);
                        g.NightFee = Convert.ToString(dt.Rows[i]["NightFee"]);
                        g.BaggageFee = Convert.ToString(dt.Rows[i]["BaggageFee"]);
                        g.OtherFee = Convert.ToString(dt.Rows[i]["OtherFee"]);
                        g.OtherSaleFee = Convert.ToString(dt.Rows[i]["OtherSaleFee"]);
                        g.CarpoolFlag = Convert.ToString(dt.Rows[i]["CarpoolFlag"]);
                        g.VanpoolFlag = Convert.ToString(dt.Rows[i]["VanpoolFlag"]);
                        sheet.UnitList.Add(g);
                        List.Add(sheet);

                    }
                    return List;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public string GetDispNumber(string no)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("select PassengerCnt ");
            sb.Append("from dispatch_sheet ");
            sb.Append("where DispatchNo in (select DispatchNo from dispatch_reservation where reservationno = @no) ");
            parms.Add(new MySqlParameter("@no", no));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                if (dt.Rows.Count > 0)
                {
                    string num;
                    num = Convert.ToString(dt.Rows[0]["PassengerCnt"]);
                    return num;
                }
                else
                    return "0";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 儲存媒合紀錄
        /// </summary>
        /// <param name="date"></param>
        /// <param name="Status"></param>
        /// <param name="ID"></param>
        /// <param name="Json"></param>
        /// <param name="ErrMsg"></param>
        /// <returns></returns>
        public bool SaveMatchRecord(string date, string Status, int ID, string Json, string ErrMsg)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("INSERT INTO dispatch_record 	(TakeDate, ExeStatus, MatchupID, Json,ErrorMsg) 	VALUES (@TakeDate,@ExeStatus,@MatchupID,@Json,@ErrorMsg)");
            parms.Add(new MySqlParameter("@TakeDate", date));
            parms.Add(new MySqlParameter("@ExeStatus", Status));
            parms.Add(new MySqlParameter("@MatchupID", ID));
            parms.Add(new MySqlParameter("@Json", Json));
            parms.Add(new MySqlParameter("@ErrorMsg", ErrMsg));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 新增派車單
        /// </summary>
        /// <param name="DispatchUnit"></param>
        /// <returns></returns>
        public bool addDispatchSheet(ODispatchSheet DispatchUnit)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();


            using (dbTConnectionMySQL conn = new dbTConnectionMySQL(this._ConnectionString))
            {
                try
                {
                    //寫入派車預約單檔
                    foreach (ODpServiceUnit u in DispatchUnit.ServiceList)
                    {
                        sb.Length = 0;
                        parms.Clear();
                        sb.Append("INSERT INTO dispatch_reservation ");
                        sb.Append("(DispatchNo, ReservationNo, ScheduleServeTime, LocationGPS,ServiceRemark) ");
                        sb.Append("VALUES (@DispatchNo, @ReservationNo, @ScheduleServeTime,GeomFromText(@LocationGPS), '0') ");
                        parms.Add(new MySqlParameter("@DispatchNo", u.DispatchNo));
                        parms.Add(new MySqlParameter("@ReservationNo", u.ReservationNo));
                        parms.Add(new MySqlParameter("@ScheduleServeTime", u.ScheduleTime));
                        parms.Add(new MySqlParameter("@LocationGPS", "Point(" + u.ServiceGPS.GetLng().ToString() + " " + u.ServiceGPS.GetLat().ToString() + ")"));

                        try
                        {
                            conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                        }
                        catch (MySqlException ex)
                        {
                            conn.Rollback();
                            throw new Exception(ex.Message);
                        }

                        //更新訂單狀態
                        sb.Length = 0;
                        parms.Clear();
                        sb.Append("UPDATE reservation_sheet SET ProcessStage='A'	WHERE ReservationNo = @ReservationNo");
                        parms.Add(new MySqlParameter("@ReservationNo", u.ReservationNo));
                        try
                        {
                            conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                        }
                        catch (Exception ex)
                        {
                            conn.Rollback();
                            throw new Exception(ex.Message);
                        }
                    }

                    //寫入派車檔
                    sb.Length = 0;
                    parms.Clear();
                    sb.Append("INSERT INTO dispatch_sheet ");
                    sb.Append("(DispatchNo, ServiceType, ServeDate, TimeSegment,  PassengerCnt, BaggageCnt, StopCnt, MaxFlag, FleetID, CarType, CarNo, DriverID) ");
                    sb.Append("VALUES (@DispatchNo,@ServiceType,@ServeDate, @TimeSegment,  @PassengerCnt,@BaggageCnt, @StopCnt, @MaxFlag, @FleetID, @CarType, @CarNo, @DriverID) ");
                    parms.Add(new MySqlParameter("@DispatchNo", DispatchUnit.DispatchNo));
                    parms.Add(new MySqlParameter("@ServiceType", DispatchUnit.ServiceType));
                    parms.Add(new MySqlParameter("@ServeDate", DispatchUnit.TakeDate));
                    parms.Add(new MySqlParameter("@TimeSegment", DispatchUnit.TimeSegment));
                    parms.Add(new MySqlParameter("@PassengerCnt", DispatchUnit.PassengerCnt));
                    parms.Add(new MySqlParameter("@BaggageCnt", DispatchUnit.BaggageCnt));
                    parms.Add(new MySqlParameter("@StopCnt", DispatchUnit.ServiceCnt));
                    parms.Add(new MySqlParameter("@MaxFlag", "0"));
                    parms.Add(new MySqlParameter("@FleetID", DispatchUnit.FleetID));
                    parms.Add(new MySqlParameter("@CarType", DispatchUnit.CarType));
                    parms.Add(new MySqlParameter("@CarNo", DispatchUnit.CarNo));
                    parms.Add(new MySqlParameter("@DriverID", DispatchUnit.DriverID));

                    try
                    {
                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                    }
                    catch (Exception ex)
                    {
                        conn.Rollback();
                        throw new Exception(ex.Message);
                    }





                    conn.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    conn.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// 更新服務紀錄
        /// </summary>
        /// <param name="DispatchNo"></param>
        /// <param name="ReservationNo"></param>
        /// <param name="OldRemark"></param>
        /// <param name="NewRemark"></param>
        /// <returns></returns>
        public bool UpdateReservationServiceStatus(ODriverServiceRecord odr)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Update dispatch_sheet Set ServiceRemark = @new , UpdFlag = @flag Where DispatchNo = @dno and DriverID = @id");
            parms.Add(new MySqlParameter("@new", odr.SheetServiceRemark));
            parms.Add(new MySqlParameter("@flag", odr.UpdFlag));
            parms.Add(new MySqlParameter("@dno", odr.Dno));
            parms.Add(new MySqlParameter("@id", odr.DriverID));


            try
            {
                using (dbTConnectionMySQL conn = new dbTConnectionMySQL(this._ConnectionString))
                {
                    if (!conn.executeUpdateQuery(sb.ToString(), parms.ToArray()))
                    {
                        conn.Rollback();
                        return false;
                    }

                    sb.Length = 0;
                    parms.Clear();

                    sb.Append("UPDATE dispatch_reservation SET ActualServreTime = @time,ServiceRemark = @remark WHERE DispatchNo = @dno and ReservationNo = @rno");
                    parms.Add(new MySqlParameter("@time", MySqlDbType.DateTime));
                    parms.Add(new MySqlParameter("@remark", MySqlDbType.String));
                    parms.Add(new MySqlParameter("@dno", MySqlDbType.VarChar));
                    parms.Add(new MySqlParameter("@rno", MySqlDbType.VarChar));

                    if (!string.IsNullOrWhiteSpace(odr.Reservation1))
                    {
                        parms[0].Value = odr.ServiceTime1;
                        parms[1].Value = odr.ServiceRemark1;
                        parms[2].Value = odr.Dno;
                        parms[3].Value = odr.Reservation1;
                        if (!conn.executeUpdateQuery(sb.ToString(), parms.ToArray()))
                        {
                            conn.Rollback();
                            return false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(odr.Reservation2))
                    {
                        parms[0].Value = odr.ServiceTime2;
                        parms[1].Value = odr.ServiceRemark2;
                        parms[2].Value = odr.Dno;
                        parms[3].Value = odr.Reservation2;
                        if (!conn.executeUpdateQuery(sb.ToString(), parms.ToArray()))
                        {
                            conn.Rollback();
                            return false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(odr.Reservation3))
                    {
                        parms[0].Value = odr.ServiceTime3;
                        parms[1].Value = odr.ServiceRemark3;
                        parms[2].Value = odr.Dno;
                        parms[3].Value = odr.Reservation3;
                        if (!conn.executeUpdateQuery(sb.ToString(), parms.ToArray()))
                        {
                            conn.Rollback();
                            return false;
                        }
                    }
                    conn.Commit();
                    return true;

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 取得排班資料
        /// </summary>
        /// <param name="ScheduleID"></param>
        /// <returns></returns>
        public OScheduleData GetScheduleByID(int ScheduleID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("SELECT ScheduleID, ScheduleDate, ShiftType, CarNo, DriverID, FleetID, CarType FROM daily_schedule Where ScheduleID = @id");
            parms.Add(new MySqlParameter("@id", ScheduleID));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


            OScheduleData d = new OScheduleData();
            d.CarNo = Convert.ToString(dt.Rows[0]["CarNo"]);
            d.CarType = Convert.ToString(dt.Rows[0]["CarType"]);
            d.DriverID = Convert.ToInt32(dt.Rows[0]["DriverID"]);
            d.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
            d.ScheduleDate = Convert.ToString(dt.Rows[0]["ScheduleDate"]);
            d.ScheduleID = Convert.ToInt32(dt.Rows[0]["ScheduleID"]);
            d.ShiftType = Convert.ToString(dt.Rows[0]["ShiftType"]);



            return d;
        }

        /// <summary>
        /// 取得排班資料
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public List<OScheduleData> GetScheduleByDate(string date)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("SELECT ScheduleID, ScheduleDate, ShiftType, CarNo, DriverID, FleetID, CarType FROM daily_schedule Where Scheduledate = @date");
            parms.Add(new MySqlParameter("@date", date));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            List<OScheduleData> List = new List<OScheduleData>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                OScheduleData d = new OScheduleData();
                d.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                d.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                d.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
                d.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                d.ScheduleDate = Convert.ToString(dt.Rows[i]["ScheduleDate"]);
                d.ScheduleID = Convert.ToInt32(dt.Rows[i]["ScheduleID"]);
                d.ShiftType = Convert.ToString(dt.Rows[i]["ShiftType"]);
                List.Add(d);
            }

            return List;
        }
        /// <summary>
        /// 複合條件取得排班資料
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public List<OScheduleData> GetScheduleByAll(string date, int DriverID)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            DataTable dt = new DataTable();
            sb.Append("SELECT ScheduleID, ScheduleDate, ShiftType, CarNo, DriverID, FleetID, CarType FROM daily_schedule Where Scheduledate = @date");
            parms.Add(new MySqlParameter("@date", date));
            if (DriverID != 0)
            {
                sb.Append(" And DriverID = @DriverID");
                parms.Add(new MySqlParameter("@DriverID", DriverID));
            }
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            List<OScheduleData> List = new List<OScheduleData>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                OScheduleData d = new OScheduleData();
                d.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                d.CarType = Convert.ToString(dt.Rows[i]["CarType"]);
                d.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);
                d.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                d.ScheduleDate = Convert.ToString(dt.Rows[i]["ScheduleDate"]);
                d.ScheduleID = Convert.ToInt32(dt.Rows[i]["ScheduleID"]);
                d.ShiftType = Convert.ToString(dt.Rows[i]["ShiftType"]);
                List.Add(d);
            }

            return List;
        }

        /// <summary>
        /// 取得第一個服務時間
        /// </summary>
        /// <param name="Dno"></param>
        /// <returns></returns>
        public DateTime GetFirstScheduleServiceTime(string Dno)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select  ScheduleServeTime ");
            sb.Append("from dispatch_reservation ");
            sb.Append("where DispatchNo = @no Order by d.ScheduleServeTime ASC");

            parms.Add(new MySqlParameter("@no", Dno));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                if (dt.Rows.Count > 0)
                    return Convert.ToDateTime(dt.Rows[0]["ScheduleServeTime"]);
                else
                    throw new Exception("派車單號異常");
            }
        }

        /// <summary>
        /// 取得最後一個服務時間
        /// </summary>
        /// <param name="Dno"></param>
        /// <returns></returns>
        public DateTime GetLastScheduleServiceTime(string Dno)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select  ScheduleServeTime ");
            sb.Append("from dispatch_reservation ");
            sb.Append("where DispatchNo = @no Order by ScheduleServeTime DESC");

            parms.Add(new MySqlParameter("@no", Dno));

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                if (dt.Rows.Count > 0)
                    return Convert.ToDateTime(dt.Rows[0]["ScheduleServeTime"]);
                else
                    throw new Exception("派車單號異常");
            }

        }

        /// <summary>
        /// 根據多重條件回傳一批簡易派車單
        /// </summary>
        /// <param name="qs">QDispatchCondStruct</param>
        /// <returns></returns>
        // Callcar.DAL.DALDispatch
        public List<ODispatchSheet> GetDispatchMultipleCondition(QDispatchCondStruct qs, int take, int skip)
        {
            StringBuilder stringBuilder = new StringBuilder();
            List<MySqlParameter> list = new List<MySqlParameter>();
            stringBuilder.Append("SELECT Distinct d.DispatchNo, d.ServiceType, d.ServeDate, d.TimeSegment,d.CarpoolFlag,d.CanPick,  d.DispatchTime, d.PassengerCnt, d.BaggageCnt, d.StopCnt, d.MaxFlag, d.FleetID, d.CarType, d.CarNo, d.DriverID ");
            stringBuilder.Append("FROM dispatch_sheet d join dispatch_reservation r on d.DispatchNo = r.DispatchNo  ");
            stringBuilder.Append(" where d.ServeDate between @sdate and @edate ");
            list.Add(new MySqlParameter("@sdate", qs.QSDate));
            list.Add(new MySqlParameter("@edate", qs.QEDate));
            if (qs.DriverID > 0)
            {
                stringBuilder.Append(" And d.DriverID = @id ");
                list.Add(new MySqlParameter("@id", qs.DriverID));
            }
            if (qs.CarNo != "")
            {
                stringBuilder.Append(" And d.CarNo = @cno ");
                list.Add(new MySqlParameter("@cno", qs.CarNo));
            }
            if (qs.DispatchNo != "")
            {
                stringBuilder.Append(" And d.DispatchNo = @dno ");
                list.Add(new MySqlParameter("@dno", qs.DispatchNo));
            }
            if (qs.OrderNo != "")
            {
                stringBuilder.Append(" And r.ReservationNo = @rno ");
                list.Add(new MySqlParameter("@rno", qs.OrderNo));
            }
            stringBuilder.Append(" order by  d.ServeDate,d.DispatchNo");
            DataTable dataTable = new DataTable();
            List<ODispatchSheet> list2 = new List<ODispatchSheet>();
            try
            {
                using (dbConnectionMySQL dbConnectionMySQL = new dbConnectionMySQL(this._ConnectionString))
                {
                    dataTable = dbConnectionMySQL.executeSelectQuery(stringBuilder.ToString(), list.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (dataTable.Rows.Count > 0)
            {
                CallCar.BAL.BALEmployee bALEmployee = new CallCar.BAL.BALEmployee(this._ConnectionString);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    ODispatchSheet oDispatchSheet = new ODispatchSheet();
                    oDispatchSheet.BaggageCnt = Convert.ToInt32(dataTable.Rows[i]["BaggageCnt"]);
                    oDispatchSheet.DispatchNo = Convert.ToString(dataTable.Rows[i]["DispatchNo"]);
                    oDispatchSheet.TakeDate = Convert.ToString(dataTable.Rows[i]["ServeDate"]);
                    oDispatchSheet.ServiceType = Convert.ToString(dataTable.Rows[i]["ServiceType"]);
                    oDispatchSheet.CarpoolFlag = Convert.ToString(dataTable.Rows[i]["CarpoolFlag"]);
                    oDispatchSheet.CanPick = Convert.ToString(dataTable.Rows[i]["CanPick"]);
                    oDispatchSheet.TimeSegment = Convert.ToString(dataTable.Rows[i]["TimeSegment"]);
                    oDispatchSheet.FisrtServiceTime = Convert.ToString(dataTable.Rows[i]["DispatchTime"]);
                    oDispatchSheet.PassengerCnt = Convert.ToInt32(dataTable.Rows[i]["PassengerCnt"]);
                    oDispatchSheet.ServiceCnt = Convert.ToInt32(dataTable.Rows[i]["StopCnt"]);
                    if (!DBNull.Value.Equals(dataTable.Rows[i]["FleetID"]))
                        oDispatchSheet.FleetID = Convert.ToInt32(dataTable.Rows[i]["FleetID"]);
                    else
                        oDispatchSheet.FleetID = 0;
                    oDispatchSheet.CarType = Convert.ToString(dataTable.Rows[i]["CarType"]);
                    oDispatchSheet.CarNo = Convert.ToString(dataTable.Rows[i]["CarNo"]);

                    if (!DBNull.Value.Equals(dataTable.Rows[i]["DriverID"]))
                    {
                        oDispatchSheet.DriverID = Convert.ToInt32(dataTable.Rows[i]["DriverID"]);
                        if (oDispatchSheet.DriverID > 0)
                        {
                            OEmployee employee = bALEmployee.GetEmployee(oDispatchSheet.DriverID);
                            oDispatchSheet.DriverName = employee.Name;
                        }
                        else
                        {
                            oDispatchSheet.DriverName = "";
                        }
                    }
                    else
                    {
                        oDispatchSheet.DriverName = "";
                    }
                    list2.Add(oDispatchSheet);
                }
                return list2;
            }
            return null;
        }

        public ODispatchSheet GetDispatchbyid(string id)
        {
            StringBuilder stringBuilder = new StringBuilder();
            List<MySqlParameter> list = new List<MySqlParameter>();
            stringBuilder.Append("SELECT Distinct d.DispatchNo, d.ServiceType, d.ServeDate, d.TimeSegment, d.DispatchTime, d.PassengerCnt, d.BaggageCnt, d.StopCnt, d.MaxFlag, d.FleetID, d.CarType, d.CarNo, d.DriverID ");
            stringBuilder.Append("FROM dispatch_sheet d   ");
            stringBuilder.Append(" where d.DispatchNo = @DispatchNo ");
            list.Add(new MySqlParameter("@DispatchNo", id));

            DataTable dataTable = new DataTable();
            List<ODispatchSheet> list2 = new List<ODispatchSheet>();
            try
            {
                using (dbConnectionMySQL dbConnectionMySQL = new dbConnectionMySQL(this._ConnectionString))
                {
                    dataTable = dbConnectionMySQL.executeSelectQuery(stringBuilder.ToString(), list.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (dataTable.Rows.Count > 0)
            {
                CallCar.BAL.BALEmployee bALEmployee = new CallCar.BAL.BALEmployee(this._ConnectionString);
                ODispatchSheet oDispatchSheet = new ODispatchSheet();
                oDispatchSheet.BaggageCnt = Convert.ToInt32(dataTable.Rows[0]["BaggageCnt"]);
                oDispatchSheet.DispatchNo = Convert.ToString(dataTable.Rows[0]["DispatchNo"]);
                oDispatchSheet.TakeDate = Convert.ToString(dataTable.Rows[0]["ServeDate"]);
                oDispatchSheet.ServiceType = Convert.ToString(dataTable.Rows[0]["ServiceType"]);
                oDispatchSheet.TimeSegment = Convert.ToString(dataTable.Rows[0]["TimeSegment"]);
                oDispatchSheet.FisrtServiceTime = Convert.ToString(dataTable.Rows[0]["DispatchTime"]);
                oDispatchSheet.PassengerCnt = Convert.ToInt32(dataTable.Rows[0]["PassengerCnt"]);
                oDispatchSheet.ServiceCnt = Convert.ToInt32(dataTable.Rows[0]["StopCnt"]);
                oDispatchSheet.FleetID = Convert.ToInt32(dataTable.Rows[0]["FleetID"]);
                oDispatchSheet.CarType = Convert.ToString(dataTable.Rows[0]["CarType"]);
                oDispatchSheet.CarNo = Convert.ToString(dataTable.Rows[0]["CarNo"]);
                oDispatchSheet.DriverID = Convert.ToInt32(dataTable.Rows[0]["DriverID"]);
                if (oDispatchSheet.DriverID > 0)
                {
                    OEmployee employee = bALEmployee.GetEmployee(oDispatchSheet.DriverID);
                    oDispatchSheet.DriverName = employee.Name;
                }
                else
                {
                    oDispatchSheet.DriverName = "";
                }

                return oDispatchSheet;
            }
            return null;
        }

        /// <summary>
        /// 更換派遣的司機
        /// </summary>
        /// <param name="Dno"></param>
        /// <param name="oFleetID"></param>
        /// <param name="oCarNo"></param>
        /// <param name="oEmpID"></param>
        /// <param name="nFleetID"></param>
        /// <param name="nCarNo"></param>
        /// <param name="nEmpID"></param>
        /// <param name="nCarType"></param>
        /// <returns></returns>
        public bool UpdDispatchCarDriver(string Dno, string oFleetID, string oCarNo, int oEmpID, string nFleetID, string nCarNo, int nEmpID, string nCarType)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Update dispatch_sheet SET FleetID= @nFleet,CarType=@Type,	CarNo=@nCarNo,DriverID= @nEmpID ");
            sb.Append("WHERE DispatchNo = @no  and FleetID=@oFleet and CarNo= @oCarNo and DriverID=@oEmpID");
            parms.Add(new MySqlParameter("@nFleet", nFleetID));
            parms.Add(new MySqlParameter("@Type", nCarType));
            parms.Add(new MySqlParameter("@nCarNo", nCarNo));
            parms.Add(new MySqlParameter("@nEmpID", nEmpID));
            parms.Add(new MySqlParameter("@no", Dno));
            parms.Add(new MySqlParameter("@oFleet", oFleetID));
            parms.Add(new MySqlParameter("@oCarNo", oCarNo));
            parms.Add(new MySqlParameter("@oEmpID", oEmpID));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    return conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 異動排班資料
        /// </summary>
        /// <returns></returns>
        public bool Upddaily_schedule(string ScheduleID, string ScheduleDate, string CarNo, string ShiftType, int DriverID, int FleetID, string CarType)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Update daily_schedule ");
            sb.Append("SET ScheduleDate=@ScheduleDate,	CarNo=@CarNo,ShiftType= @ShiftType,DriverID= @DriverID ,FleetID= @FleetID ,CarType= @CarType  ");
            sb.Append("WHERE ScheduleID = @ScheduleID ");
            parms.Add(new MySqlParameter("@ScheduleID", ScheduleID));
            parms.Add(new MySqlParameter("@ScheduleDate", ScheduleDate));
            parms.Add(new MySqlParameter("@CarNo", CarNo));
            parms.Add(new MySqlParameter("@ShiftType", ShiftType));
            parms.Add(new MySqlParameter("@DriverID", DriverID));
            parms.Add(new MySqlParameter("@FleetID", FleetID));
            parms.Add(new MySqlParameter("@CarType", CarType));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<ODispatchSheet> GetDispatchSheetForIndex()
        {
            ODispatchSheet sheet = new ODispatchSheet();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select DispatchNo,ServiceType,DispatchTime,CarType ");
            sb.Append("from dispatch_sheet  ");
            sb.Append("where servedate >= date_format(now(),'%Y%m%d') and driverid = 0 ");
            DataTable dataTable = new DataTable();
            List<ODispatchSheet> list2 = new List<ODispatchSheet>();
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                dataTable = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

            }
            if (dataTable.Rows.Count > 0)
            {
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    ODispatchSheet oDispatchSheet = new ODispatchSheet();
                    oDispatchSheet.TakeDate = Convert.ToString(dataTable.Rows[i]["DispatchTime"]);
                    oDispatchSheet.CarType = Convert.ToString(dataTable.Rows[i]["CarType"]);
                    oDispatchSheet.DispatchNo = Convert.ToString(dataTable.Rows[i]["DispatchNo"]);
                    oDispatchSheet.ServiceType = Convert.ToString(dataTable.Rows[i]["ServiceType"]);
                    list2.Add(oDispatchSheet);
                }
                return list2;
            }
            return null;
        }

        public List<ODispatchSheet> GetDispatchSheet3DForIndex()
        {
            ODispatchSheet sheet = new ODispatchSheet();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select d.DispatchNo,d.ServiceType,d.ServeDate,d.DispatchTime,f.FleetName,u.UserName,d.CarNo,d.ServiceRemark ");
            sb.Append("from dispatch_sheet d, user_info u,fleet_info f  ");
            sb.Append("where d.DriverID = u.UserID and d.FleetID = f.FleetID and d.servedate >= date_format(now(),'%Y%m%d') and d.driverid <> 0 ");
            DataTable dataTable = new DataTable();
            List<ODispatchSheet> list2 = new List<ODispatchSheet>();
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                dataTable = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

            }
            if (dataTable.Rows.Count > 0)
            {
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    ODispatchSheet oDispatchSheet = new ODispatchSheet();
                    oDispatchSheet.FisrtServiceTime = Convert.ToString(dataTable.Rows[i]["ServeDate"]);
                    oDispatchSheet.TakeDate = Convert.ToString(dataTable.Rows[i]["DispatchTime"]);
                    oDispatchSheet.CarNo = Convert.ToString(dataTable.Rows[i]["CarNo"]);
                    oDispatchSheet.DispatchNo = Convert.ToString(dataTable.Rows[i]["DispatchNo"]);
                    oDispatchSheet.ServiceType = Convert.ToString(dataTable.Rows[i]["ServiceType"]);
                    oDispatchSheet.ServiceRemark = Convert.ToString(dataTable.Rows[i]["ServiceRemark"]);
                    oDispatchSheet.CarType = Convert.ToString(dataTable.Rows[i]["FleetName"]);
                    oDispatchSheet.DriverName = Convert.ToString(dataTable.Rows[i]["UserName"]);
                    list2.Add(oDispatchSheet);
                }
                return list2;
            }
            return null;
        }

    }


    /// <summary>
    /// 其他的DAL
    /// </summary>
    public class DALGeneral : CallcarDAL
    {
        public DALGeneral(string ConnectionString) : base(ConnectionString) { }

        public OHoliday GetHoliday(string date)
        {
            try
            {
                DataTable dt = new DataTable();
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    List<MySqlParameter> parms = new List<MySqlParameter>();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT HolidayName, StartDate, EndDate, LastRStartDate FROM holiday where @date between StartDate and EndDate");


                    parms.Add(new MySqlParameter("@date", date));
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }

                if (dt.Rows.Count > 0)
                {

                    OHoliday h = new OHoliday();
                    h.HolidayName = Convert.ToString(dt.Rows[0]["HolidayName"]);
                    h.StartDate = Convert.ToString(dt.Rows[0]["StartDate"]);
                    h.EndDate = Convert.ToString(dt.Rows[0]["EndDate"]);
                    h.LastRDate = Convert.ToString(dt.Rows[0]["LastRDate"]);
                    return h;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OFare> GetFareTable(string date)
        {
            List<OFare> List = new List<OFare>();
            DataTable dt = new DataTable();
            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                StringBuilder sb = new StringBuilder();
                List<MySqlParameter> parms = new List<MySqlParameter>();
                sb.Append("SELECT  ChargeCode, StartDate, EndDate, ChargeName, FeeTableID, ChargeType, FlowType, MinQty, Currency, Amount  FROM charge_item where @date between StartDate and EndDate order by ChargeType ASC");

                parms.Add(new MySqlParameter("@date", date));
                try
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OFare f = new OFare();
                    f.Amount = Convert.ToInt32(dt.Rows[i]["Amount"]);
                    f.ChargeCode = Convert.ToString(dt.Rows[i]["ChargeCode"]);
                    f.StartDate = Convert.ToString(dt.Rows[i]["StartDate"]);
                    f.EndDate = Convert.ToString(dt.Rows[i]["EndDate"]);
                    f.ChargeName = Convert.ToString(dt.Rows[i]["ChargeName"]);
                    f.ChargeType = Convert.ToString(dt.Rows[i]["ChargeType"]);
                    f.FlowType = Convert.ToString(dt.Rows[i]["FlowType"]);
                    f.MinQty = Convert.ToInt32(dt.Rows[i]["MinQty"]);
                    f.Currency = Convert.ToString(dt.Rows[i]["Currency"]);
                    List.Add(f);
                }
                return List;
            }
            else
                return null;
        }

        public string GetDistinctServiceStatus(string CityName, string DistinctName)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select ServiceStatus from city_distinct_data where CityName = @city and DistinctName = @dict");
            parms.Add(new MySqlParameter("@city", CityName));
            parms.Add(new MySqlParameter("@dict", DistinctName));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {

                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    if (dt.Rows.Count > 0)
                    {
                        return Convert.ToString(dt.Rows[0]["ServiceStatus"]);
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception
                (ex.Message);
            }
        }

        public string GetVillageServiceStatus(string CityName, string DistinctName, string VillageName)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select r.ServiceStatus,r.MinReserveCnt from city_distinct_data d,service_range_data r ");
            sb.Append("where d.DistinctID = r.DistinctID and  d.CityName = @city and d.DistinctName = @dict and r.VillageName = @vil ");
            parms.Add(new MySqlParameter("@city", CityName));
            parms.Add(new MySqlParameter("@dict", DistinctName));
            parms.Add(new MySqlParameter("@vil", VillageName));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    if (dt.Rows.Count > 0)
                    {
                        return Convert.ToString(dt.Rows[0]["ServiceStatus"]) + "," + Convert.ToString(dt.Rows[0]["MinReserveCnt"]);
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception
                (ex.Message);
            }
        }

        public List<OFleet> GetFleetList()
        {
            List<OFleet> List = new List<OFleet>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("	SELECT FleetID, FleetName,CompanyNo, ActiveFlag,CreateTime,UpdTime 	FROM fleet_info");

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OFleet f = new OFleet();
                    f.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    f.FleetName = Convert.ToString(dt.Rows[i]["FleetName"]);
                    f.Company = Convert.ToString(dt.Rows[i]["CompanyNo"]);
                    f.ActiveFlag = Convert.ToBoolean(dt.Rows[i]["ActiveFlag"]);
                    f.CreateTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    f.UpdTime = Convert.ToDateTime(dt.Rows[i]["UpdTime"]);
                    List.Add(f);
                }

                return List;
            }
            else
                return null;
        }

        public OFleet GetFleet(int FleetID)
        {
            List<OFleet> List = new List<OFleet>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("	SELECT FleetID, FleetName, CompanyNo, ActiveFlag,CreateTime,UpdTime 	FROM fleet_info Where FleetID = @ID");
            parms.Add(new MySqlParameter("@ID", FleetID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {

                OFleet f = new OFleet();
                f.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                f.FleetName = Convert.ToString(dt.Rows[0]["FleetName"]);
                f.Company = Convert.ToString(dt.Rows[0]["CompanyNo"]);
                f.ActiveFlag = Convert.ToBoolean(dt.Rows[0]["ActiveFlag"]);
                f.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                f.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);

                return f;
            }
            else
                return null;
        }

        public OFleet GetFleet(string carteam)
        {
            List<OFleet> List = new List<OFleet>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("	SELECT FleetID, CompanyNo, FleetName, ActiveFlag,CreateTime,UpdTime 	FROM fleet_info Where FleetName like @carteam");
            parms.Add(new MySqlParameter("@carteam", "%" + carteam + "%"));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {

                OFleet f = new OFleet();
                f.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                f.Company = Convert.ToString(dt.Rows[0]["CompanyNo"]);
                f.FleetName = Convert.ToString(dt.Rows[0]["FleetName"]);
                f.ActiveFlag = Convert.ToBoolean(dt.Rows[0]["ActiveFlag"]);
                f.CreateTime = Convert.ToDateTime(dt.Rows[0]["CreateTime"]);
                f.UpdTime = Convert.ToDateTime(dt.Rows[0]["UpdTime"]);

                return f;
            }
            else
                return null;
        }

        public List<OCar> GetCarsByFleet(string FleetID)
        {
            List<OCar> List = new List<OCar>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT CarNo, FleetID, CarType, Brand, Model, ManufactureDate, Color, Displacement, DriverID, ScheduleFlag, CreateTime, UpdTime ");
            sb.Append("FROM car_info where FleetID = @id");

            parms.Add(new MySqlParameter("@id", FleetID));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OCar f = new OCar();
                    f.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    f.Brand = Convert.ToString(dt.Rows[i]["Brand"]);
                    f.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                    f.Color = Convert.ToString(dt.Rows[i]["Color"]);
                    f.Displacement = Convert.ToString(dt.Rows[i]["Displacement"]);
                    f.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);

                    f.MDate = Convert.ToString(dt.Rows[i]["ManufactureDate"]);
                    f.Model = Convert.ToString(dt.Rows[i]["Model"]);
                    f.ScheduleFlag = Convert.ToString(dt.Rows[i]["ScheduleFlag"]);
                    f.Type = Convert.ToString(dt.Rows[i]["CarType"]);
                    f.CreateTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    f.UpdTime = Convert.ToDateTime(dt.Rows[i]["UpdTime"]);
                    List.Add(f);
                }

                return List;
            }
            else
                return null;
        }

        public List<OEmployee> GetDriversByFleet(string FleetID)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            List<OEmployee> empList = new List<OEmployee>();

            sb.Append("select UserID, UserName,ActiveFlag  from user_info  Where FleetID = @id ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@id", FleetID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OEmployee emp = new OEmployee();
                    emp.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    emp.Name = Convert.ToString(dt.Rows[0]["UserName"]);
                    emp.Active = Convert.ToString(dt.Rows[0]["ActiveFlag"]);

                    empList.Add(emp);
                }
            }
            else
                return null;

            return empList;


        }

        public string Re_Charge(string rno)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("	SELECT ReservationNo, ChargeErrMsg 	FROM reservation_charge_list Where ReservationNo = @ReservationNo");
            parms.Add(new MySqlParameter("@ReservationNo", rno));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    if (dt.Rows.Count > 0)
                    {
                        return Convert.ToString(dt.Rows[0]["ChargeErrMsg"]);
                    }
                    else
                        return "";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public string Re_Charge_Data(string rno)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("	SELECT ChargeData FROM reservation_charge_list Where ReservationNo = @ReservationNo");
            parms.Add(new MySqlParameter("@ReservationNo", rno));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    if (dt.Rows.Count > 0)
                    {
                        return Convert.ToString(dt.Rows[0]["ChargeData"]);
                    }
                    else
                        return "";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public bool UpdateRe_Charge(string RNo, string result, string ErrMsg)
        {

            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE reservation_charge_list  ");
            sb.Append("set ChargeResult = @ChargeResult , ChargeErrMsg = @ChargeErrMsg  ");
            sb.Append("where  ReservationNo = @ReservationNo ");

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@ReservationNo", RNo));
                parms.Add(new MySqlParameter("@ChargeResult", result));
                parms.Add(new MySqlParameter("@ChargeErrMsg", ErrMsg));


                try
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                return true;

            }
        }

        public bool InsertRe_Charge_Data(string RNo, string result)
        {

            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE reservation_charge_list  ");
            sb.Append("set ChargeData = @result ");
            sb.Append("where  ReservationNo = @ReservationNo ");

            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@ReservationNo", RNo));
                parms.Add(new MySqlParameter("@result", result));


                try
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                return true;

            }
        }

        public bool CreateNewCarTeam(string id, string nm, string cno, bool acf)
        {

            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO fleet_info  ");
            sb.Append("(FleetID,CompanyNo, FleetName, ActiveFlag) ");
            sb.Append("VALUES (@FleetID,@CompanyNo, @FleetName, @ActiveFlag)  ");

            using (dbTConnectionMySQL conn = new dbTConnectionMySQL(this._ConnectionString))
            {
                parms.Add(new MySqlParameter("@FleetID", id));
                parms.Add(new MySqlParameter("@CompanyNo", cno));
                parms.Add(new MySqlParameter("@FleetName", nm));
                if (acf == true)
                    parms.Add(new MySqlParameter("@ActiveFlag", 1));
                else
                    parms.Add(new MySqlParameter("@ActiveFlag", 0));
                bool rtn = false;
                try
                {
                    rtn = conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
                catch (MySqlException ex)
                {
                    conn.Rollback();
                    throw new Exception(ex.Message);
                }
                catch (Exception ex)
                {
                    conn.Rollback();
                    throw new Exception(ex.Message);
                }

                if (rtn)
                {
                    conn.Commit();
                    return true;
                }
                else
                {
                    conn.Rollback();
                    return false;
                }
            }
        }

        public List<OCar> GetCarsByFleetAndDriver(string FleetID, string DriverID)
        {
            List<OCar> List = new List<OCar>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT CarNo, FleetID, CarType, Brand, Model, ManufactureDate, Color, Displacement, DriverID, ScheduleFlag, CreateTime, UpdTime ");
            sb.Append("FROM car_info where CarNo<>'0' ");
            if (FleetID != "")
            {
                sb.Append("And FleetID = @FleetID");
                parms.Add(new MySqlParameter("@FleetID", FleetID));
            }
            if (DriverID != "")
            {
                sb.Append("And DriverID = @DriverID");
                parms.Add(new MySqlParameter("@DriverID", DriverID));
            }
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OCar f = new OCar();
                    f.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    f.Brand = Convert.ToString(dt.Rows[i]["Brand"]);
                    f.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                    f.Color = Convert.ToString(dt.Rows[i]["Color"]);
                    f.Displacement = Convert.ToString(dt.Rows[i]["Displacement"]);
                    f.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);

                    f.MDate = Convert.ToString(dt.Rows[i]["ManufactureDate"]);
                    f.Model = Convert.ToString(dt.Rows[i]["Model"]);
                    f.ScheduleFlag = Convert.ToString(dt.Rows[i]["ScheduleFlag"]);
                    f.Type = Convert.ToString(dt.Rows[i]["CarType"]);
                    f.CreateTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    f.UpdTime = Convert.ToDateTime(dt.Rows[i]["UpdTime"]);
                    List.Add(f);
                }

                return List;
            }
            else
                return null;
        }

        public string GetDriversByID(int DriverID)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            List<OEmployee> empList = new List<OEmployee>();

            sb.Append("select UserID, UserName,ActiveFlag  from user_info  Where UserID = @id ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@id", DriverID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            string empname = "";
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    empname = Convert.ToString(dt.Rows[0]["UserName"]);
                }
            }
            else
                return null;

            return empname;


        }

        public int GetDriverIDByName(string Driver)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            List<OEmployee> empList = new List<OEmployee>();

            sb.Append("select UserID, UserName,ActiveFlag  from user_info  Where UserName = @id ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@id", Driver));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            int empname = 0;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    empname = Convert.ToInt32(dt.Rows[0]["UserID"]);
                }
            }
            else
                return 0;

            return empname;


        }

        public List<OCar> GetCarsByCarNo(string CarNo)
        {
            List<OCar> List = new List<OCar>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT CarNo, FleetID, CarType, Brand, Model, ManufactureDate, Color, Displacement, DriverID, ScheduleFlag, CreateTime, UpdTime ");
            sb.Append("FROM car_info where CarNo = @id");

            parms.Add(new MySqlParameter("@id", CarNo));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OCar f = new OCar();
                    f.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    f.Brand = Convert.ToString(dt.Rows[i]["Brand"]);
                    f.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                    f.Color = Convert.ToString(dt.Rows[i]["Color"]);
                    f.Displacement = Convert.ToString(dt.Rows[i]["Displacement"]);
                    f.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);

                    f.MDate = Convert.ToString(dt.Rows[i]["ManufactureDate"]);
                    f.Model = Convert.ToString(dt.Rows[i]["Model"]);
                    f.ScheduleFlag = Convert.ToString(dt.Rows[i]["ScheduleFlag"]);
                    f.Type = Convert.ToString(dt.Rows[i]["CarType"]);
                    f.CreateTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    f.UpdTime = Convert.ToDateTime(dt.Rows[i]["UpdTime"]);
                    List.Add(f);
                }

                return List;
            }
            else
                return null;
        }

        public List<OCar> GetCarsByDriverID(int DriverID)
        {
            List<OCar> List = new List<OCar>();
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT CarNo, FleetID, CarType, Brand, Model, ManufactureDate, Color, Displacement, DriverID, ScheduleFlag, CreateTime, UpdTime ");
            sb.Append("FROM car_info where DriverID = @id");

            parms.Add(new MySqlParameter("@id", DriverID));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OCar f = new OCar();
                    f.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    f.Brand = Convert.ToString(dt.Rows[i]["Brand"]);
                    f.CarNo = Convert.ToString(dt.Rows[i]["CarNo"]);
                    f.Color = Convert.ToString(dt.Rows[i]["Color"]);
                    f.Displacement = Convert.ToString(dt.Rows[i]["Displacement"]);
                    f.DriverID = Convert.ToInt32(dt.Rows[i]["DriverID"]);

                    f.MDate = Convert.ToString(dt.Rows[i]["ManufactureDate"]);
                    f.Model = Convert.ToString(dt.Rows[i]["Model"]);
                    f.ScheduleFlag = Convert.ToString(dt.Rows[i]["ScheduleFlag"]);
                    f.Type = Convert.ToString(dt.Rows[i]["CarType"]);
                    f.CreateTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    f.UpdTime = Convert.ToDateTime(dt.Rows[i]["UpdTime"]);
                    List.Add(f);
                }

                return List;
            }
            else
                return null;
        }
        public bool UpdateCar_info(string id, string FleetID, string CarType, string Brand, string ManufactureDate, string Model, string Color, string Displacement, string DriverID, string ScheduleFlag)
        {

            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE car_info  ");
            sb.Append("set FleetID = @FleetID , CarType = @CarType , Brand = @Brand , ManufactureDate = @ManufactureDate , Model = @Model , Color = @Color , Displacement = @Displacement , DriverID = @DriverID , ScheduleFlag = @ScheduleFlag ");
            sb.Append("where  CarNo = @CarNo ");
            parms.Add(new MySqlParameter("@CarNo", id));
            parms.Add(new MySqlParameter("@FleetID", FleetID));
            parms.Add(new MySqlParameter("@CarType", CarType));
            parms.Add(new MySqlParameter("@Brand", Brand));
            parms.Add(new MySqlParameter("@ManufactureDate", ManufactureDate));
            parms.Add(new MySqlParameter("@Model", Model));
            parms.Add(new MySqlParameter("@Color", Color));
            parms.Add(new MySqlParameter("@Displacement", Displacement));
            parms.Add(new MySqlParameter("@DriverID", DriverID));
            if (ScheduleFlag == null)
                parms.Add(new MySqlParameter("@ScheduleFlag", ""));
            else
                parms.Add(new MySqlParameter("@ScheduleFlag", ScheduleFlag));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool CreateNewCar(string id, string FleetID, string CarType, string Brand, string ManufactureDate, string Model, string Color, string Displacement, string DriverID, string ScheduleFlag)
        {

            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO car_info  ");
            sb.Append("(CarNo, FleetID , CarType , Brand , ManufactureDate , Model , Color , Displacement , DriverID , ScheduleFlag ) ");
            sb.Append("VALUES (@CarNo ,@FleetID ,@CarType ,@Brand ,@ManufactureDate ,@Model ,@Color ,@Displacement ,@DriverID ,@ScheduleFlag ) ");
            parms.Add(new MySqlParameter("@CarNo", id));
            parms.Add(new MySqlParameter("@FleetID", Convert.ToInt32(FleetID)));
            parms.Add(new MySqlParameter("@CarType", CarType));
            parms.Add(new MySqlParameter("@Brand", Brand));
            parms.Add(new MySqlParameter("@ManufactureDate", ManufactureDate));
            parms.Add(new MySqlParameter("@Model", Model));
            parms.Add(new MySqlParameter("@Color", Color));
            parms.Add(new MySqlParameter("@Displacement", Displacement));
            parms.Add(new MySqlParameter("@DriverID", Convert.ToInt32(DriverID)));
            parms.Add(new MySqlParameter("@ScheduleFlag", ScheduleFlag));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OEmployee> GetDriversByAll(string FleetID, string UserID, string MobilePhone, string ShiftType)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            List<OEmployee> empList = new List<OEmployee>();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("select UserID, UserName, FleetID,MobilePhone , ShiftType  from user_info where UserRole ='D' ");
            if (FleetID != "")
            {
                sb.Append("And  FleetID = @FleetID ");
                parms.Add(new MySqlParameter("@FleetID", FleetID));
            }
            if (UserID != "0")
            {
                sb.Append("And  UserID = @UserID ");
                parms.Add(new MySqlParameter("@UserID", UserID));
            }
            if (MobilePhone != "")
            {
                sb.Append("And  MobilePhone = @MobilePhone ");
                parms.Add(new MySqlParameter("@MobilePhone", MobilePhone));
            }
            if (ShiftType != "")
            {
                sb.Append("And  ShiftType = @ShiftType ");
                parms.Add(new MySqlParameter("@ShiftType", ShiftType));
            }

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OEmployee emp = new OEmployee();
                    emp.ID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    emp.Name = Convert.ToString(dt.Rows[i]["UserName"]);
                    emp.MobilePhone = Convert.ToString(dt.Rows[i]["MobilePhone"]);
                    emp.FleetID = Convert.ToInt32(dt.Rows[i]["FleetID"]);
                    emp.ShiftType = Convert.ToString(dt.Rows[i]["ShiftType"]);
                    empList.Add(emp);
                }
            }
            else
                return null;

            return empList;


        }
        public List<OEmployee> GetDriverDataByID(string DriverID)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            List<OEmployee> empList = new List<OEmployee>();

            sb.Append("select UserID, Password, UserName, CitizenID, MobilePhone, HomePhone, UserRole, CompanyNo, FleetID, ShiftType, City, DistinctName, Address, ActiveFlag, CreateTime, UpdTime from user_info  Where UserID = @id ");
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@id", DriverID));
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    OEmployee emp = new OEmployee();
                    emp.ID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    emp.Name = Convert.ToString(dt.Rows[0]["UserName"]);
                    emp.Password = Convert.ToString(dt.Rows[0]["Password"]);
                    emp.CitizenID = Convert.ToString(dt.Rows[0]["CitizenID"]);
                    emp.MobilePhone = Convert.ToString(dt.Rows[0]["MobilePhone"]);
                    emp.HomePhone = Convert.ToString(dt.Rows[0]["HomePhone"]);
                    emp.Role = Convert.ToString(dt.Rows[0]["UserRole"]);
                    emp.CompanyNo = Convert.ToString(dt.Rows[0]["CompanyNo"]);
                    emp.FleetID = Convert.ToInt32(dt.Rows[0]["FleetID"]);
                    emp.ShiftType = Convert.ToString(dt.Rows[0]["ShiftType"]);
                    emp.City = Convert.ToString(dt.Rows[0]["City"]);
                    emp.Distinct = Convert.ToString(dt.Rows[0]["DistinctName"]);
                    emp.Address = Convert.ToString(dt.Rows[0]["Address"]);
                    emp.Active = Convert.ToString(dt.Rows[0]["ActiveFlag"]);
                    emp.CreateTime = Convert.ToDateTime(dt.Rows[i]["CreateTime"]);
                    emp.UpdTime = Convert.ToDateTime(dt.Rows[i]["UpdTime"]);

                    empList.Add(emp);
                }
            }
            else
                return null;

            return empList;


        }

        public bool UpdateFleets(string FleetID, string name, string CompanyNo, string acf)
        {

            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE fleet_info  ");
            sb.Append("set FleetID = @FleetID , CompanyNo = @CompanyNo , FleetName = @FleetName , ActiveFlag = @ActiveFlag  ");
            sb.Append("where  FleetID = @FleetID ");
            parms.Add(new MySqlParameter("@FleetID", FleetID));
            parms.Add(new MySqlParameter("@CompanyNo", CompanyNo));
            parms.Add(new MySqlParameter("@FleetName", name));
            if (acf == "True")
                parms.Add(new MySqlParameter("@ActiveFlag", true));
            else
                parms.Add(new MySqlParameter("@ActiveFlag", false));

            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateUser(string ID, string Password, string UserName, string CitizenID, string MobilePhone, string HomePhone, string CompanyNo, int FleetID, string ShiftType, string City, string DistinctName, string Address, string ActiveFlag)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE user_info  ");
            sb.Append("set Password = @Password, UserName = @UserName, CitizenID = @CitizenID, MobilePhone = @MobilePhone, HomePhone = @HomePhone, CompanyNo = @CompanyNo, FleetID = @FleetID, ShiftType = @ShiftType, City = @City, DistinctName = @DistinctName, Address = @Address, ActiveFlag = @ActiveFlag    ");
            sb.Append("where  UserID = @UserID ");
            parms.Add(new MySqlParameter("@UserID", ID));
            parms.Add(new MySqlParameter("@Password", Password));
            parms.Add(new MySqlParameter("@UserName", UserName));
            parms.Add(new MySqlParameter("@CitizenID", CitizenID));
            parms.Add(new MySqlParameter("@MobilePhone", MobilePhone));
            parms.Add(new MySqlParameter("@HomePhone", HomePhone));
            parms.Add(new MySqlParameter("@CompanyNo", CompanyNo));
            parms.Add(new MySqlParameter("@FleetID", FleetID));
            parms.Add(new MySqlParameter("@ShiftType", ShiftType));
            parms.Add(new MySqlParameter("@City", City));
            parms.Add(new MySqlParameter("@DistinctName", DistinctName));
            parms.Add(new MySqlParameter("@Address", Address));
            parms.Add(new MySqlParameter("@ActiveFlag", ActiveFlag));



            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string Ordercount()
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("	SELECT count(*) as num FROM reservation_sheet");
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    if (dt.Rows.Count > 0)
                    {
                        return Convert.ToString(dt.Rows[0]["num"]);
                    }
                    else
                        return "0";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public string Discount()
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("	SELECT count(*) as num FROM dispatch_sheet");
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    if (dt.Rows.Count > 0)
                    {
                        return Convert.ToString(dt.Rows[0]["num"]);
                    }
                    else
                        return "0";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataTable Coupon(string Coupon)
        {
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("SELECT SeqNo as '序號',CouponCode as '折扣碼',StartDate as '使用有效起日',EndDate as '使用有效迄日',CarFeeSub as '車資折抵優惠金額',FixCarFee as '固定車資優惠金額',NightFeeSub as '夜加優惠金額',Memo as '說明',UseCnt as '已使用數量',LimitCnt as '限制數量',TakeDateS as '可預約乘車日起日',TakeDateE as '可預約乘車日迄日',CreateTime as '新增時間',UpdTime as '異動時間'  ");
            sb.Append("FROM coupon_info ");
            if (Coupon != "")
                sb.Append("where CouponCode= @Coupon  order by CreateTime desc ");
            else
                sb.Append(" order by 序號 desc ");

            parms.Add(new MySqlParameter("@Coupon", Coupon));

            DataTable dt = new DataTable();
            try
            {
                using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
                {
                    dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                return null;
            }
        }

        public bool InsertCoupon(string Coupon, string QSDate1, string QSDate2, string QEDate1, string QEDate2, string comment, int CarFeeSubInput, int FixCarFeeInput, int NightFeeSubInput)
        {

            List<MySqlParameter> parms = new List<MySqlParameter>();
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO coupon_info (CouponCode, StartDate, EndDate, CarFeeSub, FixCarFee, NightFeeSub, Memo, UseCnt,LimitCnt, TakeDateS, TakeDateE )");
            sb.Append("VALUES (@CouponCode, @StartDate,@EndDate , @CarFeeSub, @FixCarFee, @NightFeeSub, @Memo,@UseCnt, @LimitCnt , @TakeDateS, @TakeDateE)");

            parms.Add(new MySqlParameter("@CouponCode", Coupon));
            parms.Add(new MySqlParameter("@StartDate", QSDate1));
            parms.Add(new MySqlParameter("@EndDate", QEDate1));
            parms.Add(new MySqlParameter("@CarFeeSub", CarFeeSubInput));
            parms.Add(new MySqlParameter("@FixCarFee", FixCarFeeInput));
            parms.Add(new MySqlParameter("@NightFeeSub", NightFeeSubInput));
            parms.Add(new MySqlParameter("@Memo", comment));
            parms.Add(new MySqlParameter("@UseCnt", "0"));
            parms.Add(new MySqlParameter("@LimitCnt", "1"));
            parms.Add(new MySqlParameter("@TakeDateS", QSDate2));
            parms.Add(new MySqlParameter("@TakeDateE", QEDate2));


            using (dbConnectionMySQL conn = new dbConnectionMySQL(this._ConnectionString))
            {
                try
                {
                    return conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }
        }
    }
}