﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

using System.Collections.Specialized;
using System.Web.SessionState;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace CallCar.Controllers {




    public static class Constant {

        public static string userID = "userID";
        public static string apiBaseUrl = "https://api2.ishareco.com/bapi/rest/";


    }

    public class _BaseController : Controller {

        protected string userID = "";
        protected string userName = "";

        //protected DataServiceClient webService;

        public string JsonEncode(dynamic x) {
            return JsonConvert.SerializeObject(x);

        }

        public string sendPOST(string url, string jsonText) {

            string responseString = "";


            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream())) {
                string json = "{\"user\":\"test\"," +
                              "\"password\":\"bla\"}";

                streamWriter.Write(jsonText);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream())) {
                //var result = streamReader.ReadToEnd();
                responseString = streamReader.ReadToEnd();

            }

            responseString = responseString.TrimEnd('"');
            responseString = responseString.TrimStart('"');

            responseString = responseString.Replace("\\\"", "\"");

            return responseString;


        }
        public static String ConstructQueryString(NameValueCollection parameters) {
            List<String> items = new List<String>();

            foreach (String name in parameters) {
                //items.Add(String.Concat(name, "=", System.Web.HttpUtility.UrlEncode(parameters[name])));
                items.Add(String.Concat(name, "=", parameters[name]));
            }
            return String.Join("&", items.ToArray());
        }

        public string sendHttpRequest(string url) {


            string responseText = String.Empty;
            /*
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (StreamReader sr = new StreamReader(response.GetResponseStream())) {
                responseText = sr.ReadToEnd();
            }*/


            var postData = "thing1=hello";
            //var data = Encoding.ASCII.GetBytes(postData);


            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            //httpWebRequest.ContentType = "application/json";
            //httpWebRequest.ContentType = "application/json; charset=utf-8";
            //httpWebRequest.ContentType = "text/html; charset=utf-8";
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            //httpWebRequest.ContentLength = data.Length;


            httpWebRequest.Accept = "*/*";
            //httpWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            //httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0";
            httpWebRequest.Method = "GET";
            //httpWebRequest.Method = "POST";
            //httpWebRequest.Method = method;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream())) {
                responseText = streamReader.ReadToEnd();
            }
            return responseText;

        }



        public _BaseController() {

            //webService = new WebServiceReference.DataServiceClient();


        }
        public void setSession(string key, dynamic value) {

            System.Web.HttpContext.Current.Session[key] = value;

        }

        public dynamic getSession(string key) {

            dynamic temp = null;

            try {
                temp = System.Web.HttpContext.Current.Session[key];
            } catch (Exception) {
            }

            return temp;
        }

    }


}