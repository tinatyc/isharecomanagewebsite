﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Security.Cryptography;

namespace CallCar.Controllers
{
    public class CarController : _AdminController
    {
        // GET: Car
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Add()
        {
            return View();

        }
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        [HttpPost]
        public JsonResult GetList( int page = 1,string searchJson = null, string orderField = "", string orderType = "", int itemPerPage = 10)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {
                string CarNo = "";
                string Driver = "";


                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        CarNo = search.CarNo;
                        Driver = search.Driver;

                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }

                }

                DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);
                BALGeneral balg = new BALGeneral(ConnectionString);
                int DriverID = 0;
                List<OCar> List;
                if (Driver != "")
                {
                    DriverID = dalr.GetDriverIDByName(Driver);
                    List = dalr.GetCarsByFleetAndDriver(CarNo, DriverID.ToString());
                }
                else
                    List = dalr.GetCarsByFleetAndDriver(CarNo, Driver.ToString());

                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;

                List<dynamic> data = new List<dynamic>();

                if (List != null)
                {

                    foreach (OCar or in List)
                    {
                        TotalItem++;

                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                        a.Add("CarNo", or.CarNo.ToString()); //: "CR01E9541", 訂單編號
                        a.Add("FleetID", or.FleetID.ToString()); //: 100,  會員編號
                        a.Add("CarType", or.Type.ToString()); //: "O",  出國O,回國 I
                        a.Add("Brand", or.Brand.ToString()); //: "20170207", 乘車日期
                        a.Add("Model", or.Model.ToString()); //: "O",  出國O,回國 I
                        a.Add("ManufactureDate", or.MDate.ToString()); //: "20170207", 乘車日期
                        a.Add("Color", or.Color.ToString()); //: "O",  出國O,回國 I
                        a.Add("Displacement", or.Displacement.ToString()); //: "20170207", 乘車日期
                        a.Add("DriverID", dalr.GetDriversByID(or.DriverID).ToString()); //: "O",  出國O,回國 I
                        a.Add("ScheduleFlag", or.ScheduleFlag.ToString()); //: "20170207", 乘車日期
                        a.Add("CreateTime", or.CreateTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "2000", 預約時段
                        a.Add("UpdTime", or.UpdTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "新北市",  上車城市



                        data.Add(a);

                    }

                }

                JsonResult.Add("data", data);

                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);

                //PageTotal = 1;

                JsonResult.Add("totalItem", TotalItem);
                JsonResult.Add("pageTotal", PageTotal);
                JsonResult.Add("nowpage", page);
            }
            catch (Exception e)
            {
                e.ToString();
            }


            return Json(JsonResult);

        }

        public ActionResult Item(string id)
        {
            DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);

            List<OCar> List = dalr.GetCarsByCarNo(id);


            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();

            foreach (OCar or in List)
            {
                data.Add("CarNo", or.CarNo.ToString()); //: "CR01E9541", 訂單編號
                data.Add("FleetID", or.FleetID.ToString()); //: 100,  會員編號
                data.Add("CarType", or.Type.ToString()); //: "O",  出國O,回國 I
                data.Add("Brand", or.Brand.ToString()); //: "20170207", 乘車日期
                data.Add("ManufactureDate", or.MDate.ToString()); //: "O",  出國O,回國 I
                data.Add("Model", or.Model.ToString());
                data.Add("Color", or.Color.ToString()); //: "O",  出國O,回國 I
                data.Add("Displacement", or.Displacement.ToString()); //: "20170207", 乘車日期
                data.Add("DriverID", dalr.GetDriversByID(or.DriverID).ToString()); //: "O",  出國O,回國 I
                data.Add("ScheduleFlag", or.ScheduleFlag.ToString()); //: "20170207", 乘車日期
                data.Add("CreateTime", or.CreateTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "2000", 預約時段
                data.Add("UpdTime", or.UpdTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "新北市",  上車城市

                string dataText = "";
                List<SelectListItem> mySelectItemList = new List<SelectListItem>();
                if (or.ScheduleFlag.ToString() == "1")
                {
                    mySelectItemList.Add(new SelectListItem()
                    {
                        Text = "True",
                        Value = "1",
                        Selected = true
                    });
                    mySelectItemList.Add(new SelectListItem()
                    {
                        Text = "False",
                        Value = "0",
                        Selected = false
                    });
                }
                else
                {
                    mySelectItemList.Add(new SelectListItem()
                    {
                        Text = "True",
                        Value = "1",
                        Selected = false
                    });
                    mySelectItemList.Add(new SelectListItem()
                    {
                        Text = "False",
                        Value = "0",
                        Selected = true
                    });
                }
                ViewBag.ScheduleFlag = mySelectItemList;
                dataText = Helper.jsonEncode(data);
                ViewBag.dataText = dataText;
            }
           


            return View();

        }

        public JsonResult ItemEdit(string id, string FleetID, string CarType, string Brand, string ManufactureDate, string Model, string Color, string Displacement, string DriverID, string ScheduleFlag)
        {

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);

            bool rtn = false;

            int Driver = dalr.GetDriverIDByName(DriverID);
            rtn = dalr.UpdateCar_info( id, FleetID, CarType, Brand, ManufactureDate, Model, Color, Displacement, Driver.ToString(), ScheduleFlag);

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult ItemCreate(string id, string FleetID, string CarType, string Brand, string ManufactureDate, string Model, string Color, string Displacement, string DriverID, string ScheduleFlag)
        {

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);

            bool rtn = false;

            int Driver = dalr.GetDriverIDByName(DriverID);
            rtn = dalr.CreateNewCar(id, FleetID, CarType, Brand, ManufactureDate, Model, Color, Displacement, Driver.ToString(), ScheduleFlag);

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult);
            }

        }
    }
}