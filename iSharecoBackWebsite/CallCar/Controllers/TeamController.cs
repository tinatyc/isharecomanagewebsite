﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Security.Cryptography;

namespace CallCar.Controllers
{
    public class TeamController : _AdminController
    {
        // GET: Team
        public ActionResult Index()
        {
            return View();
        }

        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public ActionResult Item(string id)
        {
            DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);
            OFleet R;
            R = dalr.GetFleet(Convert.ToInt32(id));


            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();


            data.Add("fid", R.FleetID.ToString()); //: "CR01E9541", 訂單編號
            data.Add("fnm", R.FleetName.ToString()); //: 100,  會員編號
            data.Add("cno", R.Company.ToString()); //: "O",  出國O,回國 I
            data.Add("acf", R.ActiveFlag.ToString()); //: "20170207", 乘車日期
            data.Add("crt", R.CreateTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "2000", 預約時段
            data.Add("udt", R.UpdTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "新北市",  上車城市

            string dataText = "";

            dataText = Helper.jsonEncode(data);

            ViewBag.dataText = dataText;

            Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();
            dict.Add(R.ActiveFlag.ToString(), R.ActiveFlag.ToString());

            List<SelectListItem> mySelectItemList = new List<SelectListItem>();
            if (R.ActiveFlag.ToString() == "True")
            {
                mySelectItemList.Add(new SelectListItem()
                {
                    Text = "True",
                    Value = "True",
                    Selected = true
                });
                mySelectItemList.Add(new SelectListItem()
                {
                    Text = "False",
                    Value = "False",
                    Selected = false
                });
            }
            else
            {
                mySelectItemList.Add(new SelectListItem()
                {
                    Text = "True",
                    Value = "True",
                    Selected = false
                });
                mySelectItemList.Add(new SelectListItem()
                {
                    Text = "False",
                    Value = "False",
                    Selected = true
                });
            }
            ViewBag.Fruit = mySelectItemList;

            return View();

        }

        public ActionResult Add()
        {
            return View();

        }

        public JsonResult ItemEdit(string id, string name, string cno, string acf)
        {

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);

            bool rtn = false;

            rtn = dalr.UpdateFleets(id, name, cno, acf);

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult ItemCreate(string id, string nm, string cno, string acf)
        {

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);
            bool bo = false;
            bool rtn = false;
            if (acf == "True")
                bo = true;

            rtn = dalr.CreateNewCarTeam(id, nm, cno, bo);

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult);
            }

        }
        [HttpPost]
        //public JsonResult GetList(int page = 1, int itemPerPage = 20)
        public JsonResult GetList( int page ,string searchJson = null, string orderField = "", string orderType = "", int itemPerPage = 10)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                string QSDate = "";
                string QEDate = "";
                string OrderNo = "";
                string MobilePhone = "";
                string UserID = "0";


                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        QSDate = search.QSDate;
                        QEDate = search.QEDate;
                        OrderNo = search.OrderNo;
                        UserID = search.UserID;
                        MobilePhone = search.MobilePhone;

                        if (search.UserID == "")
                        {
                            UserID = "0";
                        }
                        else
                        {
                            UserID = search.UserID;
                        }

                        //CarNo = search.CarNo;

                    }
                    catch (Exception e)
                    {

                    }

                }

                Dictionary<string, dynamic> parameters = new Dictionary<string, dynamic>();

                parameters.Add("QSDate", QSDate); //:"20170101",//查詢派車的開始日期
                parameters.Add("QEDate", QEDate); //:"20170102",//查詢派車的結束日期
                parameters.Add("OrderNo", OrderNo); //:"20170156878",//指定訂單編號查詢
                parameters.Add("UserID", UserID); //:123456,//指定會員編號查詢
                parameters.Add("MobilePhone", MobilePhone); //:"0912XXXXXX"//指定手機號碼查詢


                //string dataString = Helper.jsonEncode(parameters);

                DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);
                //QOrderCondStruct qs = JsonConvert.DeserializeObject<QOrderCondStruct>(dataString);

                List < OFleet > List = dalr.GetFleetList();

                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;

                //RepStarDateTime = "2016-12-20";
                //RepEndDateTime = "2017-01-04";

                List<dynamic> data = new List<dynamic>();
                //var items = List;


                if (List != null)
                {

                    foreach (OFleet or in List)
                    {
                        TotalItem++;

                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                        a.Add("fid", or.FleetID.ToString()); //: "CR01E9541", 訂單編號
                        a.Add("fnm", or.FleetName.ToString()); //: 100,  會員編號
                        a.Add("cno", or.Company.ToString()); //: "O",  出國O,回國 I
                        a.Add("acf", or.ActiveFlag.ToString()); //: "20170207", 乘車日期
                        a.Add("crt", or.CreateTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "2000", 預約時段
                        a.Add("udt", or.UpdTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "新北市",  上車城市



                        data.Add(a);

                    }

                }

                JsonResult.Add("data", data);

                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);


                JsonResult.Add("totalItem", TotalItem);
                JsonResult.Add("pageTotal", PageTotal);
                JsonResult.Add("nowpage", page);
            }
            catch (Exception)
            {

            }


            return Json(JsonResult);

        }


    }
}