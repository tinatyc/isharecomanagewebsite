﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;
using DbConn;
using MySql.Data.MySqlClient;
using System.Transactions;
using TGOS;


namespace CallCar.Controllers
{
    public class OrderController : _AdminController
    {
        public ActionResult Index()
        {

            return View();
        }

        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public ActionResult Item(string id)
        {


            //string dataString = Helper.jsonEncode(parameters);

            // http://apid.ishareco.com/bapi/gOrder?no=AF00018517


            /*string url = Constant.apiBaseUrl + "gOrder?no=" + id;

            string responseText = sendHttpRequest(url);

            dynamic jsonData = Helper.jsonDecode(responseText);
            string GetOrderByNoResult = jsonData.GetOrderByNoResult;
            dynamic jsonData2 = Helper.jsonDecode(GetOrderByNoResult);

            string Status = jsonData2.Status;
            JObject RtnObject = jsonData2.RtnObject;*/

            DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);
            OReservation R;
            R = dalr.GetReservation(id);
            dynamic search = JObject.Parse(R.Credit);
            string IDMD5;
            IDMD5 = Encrypt(search.UID.Value.ToString());
            string cardno = Decrypt(search.CNo.Value.ToString(), IDMD5.Substring(0, 16), IDMD5.Substring(16, 16));
            string card4 = cardno.Substring(0, 4);
            string card12 = cardno.Substring(12, 4);
            string mmcard = card4 + "-********-" + card12;
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();
            DAL.DALUserAccount dalu = new DAL.DALUserAccount(ConnectionString);
            OAccount A;
            A = dalu.GetAccountInfo(R.UserID);
            OReservation_process_log Rpl;
            data.Add("rno", R.ReservationNo.ToString()); // 01E9541", 訂單編號
            data.Add("uid", R.UserID.ToString()); // ,  會員編號
            data.Add("uname", A.FullName.ToString()); // ,  會員編號
            data.Add("uphone", A.MobilePhone.ToString()); // ,  會員編號
            data.Add("uemail", A.Email.ToString()); // ,  會員編號
            data.Add("sertype", R.ServiceType.ToString()); // ,  出國O,回國 I
            data.Add("tdate", R.ServeDate); // 170207", 乘車日期
            data.Add("timeseg", R.PreferTime); // 00", 預約時段
            data.Add("seltype", R.ChooseType); // , 預約方式 指定航班 F, 指定航廈 T
            data.Add("pcartype", R.PickCartype); // , 訂單種類 預約0 , 臨時預約 1
            data.Add("fno", R.FlightNo); // 26", 航班編號
            data.Add("fdatetime", R.FlightTime.ToString("yyyy/MM/dd HH:mm").ToString()); // 17/02/07 23:40", 航班時間
            data.Add("pcity", R.PickupCity); // 市",  上車城市
            data.Add("pdistinct", R.PickupDistinct.ToString()); // 區", 上車區里
            data.Add("pvillage", R.PickupVillage.ToString()); //  上車區里
            data.Add("paddress", R.PickupAddress.ToString()); // 北路一段305號", 上車地址
            data.Add("plat", R.PickupGPS.GetLat().ToString()); // 182815, 上車緯度
            data.Add("plng", R.PickupGPS.GetLng().ToString()); // .444322, 上車經度
            data.Add("tcity", R.TakeoffCity.ToString()); // 市", 下車城市
            data.Add("tdistinct", R.TakeoffDistinct.ToString()); // 區", 下車區里
            data.Add("tvillage", R.TakeoffVillage.ToString()); //  下車區里
            data.Add("taddress", R.TakeoffAddress.ToString()); // 國際機場 第二航廈", 下車地址
            data.Add("tlat", R.TakeoffGPS.GetLat().ToString()); // 076821, 下車緯度
            data.Add("tlng", R.TakeoffGPS.GetLng().ToString()); // .231683, 下車經度
            data.Add("pcnt", R.PassengerCnt.ToString()); // 乘客人數
            data.Add("bcnt", R.BaggageCnt.ToString()); // 行李數量
            data.Add("max", R.MaxFlag.ToString()); // , 是否滿車
            data.Add("trialprice", R.TrailPrice.ToString()); // 試算金額
            data.Add("coupun", R.Coupon.ToString()); //  優惠內容
            data.Add("invoice", R.Invoice.ToString()); //  發票資料
            data.Add("credit", mmcard.ToString()); //  信用卡資料
                                                   // 0：新增 X：取消 A：已媒合 B：媒合中 C：帳務處理中 Z：已結帳
            if (R.ProcessStage.ToString() == "0")
                data.Add("stage", "新增"); // , 狀態
            else if (R.ProcessStage.ToString() == "X")
                data.Add("stage", "取消"); // , 狀態
            else if (R.ProcessStage.ToString() == "A")
                data.Add("stage", "已媒合"); // , 狀態
            else if (R.ProcessStage.ToString() == "B")
                data.Add("stage", "媒合中"); // , 狀態
            else if (R.ProcessStage.ToString() == "C")
                data.Add("stage", "帳務處理中"); // , 狀態
            else if (R.ProcessStage.ToString() == "Z")
                data.Add("stage", "已結帳"); // , 狀態
            else if (R.ProcessStage.ToString() == "Y")
            {
                Rpl = dalr.GetReservation_process_log(id);
                data.Add("Memo", Rpl.Memo.ToString());
                data.Add("stage", "計價&取消"); // , 狀態
            }
            else if (R.ProcessStage.ToString() == "W")
            {
                Rpl = dalr.GetReservation_process_log(id);
                data.Add("Memo", Rpl.Memo.ToString());
                data.Add("stage", "不計價取消"); // , 狀態
            }
            else
                data.Add("stage", R.ProcessStage.ToString()); // , 狀態
            data.Add("ein", R.EIN.ToString()); //  發票號碼
            data.Add("cdatetime", R.OrderTime.ToString("yyyy/MM/dd HH:mm").ToString()); // 17/02/01 00:05" 建立時間
            data.Add("udatetime", R.udpTime.ToString("yyyy/MM/dd HH:mm").ToString()); // 17/02/01 00:05" 建立時間
            data.Add("pn", R.PassengerName.ToString()); // 17/02/01 00:05" 建立時間
            data.Add("pp", R.PassengerPhone.ToString()); // 17/02/01 00:05" 建立時間
            data.Add("baby", R.BabySeatCnt.ToString());


            string dataText = "";

            dataText = Helper.jsonEncode(data);

            ViewBag.dataText = dataText;


            return View();

        }

        public JsonResult UpdAddress(string rno, string address, string type, string stage)
        {
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            string Outputstring;
            double lat = 0, lng = 0;
            Outputstring = Query(address);
            //TGOSJson t = JsonConvert.DeserializeObject<TGOSJson>(Outputstring);
            JObject jo = JObject.Parse(Outputstring);

            if (Convert.ToString(jo["status"]) == "OK")
            {
                try
                {
                    lat = double.Parse(Convert.ToString(jo["results"][0]["geometry"]["location"]["lat"]));
                    lng = double.Parse(Convert.ToString(jo["results"][0]["geometry"]["location"]["lng"]));
                }
                catch (Exception ex)
                {
                    JsonResult.Add(ex.Message, "Error");
                }
            }
            //if (t.AddressLists == null || t.AddressLists.Count == 0)
            //{
            //    JsonResult.Add("Message", "地址比對系統無法取得座標");
            //    return Json(JsonResult);
            //}

            //lat = t.AddressLists[0].Y;
            //lng = t.AddressLists[0].X;


            sb.Append("Update reservation_sheet  Set ");
            if (type == "T")
            {
                sb.Append(" TakeoffAddress = @address, TakeoffGPS = GeomFromText(@TakeoffGPS) ");
                parms.Add(new MySqlParameter("@address", address));
                parms.Add(new MySqlParameter("@TakeoffGPS", "Point(" + lng.ToString() + " " + lat.ToString() + ")"));
            }
            else
            {
                sb.Append(" PickupAddress = @address , PickupGPS = GeomFromText(@PickupGPS) ");
                parms.Add(new MySqlParameter("@address", address));
                parms.Add(new MySqlParameter("@PickupGPS", "Point(" + lng.ToString() + " " + lat.ToString() + ")"));
            }
            sb.Append(" WHERE ReservationNo = @no ");

            parms.Add(new MySqlParameter("@no", rno));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());


                        sb.Length = 0;
                        parms.Clear();

                        if (stage == "已媒合")
                        {
                            sb.Append("Update dispatch_reservation Set LocationGPS = GeomFromText(@LocationGPS) Where ReservationNo = @no");
                            parms.Add(new MySqlParameter("@LocationGPS", "Point(" + lng.ToString() + " " + lat.ToString() + ")"));
                            parms.Add(new MySqlParameter("@no", rno));

                            conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                JsonResult.Add("Message", ex.Message);
                return Json(JsonResult);
            }
            JsonResult.Add("Message", "Success");
            return Json(JsonResult);
        }
        public JsonResult UpdPassenger(string rno, string name, string phone)
        {
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            int pPhone;
            if (name.Trim() == "" || phone.Trim() == "" || phone.Length != 10 || phone.Substring(0, 2) != "09" || !Int32.TryParse(phone, out pPhone))
            {
                JsonResult.Add("Message", "輸入資料錯誤");
                return Json(JsonResult);
            }
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Update reservation_sheet Set PassengerName = @name , PassengerPhone = @phone Where ReservationNo = @no ");
            parms.Add(new MySqlParameter("@name", name));
            parms.Add(new MySqlParameter("@phone", phone));
            parms.Add(new MySqlParameter("@no", rno));
            try
            {

                using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                {
                    conn.executeUpdateQuery(sb.ToString(), parms.ToArray());

                    sb.Length = 0;
                    parms.Clear();

                    sb.Append("select SeqNo From reservation_sms Where ReservationNo = @no");
                    parms.Add(new MySqlParameter("@no", rno));

                    DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                    if (dt.Rows.Count > 0)
                    {

                        sb.Length = 0;
                        parms.Clear();
                        sb.Append("Update reservation_sms Set PassengerName = @name , MobilePhone = @phone Where ReservationNo = @no ");
                        parms.Add(new MySqlParameter("@name", name));
                        parms.Add(new MySqlParameter("@phone", phone));
                        parms.Add(new MySqlParameter("@no", rno));


                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                JsonResult.Add("Message", ex.Message);
                return Json(JsonResult);
            }

            JsonResult.Add("Message", "Success");
            return Json(JsonResult);

        }

        public JsonResult UpdCnt(string rno, string pcnt, string bcnt)
        {
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            int ipcnt, ibcnt;
            try
            {
                ipcnt = Convert.ToInt32(pcnt);
                ibcnt = Convert.ToInt32(bcnt);
            }
            catch (Exception)
            {
                JsonResult.Add("Message", "輸入資料錯誤");
                return Json(JsonResult);
            }
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Update reservation_sheet Set PassengerCnt = @pcnt , BaggageCnt = @bcnt Where ReservationNo = @no ");
            parms.Add(new MySqlParameter("@pcnt", ipcnt));
            parms.Add(new MySqlParameter("@bcnt", ibcnt));
            parms.Add(new MySqlParameter("@no", rno));
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());

                        sb.Length = 0;
                        parms.Clear();

                        sb.Append("Select s.DispatchNo,s.CarpoolFlag From dispatch_sheet s,dispatch_reservation r Where s.DispatchNo = r.DispatchNo and r.ReservationNo = @rno");
                        parms.Add(new MySqlParameter("@rno", rno));
                        DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                        if (dt.Rows.Count == 1)
                        {
                            string dno = Convert.ToString(dt.Rows[0]["DispatchNo"]);
                            string poolflag = Convert.ToString(dt.Rows[0]["CarpoolFlag"]);

                            sb.Length = 0;
                            parms.Clear();

                            sb.Append("INSERT INTO dispatch_refresh_record (RefreshType, DispatchNo,CarpoolFlag, RefreshFlag) VALUES (@type, @dno,@flag, '0')");
                            parms.Add(new MySqlParameter("@type", "U"));
                            parms.Add(new MySqlParameter("@dno", dno));
                            parms.Add(new MySqlParameter("@flag", poolflag));

                            conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                JsonResult.Add("Message", ex.Message);
                return Json(JsonResult);
            }

            JsonResult.Add("Message", "Success");
            return Json(JsonResult);

        }

        public JsonResult UpdBabySeatCnt(string rno, string bcnt)
        {
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            int ibcnt;
            try
            {
                ibcnt = Convert.ToInt32(bcnt);
            }
            catch (Exception)
            {
                JsonResult.Add("Message", "輸入資料錯誤");
                return Json(JsonResult);
            }
            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();
            sb.Append("Update reservation_sheet Set BabySeatCnt = @BabySeatCnt Where ReservationNo = @no ");
            parms.Add(new MySqlParameter("@BabySeatCnt", ibcnt));
            parms.Add(new MySqlParameter("@no", rno));
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());

                        sb.Length = 0;
                        parms.Clear();

                        sb.Append("Select s.DispatchNo,s.CarpoolFlag From dispatch_sheet s,dispatch_reservation r Where s.DispatchNo = r.DispatchNo and r.ReservationNo = @rno");
                        parms.Add(new MySqlParameter("@rno", rno));
                        DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());

                        if (dt.Rows.Count == 1)
                        {
                            string dno = Convert.ToString(dt.Rows[0]["DispatchNo"]);
                            string poolflag = Convert.ToString(dt.Rows[0]["CarpoolFlag"]);

                            sb.Length = 0;
                            parms.Clear();

                            sb.Append("INSERT INTO dispatch_refresh_record (RefreshType, DispatchNo,CarpoolFlag, RefreshFlag) VALUES (@type, @dno,@flag, '0')");
                            parms.Add(new MySqlParameter("@type", "U"));
                            parms.Add(new MySqlParameter("@dno", dno));
                            parms.Add(new MySqlParameter("@flag", poolflag));

                            conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                JsonResult.Add("Message", ex.Message);
                return Json(JsonResult);
            }

            JsonResult.Add("Message", "Success");
            return Json(JsonResult);

        }
        public JsonResult ItemCancel(string id, string memo)
        {
            string Source = "B";
            string data = id;
            string token = "";
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            BALReservation balr = new BALReservation(ConnectionString);
            OReservation or;
            string Rno = string.Empty;

            if (Source == "U")
            {
                string iv = "8417728284177282";
                Common.AESCryptography aes = new Common.AESCryptography(token, iv);
                Rno = aes.Decrypt(data);
            }
            else
                Rno = data;

            try
            {
                or = balr.GetReservationByNo(Rno);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);

            DateTime NowTime = DateTime.Now;
            bool rtn = false;

            if (or == null)
            {
                JsonResult.Add("Message", "XXX");
                return Json(JsonResult);
            }
            if (or.ProcessStage == "X" || or.ProcessStage == "W" || or.ProcessStage == "Y" || or.ProcessStage == "Z")
            {
                JsonResult.Add("Message", "不可取消");
                return Json(JsonResult);
            }
            else if (or.ProcessStage == "A")
            {
                if (Source == "U")
                {
                    DateTime TakeDatetime = Common.General.ConvertToDateTime(or.ServeDate + or.PreferTime, 12);

                    double diffTime = new TimeSpan(TakeDatetime.Ticks - NowTime.Ticks).TotalHours;
                    if (diffTime < 24)
                    {
                        JsonResult.Add("Message", "998");
                        return Json(JsonResult);
                    }
                }

                try
                {
                    rtn = dalr.CancelReservation(Rno, or.ProcessStage);
                    string neID = getSession("userID").ToString();
                    dalr.AddReservation_process_log(Rno, "W", memo, neID);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }


            }
            else if (or.ProcessStage == "0")
            {
                try
                {
                    rtn = dalr.CancelReservation(Rno, or.ProcessStage);
                    string neID = getSession("userID").ToString();
                    dalr.AddReservation_process_log(Rno, "W", memo, neID);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }
            else
            {
                JsonResult.Add("Message", "XXX");
                return Json(JsonResult);
            }

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult);
            }

        }

        public JsonResult ItemCancel2(string id, string memo)
        {
            string Source = "B";
            string data = id;
            string token = "";
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            BALReservation balr = new BALReservation(ConnectionString);
            OReservation or;
            string Rno = string.Empty;

            if (Source == "U")
            {
                string iv = "8417728284177282";
                Common.AESCryptography aes = new Common.AESCryptography(token, iv);
                Rno = aes.Decrypt(data);
            }
            else
                Rno = data;

            try
            {
                or = balr.GetReservationByNo(Rno);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);

            DateTime NowTime = DateTime.Now;
            bool rtn = false;

            if (or == null)
            {
                JsonResult.Add("Message", "XXX");
                return Json(JsonResult);
            }
            if (or.ProcessStage == "X" || or.ProcessStage == "W" || or.ProcessStage == "Y" || or.ProcessStage == "Z")
            {
                JsonResult.Add("Message", "不可取消");
                return Json(JsonResult);
            }
            else if (or.ProcessStage == "A")
            {
                if (Source == "U")
                {
                    DateTime TakeDatetime = Common.General.ConvertToDateTime(or.ServeDate + or.PreferTime, 12);

                    double diffTime = new TimeSpan(TakeDatetime.Ticks - NowTime.Ticks).TotalHours;
                    if (diffTime < 24)
                    {
                        JsonResult.Add("Message", "998");
                        return Json(JsonResult);
                    }
                }

                try
                {
                    string neID = getSession("userID").ToString();
                    dalr.AddReservation_process_log(Rno, "Y", memo, neID);
                    rtn = dalr.CancelReservation2(Rno, or.ProcessStage);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }


            }
            else if (or.ProcessStage == "0")
            {
                try
                {
                    string neID = getSession("userID").ToString();
                    dalr.AddReservation_process_log(Rno, "Y", memo, neID);
                    rtn = dalr.CancelReservation(Rno, or.ProcessStage);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }
            else
            {
                JsonResult.Add("Message", "XXX");
                return Json(JsonResult);
            }

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult);
            }

        }

        [HttpPost]
        //public JsonResult GetList(int page = 1, int itemPerPage = 20)
        public JsonResult GetList(int page, string ProcessStage, string CarpoolFlag, string searchJson = null, string orderField = "", string orderType = "", int itemPerPage = 10)
        {
            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                string QSDate = "";
                string QEDate = "";
                string OrderNo = "";
                string MobilePhone = "";
                string PMobilePhone = "";
                string UserID = "0";

                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        QSDate = search.QSDate;
                        QEDate = search.QEDate;
                        OrderNo = search.OrderNo;
                        UserID = search.UserID;
                        MobilePhone = search.MobilePhone;
                        PMobilePhone = search.PMobilePhone;
                        if (search.UserID == "")
                        {
                            UserID = "0";
                        }
                        else
                        {
                            UserID = search.UserID;
                        }

                        //CarNo = search.CarNo;

                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }

                }

                Dictionary<string, dynamic> parameters = new Dictionary<string, dynamic>();

                /*
                parameters.Add("QSDate", "20170101");//查詢派車的開始日期
                parameters.Add("QEDate", "20170102");//查詢派車的結束日期
                parameters.Add("DispatchNo", "20170126578");//指定派車單號查詢
                parameters.Add("OrderNo", "20170156878");//指定訂單編號查詢
                parameters.Add("DriverID", 123456);//指定司機查詢
                parameters.Add("CarNo", "RAM-0158");//指定車號查詢
                */

                parameters.Add("QSDate", QSDate); //:"20170101",//查詢派車的開始日期
                parameters.Add("QEDate", QEDate); //:"20170102",//查詢派車的結束日期
                parameters.Add("OrderNo", OrderNo); //:"20170156878",//指定訂單編號查詢
                parameters.Add("UserID", UserID); //:123456,//指定會員編號查詢
                parameters.Add("MobilePhone", MobilePhone); //:"0912XXXXXX"//指定手機號碼查詢
                parameters.Add("PMobilePhone", PMobilePhone);

                string dataString = Helper.jsonEncode(parameters);

                /*string url = Constant.apiBaseUrl + "gOrders";


                string responseText = sendPOST(url, dataString);

                dynamic jsonData = Helper.jsonDecode(responseText);

                string Status = jsonData.Status;
                var RtnObject = jsonData.RtnObject;*/

                DAL.DALReservation dalr = new DAL.DALReservation(ConnectionString);
                QOrderCondStruct qs = JsonConvert.DeserializeObject<QOrderCondStruct>(dataString);
                List<OReservation> List = dalr.GetReservationsMultipleCondition(qs, qs.take, qs.skip, ProcessStage, CarpoolFlag);

                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;

                //RepStarDateTime = "2016-12-20";
                //RepEndDateTime = "2017-01-04";

                List<dynamic> data = new List<dynamic>();
                //var items = List;


                if (List != null)
                {

                    foreach (OReservation or in List)
                    {
                        TotalItem++;
                        DAL.DALUserAccount gg = new DAL.DALUserAccount(ConnectionString);
                        OAccount ggll = gg.GetID(or.UserID.ToString());
                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                        a.Add("rno", or.ReservationNo.ToString()); //: "CR01E9541", 訂單編號
                                                                   //a.Add("uid", or.UserID.ToString()); //: 100,  會員編號
                        a.Add("poolflag", or.CarpoolFlag.ToString()); // 專接共乘
                        a.Add("passname", or.PassengerName.ToString());
                        a.Add("uidname", ggll.LName.ToString() + ggll.FName.ToString()); //: 100,  會員編號
                        a.Add("sertype", or.ServiceType.ToString()); //: "O",  出國O,回國 I
                        if (or.ServeDate.ToString() != "")
                            a.Add("tdate", or.ServeDate.Substring(0, 4) + "-" + or.ServeDate.Substring(4, 2) + "-" + or.ServeDate.Substring(6, 2));
                        else
                            a.Add("tdate", or.ServeDate.ToString()); //: "20170207", 乘車日期

                        if (or.PreferTime.ToString() != "")
                            a.Add("timeseg", or.PreferTime.Substring(0, 2) + ":" + or.PreferTime.Substring(2, 2));
                        else
                            a.Add("timeseg", or.PreferTime.ToString()); //: "2000", 預約時段
                        a.Add("Stimeseg", or.ScheduleServeTime.ToString()); //
                        a.Add("pcity", or.PickupCity.ToString()); //: "新北市",  上車城市
                        a.Add("pdistinct", or.PickupDistinct.ToString()); //: "淡水區", 上車區里
                        a.Add("paddress", or.PickupAddress.ToString()); //: "中山北路一段305號", 上車地址
                        a.Add("plat", or.PickupGPS.GetLat().ToString()); //: 25.182815, 上車緯度
                        a.Add("plng", or.PickupGPS.GetLng().ToString()); //: 121.444322, 上車經度
                        a.Add("tcity", or.TakeoffCity.ToString()); //: "桃園市", 下車城市
                        a.Add("tdistinct", or.TakeoffDistinct.ToString()); //: "大園區", 下車區里
                        a.Add("taddress", or.TakeoffAddress.ToString()); //: "桃園國際機場 第二航廈", 下車地址
                        a.Add("tlat", or.TakeoffGPS.GetLat().ToString()); //: 25.076821, 下車緯度
                        a.Add("tlng", or.TakeoffGPS.GetLng().ToString()); //: 121.231683, 下車經度
                        a.Add("pcnt", or.PassengerCnt.ToString()); //: 1, 乘客人數
                        a.Add("bcnt", or.BaggageCnt.ToString()); //: 1, 行李數量
                        a.Add("stage", or.ProcessStage.ToString()); //: "0", 狀態
                        a.Add("cdatetime", or.OrderTime.ToString("yyyy/MM/dd HH:mm").ToString()); //: "2017/02/01 00:05" 建立時間
                        a.Add("van", or.VanpoolFlag); //專接共乘的身份


                        data.Add(a);

                    }

                }

                JsonResult.Add("data", data);

                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);

                //PageTotal = 1;

                JsonResult.Add("totalItem", TotalItem);
                JsonResult.Add("pageTotal", PageTotal);
                JsonResult.Add("nowpage", page);
            }
            catch (Exception e)
            {
                e.ToString();
            }


            return Json(JsonResult);

        }

        public static string Encrypt(string PlanText)
        {
            byte[] bytes = Encoding.Default.GetBytes(PlanText);
            byte[] bytes2 = Encoding.Default.GetBytes("callcar");
            byte[] array = new byte[bytes.Length + bytes2.Length];
            bytes.CopyTo(array, 0);
            bytes2.CopyTo(array, bytes.Length);
            MD5 mD = MD5.Create();
            byte[] array2 = mD.ComputeHash(array);
            byte[] array3 = new byte[array2.Length + bytes2.Length];
            array2.CopyTo(array3, 0);
            bytes2.CopyTo(array3, array2.Length);
            return Convert.ToBase64String(array3);
        }

        public string Decrypt(string CipherText, string key, string iv)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(key);
            byte[] bytes2 = Encoding.UTF8.GetBytes(iv);
            byte[] cipherText = Convert.FromBase64String(CipherText);
            return DecryptStringFromBytes(cipherText, bytes, bytes2);
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            if (cipherText == null || cipherText.Length == 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length == 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length == 0)
            {
                throw new ArgumentNullException("key");
            }
            string result = null;
            using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
            {
                rijndaelManaged.Mode = CipherMode.CBC;
                rijndaelManaged.Padding = PaddingMode.PKCS7;
                rijndaelManaged.FeedbackSize = 128;
                rijndaelManaged.Key = key;
                rijndaelManaged.IV = iv;
                ICryptoTransform transform = rijndaelManaged.CreateDecryptor(rijndaelManaged.Key, rijndaelManaged.IV);
                try
                {
                    using (MemoryStream memoryStream = new MemoryStream(cipherText))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read))
                        {
                            using (StreamReader streamReader = new StreamReader(cryptoStream))
                            {
                                result = streamReader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    result = "keyError";
                }
            }
            return result;
        }

        private static string Query(string aAddress)
        {
            #region old (TGOS)
            ////所要查詢的門牌地址
            ////string aAddress = "台北市內湖區瑞光路76巷43號";
            ////坐標系統(SRS)EPSG:4326(WGS84)國際通用, EPSG:3825 (TWD97TM119) 澎湖及金馬適用,EPSG:3826 (TWD97TM121) 台灣地區適用,EPSG:3827 (TWD67TM119) 澎湖及金馬適用,EPSG:3828 (TWD67TM121) 台灣地區適用
            //string aSRS = "EPSG:4326";
            ////0:最近門牌號機制,1:單雙號機制,2:[最近門牌號機制]+[單雙號機制]
            //int aFuzzyType = 0;
            ////回傳的資料格式，允許傳入的代碼為：JSON、XML
            //string aResultDataType = "JSON";
            ////模糊比對回傳門牌號的許可誤差範圍，輸入格式為正整數，如輸入 0 則代表不限制誤差範圍
            //int aFuzzyBuffer = 0;
            ////是否只進行完全比對，允許傳入的值為：true、false，如輸入 true ，模糊比對機制將不被使用
            //string aIsOnlyFullMatch = "false";
            ////是否鎖定縣市，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [縣市] 要與所輸入的門牌地址中的 [縣市] 完全相同
            //string aIsLockCounty = "false";
            ////是否鎖定鄉鎮市區，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [鄉鎮市區] 要與所輸入的門牌地址中的 [鄉鎮市區] 完全相同
            //string aIsLockTown = "false";
            ////是否鎖定村里，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [村里] 要與所輸入的門牌地址中的 [村里] 完全相同
            //string aIsLockVillage = "false";
            ////是否鎖定路段，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [路段] 要與所輸入的門牌地址中的 [路段] 完全相同
            //string aIsLockRoadSection = "false";
            ////是否鎖定巷，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [巷] 要與所輸入的門牌地址中的 [巷] 完全相同
            //string aIsLockLane = "false";
            ////是否鎖定弄，允許傳入的值為：true、false，如輸入 true ，則代表查詢結果中的 [弄] 要與所輸入的門牌地址中的 [弄] 完全相同
            //string aIsLockAlley = "false";
            ////是否鎖定地區，允許傳入的值為：true、fals，如輸入 true ，則代表查詢結果中的 [地區] 要與所輸入的門牌地址中的 [地區] 完全相同
            //string aIsLockArea = "false";
            ////號之、之號是否視為相同，允許傳入的值為：true、false
            //string aIsSameNumber_SubNumber = "false";
            ////TGOS_Query_Addr_Url WebService網址
            //string aCanIgnoreVillage = "true";
            ////找不時是否可忽略村里，允許傳入的值為：true、false
            //string aCanIgnoreNeighborhood = "true";
            ////找不時是否可忽略鄰，允許傳入的值為：true、false
            //int aReturnMaxCount = 0;
            ////如為多筆時，限制回傳最大筆數，輸入格式為正整數，如輸入 0 則代表不限制回傳筆數





            ////使用string.Format給予參數設定
            //string param = @"oAPPId={0}&oAPIKey={1}&oAddress={2}&oSRS={3}&oFuzzyType={4}
            //             &oResultDataType={5}&oFuzzyBuffer={6}&oIsOnlyFullMatch={7}&oIsLockCounty={8}
            //             &oIsLockTown={9}&oIsLockVillage={10}&oIsLockRoadSection={11}&oIsLockLane={12}
            //             &oIsLockAlley={13}&oIsLockArea={14}&oIsSameNumber_SubNumber={15}&oCanIgnoreVillage={16}&oCanIgnoreNeighborhood={17}&oReturnMaxCount={18}";

            ////給予需要的參數
            //param = string.Format(param,

            //      //應用程式識別碼(APPId) 實由使用者向TGOS申請
            //      QueryAddr.QueryAddraAppId,
            //      //應用程式介接驗證碼(APIKey)由使用者向TGOS申請
            //      QueryAddr.QueryAddrAPIKey,
            //      aAddress,
            //      aSRS,
            //      aFuzzyType,
            //      aResultDataType,
            //      aFuzzyBuffer,
            //      aIsOnlyFullMatch,
            //      aIsLockCounty,
            //      aIsLockTown,
            //      aIsLockVillage,
            //      aIsLockRoadSection,
            //      aIsLockLane,
            //      aIsLockAlley,
            //      aIsLockArea,
            //      aIsSameNumber_SubNumber,
            //      aCanIgnoreVillage,
            //      aCanIgnoreNeighborhood,
            //      aReturnMaxCount
            //  );
            //string Outputstring = string.Empty;
            //Outputstring = QueryAddr.QueryAddrResult(param, QueryAddr.Query_Addr_Url + "/QueryAddr?", Outputstring);
            //return Outputstring;
            #endregion

            #region Google MAP API
            var url = String.Format("https://maps.google.com/maps/api/geocode/json?address={0}&language=zh-TW&key=AIzaSyCmDSfdCv8XurR2G-ECzynxA_FgtdWFw60", aAddress);

            string result = String.Empty;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            using (var response = request.GetResponse())
            using (StreamReader sr = new StreamReader(response.GetResponseStream()))
            {
                //Json格式: 請參考http://code.google.com/intl/zh-TW/apis/maps/documentation/geocoding/
                result = sr.ReadToEnd();
            }
            return result;
            #endregion
        }
    }
}