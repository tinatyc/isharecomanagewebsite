﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CallCar.Models;
using CallCar.BAL;
using System.Configuration;
using System.Transactions;
using DbConn;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;

namespace CallCar.Controllers
{
    public class DispatchController : _AdminController
    {

        public static string ConnectionString = ConfigurationManager.ConnectionStrings["CarpoolLab"].ConnectionString;
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetCars(string fleetID)
        {

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();


            //http://apid.ishareco.com/bapi/gCars?fid=54657828

            //gDrivers

            // string url = Constant.apiBaseUrl + "gCars?fid=" + fleetID;
            RtnStruct r = new RtnStruct();
            BALGeneral bal = new BALGeneral(ConnectionString);
            BALEmployee bale = new BALEmployee(ConnectionString);
            List<OCar> List = bal.GetCars(fleetID);
            List<CarListStruct> ListCar = new List<CarListStruct>();
            if (List != null)
            {
                foreach (OCar c in List)
                {
                    CarListStruct cs = new CarListStruct();
                    cs.CarNo = c.CarNo;
                    cs.DriverName = bale.GetEmployee(c.DriverID).Name;
                    cs.FleetName = bal.GetFleet(c.FleetID).FleetName;
                    cs.ScheduleFlag = c.ScheduleFlag;
                    cs.Type = c.Type;

                    ListCar.Add(cs);
                }

                r.RtnObject = ListCar;
            }
            else
                r.RtnObject = null;

            r.Status = "Success";


            string responseText = JsonConvert.SerializeObject(r);

            //dynamic jsonData = Helper.jsonDecode(responseText);
            string getCarsByFleetResult = responseText;


            JsonResult.Add("data", getCarsByFleetResult);

            return Json(JsonResult);

        }


        public JsonResult GetDrivers(string driverID)
        {

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            DAL.DALGeneral dg = new DAL.DALGeneral(ConnectionString);
            int dID = dg.GetDriverIDByName(driverID);
            List<OCar> List = dg.GetCarsByDriverID(dID);
            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();

            foreach (OCar or in List)
            {
                data.Add("CarNo", or.CarNo.ToString()); //: "CR01E9541", 訂單編號
                data.Add("CarType", or.Type.ToString()); //: "O",  出國O,回國 I
                JsonResult.Add("data", data);
            }
            return Json(JsonResult);

        }

        public ActionResult Item(string id)
        {

            //            http://apid.ishareco.com/bapi/gFleets

            // string url = Constant.apiBaseUrl + "gFleets";

            // string responseText = sendHttpRequest(url);
            RtnStruct r = new RtnStruct();
            BALGeneral bal = new BALGeneral(ConnectionString);
            List<OFleet> List = bal.GetFleets();
            if (List != null)
            {
                List<FleetListStruct> ListFleet = new List<FleetListStruct>();

                foreach (OFleet f in List)
                {
                    FleetListStruct fs = new FleetListStruct();
                    fs.Name = f.FleetName;
                    fs.ID = f.FleetID.ToString();
                    fs.ActiveFlag = f.ActiveFlag;

                    ListFleet.Add(fs);
                }


                r.RtnObject = ListFleet;
            }
            else
                r.RtnObject = null;

            r.Status = "Success";

            string responseText = JsonConvert.SerializeObject(r);
            // dynamic jsonData = Helper.jsonDecode(responseText);
            string GetFleetsResult = responseText;

            DAL.DALDispatch dalr = new DAL.DALDispatch(ConnectionString);
            ODispatchSheet R;
            R = dalr.GetDispatchbyid(id);


            Dictionary<string, dynamic> data = new Dictionary<string, dynamic>();


            data.Add("fid", R.FleetID.ToString()); //: "CR01E9541", 訂單編號
            data.Add("cid", R.CarNo.ToString()); //: 100,  會員編號
            data.Add("did", R.DriverID.ToString()); //: "O",  出國O,回國 I
            data.Add("ty", R.CarType.ToString()); //: "20170207", 乘車日期


            string dataText = "";

            dataText = Helper.jsonEncode(data);

            ViewBag.dataText = dataText;

            ViewBag.GetFleetsResult = GetFleetsResult;

            ViewBag.id = id;


            return View();
        }

        public JsonResult ItemEdit(string rno, string fleetID, string driverID, string carID, string Type, string of, string od, string oc, string ot)
        {

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DAL.DALDispatch dalr = new DAL.DALDispatch(ConnectionString);

            bool rtn = false;
            DAL.DALGeneral dg = new DAL.DALGeneral(ConnectionString);
            int dID = dg.GetDriverIDByName(driverID);
            rtn = dalr.UpdDispatchCarDriver(rno, of, oc, Convert.ToInt32(od), fleetID, carID, dID, Type);

            if (rtn)
            {
                JsonResult.Add("Message", "Success");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonResult.Add("Message", "Failure");
                return Json(JsonResult, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        //public JsonResult GetList(int page = 1, int itemPerPage = 20)
        public JsonResult GetDispatchLocation(string dno)
        {


            RtnStruct r = new RtnStruct();
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();


            //NameValueCollection parameters = new NameValueCollection();

            //string url = Constant.apiBaseUrl + "gDispatchLocation?dno=" + dno;

            BALDispatch bal = new BALDispatch(ConnectionString);
            DispatchLocationsStruct dl = bal.GetDispatchLocationByNo(dno);

            if (dl != null)
            {
                r.Status = "Success";
                r.RtnObject = dl;
            }
            else
            {
                r.Status = "Success";
                r.RtnObject = null;
            }

            //string url = Constant.apiBaseUrl + "gDispatchLocation";

            //parameters.Add("dno", "dno");

            //string responseText = sendHttpRequest(url);

            dynamic jsonData = Helper.jsonEncode(r);
            //string GetDispatchLocationInfoResult = jsonData.GetDispatchLocationInfoResult;
            string GetDispatchLocationInfoResult = jsonData;
            //JsonResult.Add("gg", gg);
            //dynamic jsonData2 = Helper.jsonDecode(GetDispatchLocationInfoResult);

            //string Status = jsonData2.Status;
            //var RtnObject = jsonData2.RtnObject;

            JsonResult.Add("data", GetDispatchLocationInfoResult);


            return Json(JsonResult);


        }



        [HttpPost]
        //public JsonResult GetList(int page = 1, int itemPerPage = 20)
        public JsonResult GetList(int page, string searchJson = null, string orderField = "", string orderType = "", int itemPerPage = 10)
        {

            if (itemPerPage > 100)
            {
                itemPerPage = 100;
            }

            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();

            try
            {

                string QSDate = "";
                string QEDate = "";
                string DispatchNo = "";
                string OrderNo = "";
                string DriverID = "0";
                string CarNo = "";


                if (searchJson != null)
                {
                    try
                    {

                        dynamic search = JObject.Parse(searchJson);

                        QSDate = search.QSDate;
                        QEDate = search.QEDate;
                        DispatchNo = search.DispatchNo;
                        OrderNo = search.OrderNo;

                        if (search.DriverID == "")
                        {
                            DriverID = "0";
                        }
                        else
                        {
                            DriverID = search.DriverID;
                        }

                        CarNo = search.CarNo;

                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }

                }

                //Dictionary<string, dynamic> dataParameters = new Dictionary<string, dynamic>();

                QDispatchCondStruct dataParameters = new QDispatchCondStruct();

                dataParameters.QSDate = QSDate; //查詢派車的開始日期
                dataParameters.QEDate = QEDate; //查詢派車的結束日期
                dataParameters.DispatchNo = DispatchNo; //指定派車單號查詢
                dataParameters.OrderNo = OrderNo; //指定訂單編號查詢
                dataParameters.DriverID = Convert.ToInt32(DriverID); //指定司機查詢
                dataParameters.CarNo = CarNo; //指定車號查詢
                dataParameters.skip = 0; //指定車號查詢
                dataParameters.take = 100; //指定車號查詢

                string dataString = Helper.jsonEncode(dataParameters);

                //NameValueCollection parameters = new NameValueCollection();
                //parameters.Add("data", dataString);
                //parameters.Add("skip", "0");
                //parameters.Add("take", "20");

                //string url = Constant.apiBaseUrl + "gDispatchs?data=" + dataString;
                //string url = Constant.apiBaseUrl + "gDispatchs";

                //string responseText = sendPOST(url, dataString);
                BALDispatch bal = new BALDispatch(ConnectionString);
                BALGeneral balg = new BALGeneral(ConnectionString);
                RtnStruct r = new RtnStruct();
                List<ODispatchSheet> List = bal.GetDispatchByConditions(dataString);
                if (List == null)
                    r.RtnObject = null;
                else
                {
                    List<DispatchBriefStruct> rfsList = new List<DispatchBriefStruct>();

                    foreach (ODispatchSheet or in List)
                    {
                        DispatchBriefStruct rfs = new DispatchBriefStruct();
                        rfs.bc = or.BaggageCnt;
                        rfs.cno = or.CarNo;
                        if (or.FleetID == 0)
                            rfs.cp = "(無車隊)";
                        else
                            rfs.cp = balg.GetFleet(or.FleetID).FleetName;
                        rfs.ct = or.CarType;
                        rfs.dno = or.DispatchNo;
                        rfs.dr = or.DriverName;
                        rfs.pc = or.PassengerCnt;
                        rfs.sc = or.ServiceCnt;
                        rfs.st = or.ServiceType;
                        rfs.td = or.TakeDate;
                        rfs.tt = or.TimeSegment;
                        rfs.did = or.DriverID;
                        rfs.fid = or.FleetID;
                        rfs.poolflag = or.CarpoolFlag;
                        rfs.canpick = or.CanPick;
                        rfsList.Add(rfs);
                    }
                    r.RtnObject = rfsList;
                }
                r.Status = "Success";



                dynamic jsonData = Helper.jsonDecode(JsonConvert.SerializeObject(r));

                //string GetDispatchByConditionResult = jsonData.GetDispatchByConditionResult;
                ////JsonResult.Add("gg", gg);
                //dynamic jsonData2 = Helper.jsonDecode(GetDispatchByConditionResult);

                string Status = jsonData.Status;
                var RtnObject = jsonData.RtnObject;

                int countSkip = itemPerPage * (page - 1);
                int countTake = itemPerPage;

                int TotalItem = 0;
                DAL.DALGeneral dalr = new DAL.DALGeneral(ConnectionString);
                //RepStarDateTime = "2016-12-20";
                //RepEndDateTime = "2017-01-04";

                List<dynamic> data = new List<dynamic>();

                var items = RtnObject;

                if (items != null)
                {

                    foreach (JObject item in items)
                    {
                        TotalItem++;


                        Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                        a.Add("dno", item.GetValue("dno").ToString()); //:"20170118", //搭乘日期
                        a.Add("td", item.GetValue("td").ToString()); //:"20170118", //搭乘日期
                        a.Add("tt", item.GetValue("tt").ToString()); //:"2017/01/18T09:25",//服務時間
                        a.Add("st", item.GetValue("st").ToString()); //:"O", //出回國 (I→回國/O→出國)
                        a.Add("pc", item.GetValue("pc").ToString()); //:5,//全車人數
                        a.Add("bc", item.GetValue("bc").ToString()); //:5, //全車行李數
                        a.Add("sc", item.GetValue("sc").ToString()); //:3, //全車訂單數
                        a.Add("cp", item.GetValue("cp").ToString()); //:"和項車隊",//車隊
                        a.Add("dr", item.GetValue("dr").ToString()); //:"王曉明",//司機
                        a.Add("cno", item.GetValue("cno").ToString()); //:"RAM-2568",//車號
                        a.Add("poolflag", item.GetValue("poolflag").ToString()); //專接共乘
                        if (item.GetValue("cp").ToString().IndexOf("無車隊") == -1)
                        {
                            List<OEmployee> DD = dalr.GetDriversByAll("", item.GetValue("did").ToString(), "", "");
                            foreach (OEmployee or in DD)
                            {
                                a.Add("dp", or.MobilePhone.ToString()); //: 100,  會員編號
                            }
                        }
                        else
                        {
                            a.Add("dp", "");
                        }
                        a.Add("ct", item.GetValue("ct").ToString()); //:"七人座"//車型


                        data.Add(a);

                    }

                }

                JsonResult.Add("data", data);


                double PageTotal = (double)TotalItem / itemPerPage;
                PageTotal = Math.Ceiling(PageTotal);

                //PageTotal = 1;

                JsonResult.Add("totalItem", TotalItem);
                JsonResult.Add("pageTotal", PageTotal);
                JsonResult.Add("nowpage", page);
            }
            catch (Exception ex)
            {
                ex.ToString();

            }


            return Json(JsonResult);

        }

        public JsonResult Getchild(string id)
        {
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            DAL.DALDispatch DDis = new DAL.DALDispatch(ConnectionString);
            BALReservation balr = new BALReservation(ConnectionString);
            try
            {

                ODispatchSheet RRR = DDis.GetDispatchSheetsNoUser(id);

                ODpServiceUnit[] ddd = RRR.ServiceList.ToArray();
                List<dynamic> child = new List<dynamic>();
                Dictionary<string, dynamic> b = new Dictionary<string, dynamic>();
                b.Add("allnum", RRR.ServiceList.Count);
                string ServiceRemark, VanpoolFlag = "";
                for (int i = 0; i < RRR.ServiceList.Count; i++)
                {
                    b.Add("rno" + i, ddd[i].ReservationNo.ToString().Trim());
                    b.Add("ASTF" + i, ddd[i].ScheduleTime.ToString("yyyy/MM/dd").Trim());
                    b.Add("tdate" + i, RRR.TimeSegment.ToString().Trim());
                    b.Add("SFT" + i, ddd[i].ScheduleFlightTime.ToString("yyyy/MM/dd HH:mm").Trim());
                    b.Add("AST" + i, ddd[i].ScheduleTime.ToString("HH:mm").Trim());
                    b.Add("timeseg" + i, ddd[i].TimeSegment.ToString().Trim());
                    b.Add("pcnt" + i, ddd[i].PassengerCnt.ToString().Trim());
                    b.Add("bcnt" + i, ddd[i].BaggageCnt.ToString().Trim());
                    b.Add("paddress" + i, ddd[i].MainAddress.ToString().Trim());
                    b.Add("pname" + i, ddd[i].PassengerName.ToString().Trim());
                    b.Add("pphone" + i, ddd[i].PassengerPhone.ToString().Trim());
                    VanpoolFlag = balr.GetReservationByNo(ddd[i].ReservationNo).VanpoolFlag;
                    b.Add("van" + i, (VanpoolFlag == "M" ? "主" : (VanpoolFlag == "S" ? "從" : "")));
                    ServiceRemark = ddd[i].ServiceRemark.ToString().Trim();
                    b.Add("sremark" + i, (ServiceRemark == "C" ? "完成" : (ServiceRemark == "F" ? "失敗" : (ServiceRemark == "N" ? "NoShow" : "一般"))));
                    child.Add(b);
                }
                JsonResult.Add("child", child);
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return Json(JsonResult);
        }

        public JsonResult gosplitDispatch(string did, string rid)
        {
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            //Dictionary<string, dynamic> dataParameters = new Dictionary<string, dynamic>();

            //dataParameters.Add("d", did);
            //dataParameters.Add("r", rid);
            //string dataString = Helper.jsonEncode(dataParameters);
            //string url = "https://api.ishareco.com/b2b/ishareco/splitDispatch";
            //string responseText = sendPOST(url, dataString);
            //try
            //{
            //    dynamic jsonData = Helper.jsonDecode(responseText);
            //    dynamic RtnObject = jsonData.RtnObject;
            //    JsonResult.Add("Status", jsonData.Status.ToString());
            //}
            //catch (Exception e)
            //{
            //    e.ToString();
            //}




            return Json(JsonResult);
        }

        public JsonResult gochgServeTime(string did, string rid, string Time, string poolflag)
        {
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            Dictionary<string, dynamic> dataParameters = new Dictionary<string, dynamic>();
            List<dynamic> data = new List<dynamic>();
            dataParameters.Add("d", did);
            dataParameters.Add("r", rid);
            dataParameters.Add("t", Time);
            #region old
            //string dataString = Helper.jsonEncode(dataParameters);
            //string url = "https://api.ishareco.com/b2b/ishareco/chgServeTime";
            //string responseText = sendPOST(url, dataString);
            //try
            //{
            //    dynamic jsonData = Helper.jsonDecode(responseText);
            //    dynamic RtnObject = jsonData.RtnObject;
            //    Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

            //    a.Add("Message", jsonData.Status.ToString());

            //    data.Add(a);
            //    JsonResult.Add("data", data);


            //}
            //catch (Exception e)
            //{
            //    e.ToString();
            //}
            //return Json(JsonResult);
            #endregion

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    StringBuilder sb = new StringBuilder();
                    List<MySqlParameter> parms = new List<MySqlParameter>();

                    sb.Append("UPDATE dispatch_reservation SET ScheduleServeTime= @time WHERE DispatchNo = @dno and ReservationNo = @rno");
                    parms.Add(new MySqlParameter("@time", Time));
                    parms.Add(new MySqlParameter("@dno", did));
                    parms.Add(new MySqlParameter("@rno", rid));

                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                    }
                    sb.Length = 0;
                    parms.Clear();

                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                    {
                        sb.Append("SELECT SeqNo FROM reservation_sms	where ReservationNo = @rno");
                        parms.Add(new MySqlParameter("@rno", rid));


                        DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                        if (dt.Rows.Count > 0)
                        {
                            sb.Length = 0;
                            parms.Clear();

                            sb.Append("UPDATE reservation_sms SET ServeTime=@time WHERE ReservationNo = @rno");
                            parms.Add(new MySqlParameter("@time", Time));
                            parms.Add(new MySqlParameter("@rno", rid));


                            conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                        }
                    }
                    sb.Length = 0;
                    parms.Clear();

                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                    {
                        sb.Append("INSERT INTO dispatch_refresh_record (RefreshType, DispatchNo,CarpoolFlag, RefreshFlag) VALUES (@type, @dno,@flag, '0')");
                        parms.Add(new MySqlParameter("@type", "U"));
                        parms.Add(new MySqlParameter("@dno", did));
                        parms.Add(new MySqlParameter("@flag", poolflag));

                        conn.executeInsertQuery(sb.ToString(), parms.ToArray());
                    }
                    scope.Complete();
                }
                Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();
                a.Add("Message", "Success");
                data.Add(a);
                JsonResult.Add("data", data);
                return Json(JsonResult);
            }
            catch (Exception ex)
            {
                Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();
                a.Add("Message", ex.Message);
                data.Add(a);
                JsonResult.Add("data", data);
                return Json(JsonResult);
            }
        }



        public JsonResult goCbDispatch(string fromd, string rid, string tod)
        {
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            Dictionary<string, dynamic> dataParameters = new Dictionary<string, dynamic>();
            List<dynamic> data = new List<dynamic>();
            dataParameters.Add("fromd", fromd);
            dataParameters.Add("r", rid);
            dataParameters.Add("tod", tod);
            string dataString = Helper.jsonEncode(dataParameters);
            string url = "https://api.ishareco.com/b2b/ishareco/CbDispatch";
            string responseText = sendPOST(url, dataString);
            try
            {
                dynamic jsonData = Helper.jsonDecode(responseText);
                dynamic RtnObject = jsonData.RtnObject;
                Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();

                a.Add("Message", jsonData.Status.ToString());
                data.Add(a);
                JsonResult.Add("data", data);
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return Json(JsonResult);
        }

        public JsonResult SetNoShow(string dno, string rno, string poolflag)
        {
            Dictionary<string, dynamic> JsonResult = new Dictionary<string, dynamic>();
            List<dynamic> data = new List<dynamic>();

            StringBuilder sb = new StringBuilder();
            List<MySqlParameter> parms = new List<MySqlParameter>();

            sb.Append("UPDATE dispatch_reservation SET ServiceRemark='N'  WHERE DispatchNo = @dno and ReservationNo = @rno");
            parms.Add(new MySqlParameter("@dno", dno));
            parms.Add(new MySqlParameter("@rno", rno));

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (dbConnectionMySQL conn = new dbConnectionMySQL(ConnectionString))
                    {
                        conn.executeUpdateQuery(sb.ToString(), parms.ToArray());


                        //if (poolflag == "P" || poolflag == "V")
                        //{
                        //    sb.Length = 0;
                        //    parms.Clear();

                        //    sb.Append("SELECT Price FROM reservation_charge_list 	where ReservationNo = @rno");
                        //    parms.Add(new MySqlParameter("@rno", rno));
                        //    DataTable dt = conn.executeSelectQuery(sb.ToString(), parms.ToArray());
                        //    sb.Length = 0;
                        //    parms.Clear();
                        //    if (dt.Rows.Count == 1)
                        //    {
                        //        int Price = Convert.ToInt32(Convert.ToString(dt.Rows[0]["Price"]));
                        //        if (Price > 600)
                        //        {
                        //            Price = 600;

                        //            sb.Append("UPDATE reservation_charge_list SET	Price=@price WHERE ReservationNo = @rno");
                        //            parms.Add(new MySqlParameter("@price", Price));
                        //            parms.Add(new MySqlParameter("@rno", rno));

                        //            conn.executeUpdateQuery(sb.ToString(), parms.ToArray());
                        //        }
                        //    }
                        //}
                    }
                    scope.Complete();
                }
                Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();
                a.Add("Message", "Success");
                data.Add(a);
                JsonResult.Add("data", data);
                return Json(JsonResult);
            }
            catch (Exception ex)
            {
                Dictionary<string, dynamic> a = new Dictionary<string, dynamic>();
                a.Add("Message", ex.Message);
                data.Add(a);
                JsonResult.Add("data", data);
                return Json(JsonResult);
            }


        }
    }
}