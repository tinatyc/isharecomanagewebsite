﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallCar.Models
{
    /// <summary>
    /// 會員帳號物件
    /// </summary>
    public class OAccount
    {
        //Property
        public int ID { get; set; }
        public string Source { get; set; }
        public string SourceID { get; set; }
        public string NickName { get; set; }
        public string FullName { get; set; }
        public string FName { get; set; }
        public string MName { get; set; }
        public string LName { get; set; }
        public int Gender { get; set; }
        public string AgeRange { get; set; }
        public string Local { get; set; }
        public string Email { get; set; }
        public string Birthday { get; set; }
        public string MobilePhone { get; set; }
        public string Password { get; set; }
    }

    /// <summary>
    /// 員工物件
    /// </summary>
    public class OEmployee
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string MobilePhone { get; set; }
        public string CitizenID { get; set; }
        public string HomePhone { get; set; }
        public string Role { get; set; }
        public string CompanyNo { get; set; }
        public int FleetID { get; set; }
        public string City { get; set; }
        public string Distinct { get; set; }
        public string Address { get; set; }
        public string Active { get; set; }
        public string ShiftType { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }

    /// <summary>
    /// 發票物件
    /// </summary>
    public class OInvoice
    {
        public string InvoiceID { get; set; }
        public int UserID { get; set; }
        public string InvoiceType { get; set; }
        public string InvoiceTitle { get; set; }
        public string EIN { get; set; }
        public string InvoiceAddress { get; set; }
        public string DefaultFlag { get; set; }
    }

    /// <summary>
    /// 信用卡物件
    /// </summary>
    public class OCard
    {
        public int ID { get; set; }
        public int UID { get; set; }
        public string Company { get; set; }
        public string CNo { get; set; }
        public string Expire { get; set; }
        public string Code { get; set; }
        public int Check { get; set; }
        public string t { get; set; }
        public string d { get; set; }

    }

    /// <summary>
    /// 取號物件
    /// </summary>
    public class OSerialNum
    {
        public string Purpose { get; set; }
        public string Prefix { get; set; }
        public int Digit { get; set; }
        public string Current { get; set; }
        public int Used { get; set; }
    }

    /// <summary>
    /// 訂單物件
    /// </summary>
    public class OReservation
    {
        //Porperty
        public string ReservationNo { get; set; } //訂單編號
        public int UserID { get; set; }  //用戶序號
        public string ServiceType { get; set; } //出國或回國
        public string ServeDate { get; set; } //乘車日期
        public string PreferTime { get; set; } //預約時段
        public string ChooseType { get; set; } //航班或航廈
        public string PickCartype { get; set; } //預約或撿趟
        public string CarpoolFlag { get; set; } // 專車共乘
        public string FlightNo { get; set; }  //航班編號
        public string ScheduleServeTime { get; set; }  //航班編號
        public string FlightDate { get; set; } //航班日期
        public DateTime FlightTime { get; set; } //航班時間
        public string PickupCity { get; set; } //上車地點
        public string PickupDistinct { get; set; } //上車地點
        public string PickupVillage { get; set; }
        public string PickupAddress { get; set; } //上車地點
        public OSpatialGPS PickupGPS { get; set; }
        public string TakeoffCity { get; set; } //上車地點
        public string TakeoffDistinct { get; set; } //上車地點
        public string TakeoffVillage { get; set; }
        public string TakeoffAddress { get; set; } //上車地點
        public OSpatialGPS TakeoffGPS { get; set; }
        public int PassengerCnt { get; set; } //人數
        public int BaggageCnt { get; set; } //行李數量
        public string MaxFlag { get; set; } //挑戰滿車
        public string Coupon { get; set; } //優惠
        public string Invoice { get; set; } //發票資料
        public string Credit { get; set; } //信用卡資料
        public DateTime OrderTime { get; set; } //下訂時間
        public DateTime udpTime { get; set; } //下訂時間
        public string ProcessStage { get; set; } //處理階段
        public string EIN { get; set; } //回傳的發票號
        public int TrailPrice { get; set; } //金額
        public int realamt { get; set; }
        public int carteam { get; set; } //金額
        public string PassengerName { get; set; } //乘車人姓名
        public string PassengerPhone { get; set; } //乘車人電話
        public string VanpoolFlag { get; set; }
        public int BabySeatCnt { get; set; }
        //public class SpatialGPS
        //{
        //    public SpatialGPS(double lat{get;set;} double lng)
        //    {
        //        this.lat = lat;
        //        this.lng = lng;
        //    }
        //    public double GetLat()
        //    {
        //        return this.lat;
        //    }

        //    public double GetLng()
        //    {
        //        return this.lng;
        //    }
        //    private double lat { get; set; }
        //    private double lng { get; set; }


        //}
    }

    public class OReservation_process_log
    {
        public string ReservationNo { get; set; } //訂單編號
        public string ProcessStage { get; set; } //訂單編號
        public string Memo { get; set; } //訂單編號

    }

    /// <summary>
    /// 定期航班物件
    /// </summary>
    public class OScheduledFlight
    {
        public string FlightType { get; set; }
        public string FlightDate { get; set; }
        public string AirlineID { get; set; }
        public string FlightNo { get; set; }
        public string DepartureAirportID { get; set; }
        public string DepartureTime { get; set; }
        public string ArrivalAirportID { get; set; }
        public string ArrivalTime { get; set; }
        public string Ternimal { get; set; }
    }

    /// <summary>
    /// 假日物件
    /// </summary>
    public class OHoliday
    {
        public string HolidayName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string LastRDate { get; set; }
    }

    /// <summary>
    /// 費用物件
    /// </summary>
    public class OFare
    {
        public string ChargeCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ChargeName { get; set; }
        public string ChargeType { get; set; }
        public string FlowType { get; set; }
        public int MinQty { get; set; }
        public string Currency { get; set; }
        public int Amount { get; set; }
    }

    public class OPickCarInfo
    {
        public OPickCarInfo()
        {
            Reservations = new List<DispatchUnit>();
            this.ShowFlag = false;
        }
        public string DispatchNo { get; set; }
        public string TakeDate { get; set; }
        public string TimeRange { get; set; }
        public string ServiceType { get; set; }
        public List<DispatchUnit> Reservations { get; set; }
        public int RemainPCapacity { get; set; }
        public int RemainBCapacity { get; set; }
        public int StopCnt { get; set; }
        public string CarType { get; set; }
        public string CarNo { get; set; }
        public string DriverID { get; set; }
        public string DriverName { get; set; }
        public bool ShowFlag { get; set; }
        public string MaxFlag { get; set; }
        public DateTime ShowServiceTime { get; set; }

        public class DispatchUnit
        {
            public string No { get; set; }
            public int SeqNo { get; set; }
            public double lat { get; set; }
            public double lng { get; set; }
            public DateTime ScheduleTime { get; set; }
        }
    }

    public class ODispatchSheet
    {
        public ODispatchSheet()
        {
            this.ServiceList = new List<ODpServiceUnit>();
        }

        public string DispatchNo { get; set; }
        public string TakeDate { get; set; }
        public string ServiceType { get; set; }
        public string TimeSegment { get; set; }
        public List<ODpServiceUnit> ServiceList { get; set; }
        public int PassengerCnt { get; set; }
        public int BaggageCnt { get; set; }
        public int ServiceCnt { get; set; }
        public int FleetID { get; set; }
        public string FleetName { get; set; }
        public string CarType { get; set; }
        public string CarNo { get; set; }
        public int DriverID { get; set; }
        public string DriverName { get; set; }
        public string FisrtServiceTime { get; set; }
        public string ServiceRemark { get; set; }
        public string UpdFlag { get; set; }
        public string CarpoolFlag { get; set; }
        public string CanPick { get; set; }


    }

    public class ODpServiceUnit
    {
        public string uid { get; set; }
        public string TimeSegment { get; set; }
        public string DispatchNo { get; set; }
        public string ReservationNo { get; set; }
        public string PassengerName { get; set; }
        public string PassengerPhone { get; set; }
        public int ServiceOrder { get; set; }
        public int PassengerCnt { get; set; }
        public int BaggageCnt { get; set; }
        public int AddBaggageCnt { get; set; }
        public string DiscountType { get; set; }
        public string Coupon { get; set; }
        public string FlightNo { get; set; }
        public DateTime ScheduleFlightTime { get; set; }
        public string MainAddress { get; set; }
        public string AirportAddress { get; set; }
        public OSpatialGPS ServiceGPS { get; set; }
        public DateTime ScheduleTime { get; set; }
        public DateTime ActualTime { get; set; }
        public string ServiceRemark { get; set; }
    }

    public class ORsChargeList
    {
        public string ReservationNo { get; set; }
        public string SelectionType { get; set; }
        public string FlightNo { get; set; }
        public string FlightDate { get; set; }
        public DateTime FlightTime { get; set; }
        public string ServiceRemark { get; set; }
        public string ChargeResult { get; set; }
        public string FlightRemark { get; set; }
        public DateTime ActualFlightTime { get; set; }
        public int Price { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }

    public class ODriverServiceRecord
    {
        public string Dno { get; set; }
        public int DriverID { get; set; }
        public string SheetServiceRemark { get; set; }
        public string UpdFlag { get; set; }
        public string Reservation1 { get; set; }
        public string ServiceRemark1 { get; set; }
        public DateTime ServiceTime1 { get; set; }
        public string Reservation2 { get; set; }
        public string ServiceRemark2 { get; set; }
        public DateTime ServiceTime2 { get; set; }
        public string Reservation3 { get; set; }
        public string ServiceRemark3 { get; set; }
        public DateTime ServiceTime3 { get; set; }
    }

    public class OSpatialGPS
    {
        public OSpatialGPS(double lat, double lng)
        {
            this.lat = lat;
            this.lng = lng;
        }
        public double GetLat()
        {
            return this.lat;
        }

        public double GetLng()
        {
            return this.lng;
        }
        public double lat { get; set; }
        public double lng { get; set; }

    }

    public class OArrivalFlightInfo
    {
        public string FlightDate { get; set; }
        public string FlightNumber { get; set; }
        public int AirRouteType { get; set; }
        public string AirlineID { get; set; }
        public string DepartureAirportID { get; set; }
        public string ArrivalAirportID { get; set; }
        public string ScheduleArrivalTime { get; set; }
        public string ActualArrivalTime { get; set; }
        public string ArrivalRemark { get; set; }
        public string ArrivalTerminal { get; set; }
        public string ArrivalGate { get; set; }
        public string UpdateTime { get; set; }
    }

    public class ODepartureFlightInfo
    {
        public string FlightDate { get; set; }
        public string FlightNumber { get; set; }
        public int AirRouteType { get; set; }
        public string AirlineID { get; set; }
        public string DepartureAirportID { get; set; }
        public string ArrivalAirportID { get; set; }
        public string ScheduleDepartureTime { get; set; }
        public string ActualDepartureTime { get; set; }
        public string DepartureRemark { get; set; }
        public string DepartureTerminal { get; set; }
        public string DepartureGate { get; set; }
        public string UpdateTime { get; set; }
    }

    public class OMachSourceData
    {
        public OMachSourceData()
        {

            this.ReservationList = new List<OMatchRData>();
            this.CarList = new List<OMatchCData>();
        }

        public string MatchDate { get; set; }
        public List<OMatchRData> ReservationList { get; set; }
        public List<OMatchCData> CarList { get; set; }
    }

    public class OMatchRData
    {
        public string ReservationNo { get; set; }
        public string ServiceType { get; set; }
        public string TimeSegment { get; set; }
        //public string PickupCity { get; set; }
        //public string PickupDistinct { get; set; }
        //public string PickupAddress { get; set; }
        public double PickupLat { get; set; }
        public double PickupLng { get; set; }
        //public string TakeoffCity { get; set; }
        //public string TakeoffDistinct { get; set; }
        //public string TakeoffAddress { get; set; }
        public double TakeoffLat { get; set; }
        public double TakeoffLng { get; set; }
        public int PassengerCnt { get; set; }
        public int BaggageCnt { get; set; }
        public string MaxFlag { get; set; }
    }

    public class OMatchCData
    {
        public int ScheduleID { get; set; }
        public string ShiftType { get; set; }
        public string CarType { get; set; }
        public int NCapacity { get; set; }
        public int MCapacity { get; set; }
    }

    public class OScheduleData
    {
        public int ScheduleID { get; set; }
        public string ScheduleDate { get; set; }
        public string ShiftType { get; set; }
        public string CarNo { get; set; }
        public int DriverID { get; set; }
        public int FleetID { get; set; }
        public string CarType { get; set; }
    }

    public class OCompany
    {
        public OCompany() { }

        public void SetFinData(string Key, string IV, string StoreID)
        {
            this.Pay2GoKey = Key;
            this.Pay2GoStoreID = StoreID;
            this.Pay2GoIV = IV;
        }

        public string GetPay2GoKey()
        {
            return this.Pay2GoKey;
        }

        public string GetPay2GoIV()
        {
            return this.Pay2GoIV;
        }

        public string GetPay2GoStoreID()
        {
            return this.Pay2GoStoreID;
        }

        public string CompanyNo { get; set; }
        public string CompanyName { get; set; }
        public bool ActiveFlag { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }

        private string Pay2GoKey { get; set; }
        private string Pay2GoIV { get; set; }
        private string Pay2GoStoreID { get; set; }
    }

    public class OFleet
    {
        public OFleet() { }

        public int FleetID { get; set; }
        public string FleetName { get; set; }
        public string Company { get; set; }
        public bool ActiveFlag { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }


    }

    public class OCar
    {
        public string CarNo { get; set; }
        public int FleetID { get; set; }
        public string Type { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string MDate { get; set; }
        public string Color { get; set; }
        public string Displacement { get; set; }
        public int DriverID { get; set; }
        public string ScheduleFlag { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdTime { get; set; }
    }

    public class OPay2goResult
    {
        public OPay2goResult()
        {
            this.Result = new List<OPay2goResultUnit>();
        }
        public List<OPay2goResultUnit> Result { get; set; }

    }
    public class OPay2goResultUnit
    {
        public string Index { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string Result { get; set; }
    }

    public class Charge_Data_S
    {
        public string MerchantID { get; set; }
        public string Amt { get; set; }
        public string TokenLife { get; set; }
        public string MerchantOrderNo { get; set; }
        public string TradeNo { get; set; }

        public string CheckCode { get; set; }
    }

    public class O24Reservation
    {
        //Porperty
        public string ReservationNo { get; set; } //訂單編號
        public int UserID { get; set; }  //用戶序號
        public string ServiceType { get; set; } //出國或回國
        public string TakeDate { get; set; } //乘車日期
        public string TimeSegment { get; set; }
        public string FlightNo { get; set; }
        public string address { get; set; }
        public string airsTer { get; set; }
        public string name { get; set; }
    }
    public class OGoPaylist
    {
        public OGoPaylist()
        {
            this.UnitList = new List<OGoPaylistUnit>();
        }
        public List<OGoPaylistUnit> UnitList { get; set; }
    }

    public class OGoPaylistUnit
    {
        public string ReservationNo { get; set; }
        public string UserID { get; set; }
        public string ServiceType { get; set; }
        public string takedate { get; set; }
        public string PassengerCnt { get; set; }
        public string BaggageCnt { get; set; }
        public string AddBaggageCnt { get; set; }
        public string DiscountType { get; set; }
        public string Coupon { get; set; }
        public string InvoiceData { get; set; }
        public string RealAmt { get; set; }
        public string PromoteAmt { get; set; }
        public string CollectAmt { get; set; }
        public string TotalAmt { get; set; }
        public string OfficialTotalAmt { get; set; }
        public string CarFee { get; set; }
        public string NightFee { get; set; }
        public string BaggageFee { get; set; }
        public string OtherFee { get; set; }
        public string OtherSaleFee { get; set; }
        public string Price { get; set; }
        public string ChargeResult { get; set; }
        public string UserName { get; set; }
        public string ServiceRemark { get; set; }
        public string CarpoolFlag { get; set; }
        public string VanpoolFlag { get; set; }
    }
    public class OErrorSheet
    {
        //Porperty
        public string ReservationNo { get; set; } //訂單編號
        public string TakeDate { get; set; } //乘車日期
        public string TimeSegment { get; set; }
        public string passengername { get; set; }
        public string passengerphone { get; set; }
    }

    public class ORecev
    {
        //Porperty
        public string DispatchNo { get; set; } //訂單編號
        public string ReservationNo { get; set; } //出國或回國
        public string takedate { get; set; } //乘車日期
        public string TimeSegment { get; set; } //預約時段
        public int PassengerCnt { get; set; } //人數
        public int APassengerCnt { get; set; } //人數
        public int BaggageCnt { get; set; } //行李數量
        public int ABaggageCnt { get; set; } //行李數量
        public string Coupon { get; set; } //優惠
        public int Price { get; set; } //金額
        public int realamt { get; set; }
        public string ChargeResult { get; set; } //金額
        public string UpdTime { get; set; } //金額
        public string Memo { get; set; } //金額
    }

    public class OPay
    {
        //Porperty
        public string DispatchNo { get; set; } //訂單編號
        public string ReservationNo { get; set; } //出國或回國
        public string takedate { get; set; } //乘車日期
        public string TimeSegment { get; set; } //預約時段
        public string address { get; set; } //人數
        public int APassengerCnt { get; set; } //人數
        public int stop { get; set; } //行李數量
        public string night { get; set; } //行李數量
        public string baby { get; set; } //優惠
        public string car { get; set; } //金額
        public string carnum { get; set; }
        public string team { get; set; }
        public string service { get; set; } //金額
        public string Memo { get; set; } //金額
    }

    public class OCoupon
    {
        public string CouponCode { get; set; } //訂單編號
        public string StartDate { get; set; } //出國或回國
        public string EndDate { get; set; } //乘車日期
        public int CarFeeSub { get; set; } //預約時段
        public int FixCarFee { get; set; } //人數
        public int NightFeeSub { get; set; } //人數
        public string Memo { get; set; } //行李數量
        public int UseCnt { get; set; } //行李數量
        public int LimitCnt { get; set; } //優惠
        public string TakeDateS { get; set; } //金額
        public string TakeDateE { get; set; }
        public string CreateTime { get; set; }
        public string UpdTime { get; set; } //金額
    }
}