﻿var listJsonData = null;
var a;
var addressFrameID = '';

String.prototype.rtrim = function (s) {
    return this.replace(new RegExp(s + "*$"), '');
};


function toPage(aName, cName) {

    if (typeof (cName) == 'undefined') {
        document.location = baseUrl + '/' + controllerName + '/' + aName;
    } else {
        document.location = baseUrl + '/' + cName + '/' + aName;
    }

}

var isDisplayLog = true;
function log(x) {
    if (isDisplayLog) {
        console.log(x);
    }
}

function setDatetimepicker() {

    $('.datetimepicker').datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: 'HH:mm',
        format: 'YYYY-MM-DD HH:mm:ss',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });
}

function setSummernote() {
    $('.summernote').summernote({
        /*
		 toolbar : [
		 //['style', ['style']], // no style button
		 ['style', ['bold', 'italic', 'underline', 'clear']], ['fontsize', ['fontsize']], ['color', ['color']], ['para', ['ul', 'ol', 'paragraph']], ['height', ['height']], ['insert', ['picture', 'link', 'video']] // no insert buttons
		 //['table', ['table']], // no table button
		 //['help', ['help']] //no help button
		 ],
		 */
        height: 400,
        callbacks: {
            onImageUpload: function (files, editor, welEditable) {
                // sendFile(files[0], editor, welEditable);
                for (var i = files.length - 1; i >= 0; i--) {
                    sendFile(files[i], this);
                }
            }
        }
    });

}

function switchLanguage(x) {

    // $('*[language]').stop();
    if (x == 'all') {
        $('*[language]').slideDown();
    } else {
        $('*[language]').slideUp();
        $('*[language="' + x + '"]').slideDown();
    }
}

function pageReload() {
    location.reload();
}

function setRadioValue(name, value) {
    $('input[name=' + name + ']').each(function () {
        if ($(this).val() == value) {
            $(this).attr('checked', true);
        }
    });
}

function setFormData(formID, d) {
    assignFormValue(formID, d);
}

function assignFormValue(formID, d) {
    for (var key in d) {
        var e = $('#' + formID + ' *[name="' + key + '"]');
        var tagName = $(e).prop("tagName");
        var v = d[key];

        if (e != null && v != null) {
            if (typeof (tagName) != 'undefined') {
                if (tagName == 'INPUT' || tagName == 'SELECT') {

                    if ($(e).attr('type') == 'radio' || $(e).attr('type') == 'checkbox') {
                        $('input[name=' + key + '][value=' + v + ']').prop('checked', true);
                    } else {
                        if ($(e).attr('type') != 'file') {
                            $(e).val(v);
                        }
                    }

                } else {
                    $(e).html(v);
                }
            }
        }
    }
}

//
// function setData(frameID, data) {
//
// for (var i in data) {
//
// var x = data[i];
//
// if ( typeof (x) == 'object') {
// setData(frameID, x);
// } else {
// $('#frameID');
//
// }
//
// }
//
// }

/*

 function setReadMode(containerID) {

 $('#' + containerID).find('input').prop('readonly', 'readonly');
 $('#' + containerID).find('select').prop('readonly', 'readonly');
 $('#' + containerID).find('textarea').prop('readonly', 'readonly');

 $('#' + containerID).find('input').prop('disabled', true);
 $('#' + containerID).find('select').prop('disabled', true);
 $('#' + containerID).find('textarea').prop('disabled', true);

 }*/

function getModalMode() {
    return modalMode;
}


$(document).ready(function () {


    if (typeof (data) != 'undefined') {
        if (data != null) {
            assignFormValue('form', data);
        } else {

        }
    }

    setDatepicker();

    /*
   
    setDatetimepicker();
    setSummernote();
    */

    //initModal();

    //number inpuy
    // $('input[type=number]').keypress(isNumberKey);
    $('input[type=number]').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


});

function resetBodyHeight() {
    var leftSide = $('.left-side').height();
    var mainContent = $('.main-content .wrapper').height();

    if (leftSide > mainContent) {
        $('.main-content').height(leftSide + 100);
    } else {
        $('.main-content').height(mainContent + 200);
    }

}

var pickerSetValueFrameID = '';

function doSearch() {
    var text = $('#headerSearchInput').val();
    if ($.trim(text) != '') {
        toPage('/search/list/' + encodeURI(text));
    } else {
        alert('請輸入關鍵字');

    }

}

function searchEnter(e) {
    if (e.keyCode == 13) {
        doSearch();
    }

}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function setDatepicker() {
    // var option = {
    // dateFormat : 'yy-mm-dd',
    // changeYear : true
    // };

    var option = {
        dateFormat: 'yy/mm/dd',
        changeYear: true,
        //monthNames : ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        yearRange: '-100:+10',
        // minDate : '-20y',
        //maxDate : '+0d',
    };

    //$("input.datepicker").datepicker(option);


    $('input.datepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYYMMDD'
        }
    }, function (start, end, label) {

    });

    $('input.datepickertime').daterangepicker({
        timePicker: true,
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY/MM/DD HH:mm'
        }
    }, function (start, end, label) {

    });
}

/*
 function setDatetimepicker() {
 var option = {
 timeFormat : 'HH:mm:ss',
 dateFormat : 'yy-mm-dd',
 changeYear : true
 };
 $("input[rel=datetimepicker]").datetimepicker(option);
 }
 */

function int(v) {
    return parseInt(v);
}

function float(v) {
    return parseFloat(v);
}

function isUrl(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
	'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
	'((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
	'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
	'(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
	'(\\#[-a-z\\d_]*)?$', 'i');
    // fragment locator

    // fragment locater
    if (!pattern.test(str)) {
        // alert("Please enter a valid URL.");
        return false;
    } else {
        return true;
    }
}

function filterArea() {
    var v = $('#cityID').val();
    $('#areaID').empty();
    //$('#areaID').append('<option value="0">全部區域</option>');
    $('#areaIDClone option[rel=' + v + ']').each(function () {
        $('#areaID').append($(this).clone());
    });
}



function setListHtml(html, type) {

    if (typeof (type) == 'undefined') {
        type = 'table';
    }

    if (html == '') {
        if (type == 'table') {
            return '<tr><td colspan="99" class="text-center">搜尋完成, 找不到任何項目</td></tr>';
            //return '<tr><td colspan="99" class="text-center">Search completed, didn\'t found anything :(</td></tr>';
        } else {
            return '搜尋完成, 找不到任何項目 :(';
            //return ' completed, didn\'t found anything :(';
        }
    } else {
        return html;
    }
}

function afterGetList(containerID) {

}
function afterGetList2(containerID,id) {
    for (var i in id) {
        var x = id[i];
        console.log("afterGetList2");
        CreateGroup('group' + v(x['dno']));
    }
}
function afterGetList3(containerID, id) {
    for (var i in id) {
        var x = id[i];
        CreateGroup('group' + v(x['rno']));
    }
}
function v(v) {
    if (v == null) {
        return '';
    } else {
        return v;
    }
}

function toTop() {
    $('html, body').animate({
        scrollTop: 0
    }, 'fast');
}

function isMobile() {
    if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
        return true;

    } else {
        return false;
    }
}

function beforeGetList(containerID) {
    console.log("containerID = " + containerID);
    $('.list tbody').html('<tr><td colspan="99" style="text-align:center">搜尋中, 請稍候...</td></tr>');
}

function beforeInsertData(containerID) {
    console.log("containerID = " + containerID);
    $('.list tbody').html('<tr><td colspan="99" style="text-align:center">新增中, 請稍候...</td></tr>');
}

function getQueryParams(qs) {
    qs = qs.split("+").join(" ");
    var params = {},
	    tokens,
	    re = /[?&]?([^=]+)=([^&]*)/g;
    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }
    return params;
}

function setListSearchParameter(containerID, searchParameter) {
    for (var i in searchParameter) {
        var x = searchParameter[i];
        $('#' + containerID).find('input[name=' + i + ']').val(x);
    }
}

function htmlEncode(value) {
    return $('<div/>').text(value).html();
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

function htmlEscape(str) {
    return String(str).replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

//guid
var getGuid = (function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    return function () {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    };
})();

function autoSetMenuActive() {
    $('#sideMenuFrame a[controllerName="' + controllerName + '"]').addClass('bold');
    // $('#sideMenuFrame a[controllerName="' + controllerName + '"]').addClass('underline');
    $('#sideMenuFrame a[controllerName="' + controllerName + '"]').addClass('darkPurple');
    $('#sideMenuFrame a[controllerName="' + controllerName + '"]').parent().parent().parent().addClass('nav-active');
}

function manualSetMenuActive(name) {
    $('#sideMenuFrame a[controllerName="' + name + '"]').addClass('bold');
    // $('#sideMenuFrame a[controllerName="' + controllerName + '"]').addClass('underline');
    $('#sideMenuFrame a[controllerName="' + name + '"]').addClass('darkPurple');
    $('#sideMenuFrame a[controllerName="' + name + '"]').parent().parent().parent().addClass('nav-active');
}

function setMenuActive(x) {
    $('#menu' + x).addClass('nav-active');
}

// list functions--------------------------------------------------start---------------------------------------------------

var listContainerID = 'list';
var listCondition;
var searchCookieName = controllerName + '.' + actionName + '.search';

function listConditionInit() {
    listCondition = {};
    var x = {};
    x['currentPage'] = 1;
    x['orderField'] = '';
    x['orderType'] = 'desc';
    x['itemPerPage'] = 10;
    x['getListUrl'] = baseUrl + '/' + controllerName + '/GetList';

    listCondition['list'] = x;
}

//get page on ready
var getListRespond = function (r) {
    //on each list page
    var html = getListHtml(r['data'], r['nowpage']);

    listCondition['list']['data'] = r['data'];

    html = setListHtml(html);
    //set html to table
    $('#' + listContainerID).find('.list tbody').html(html);

    //set page html
    setPageFrame(listContainerID, r['pageTotal'], r['totalItem']);
    //something to do when set html on table
    afterGetList(listContainerID);

};

var getListRespond2 = function (r) {
    //on each list page

    var html = getListHtml(r['data'], r['nowpage']);

    listCondition['list']['data'] = r['data'];

    html = setListHtml(html);
    //set html to table
    $('#' + listContainerID).find('.list tbody').html(html);

    //set page html
    setPageFrame(listContainerID, r['pageTotal'], r['totalItem']);
    //something to do when set html on table
    afterGetList2(listContainerID, r['data']);



};
var getListRespond3 = function (r) {
    //on each list page

    var html = getListHtml(r['data'], r['nowpage']);

    listCondition['list']['data'] = r['data'];

    html = setListHtml(html);
    //set html to table
    $('#' + listContainerID).find('.list tbody').html(html);

    //set page html
    setPageFrame(listContainerID, r['pageTotal'], r['totalItem']);
    //something to do when set html on table
    afterGetList3(listContainerID, r['data']);



};
var getListRespondRecev = function (r) {
    //on each list page
    var listContainerID2 = 'listRecev';

    var html = getListHtmlRecev(r['data']);

   // listCondition['listRecev']['data'] = r['data'];

    html = setListHtml(html);
    //set html to table
    $('#' + listContainerID2).find('.listRecev tbody').html(html);

    //set page html
    setPageFrame(listContainerID2, r['pageTotal'], r['totalItem']);
    //something to do when set html on table
    afterGetList(listContainerID2);

};

var getListRespondPay = function (r) {
    //on each list page
    var listContainerID2 = 'listPay';

    var html = getListHtmlPay(r['data']);

    // listCondition['listRecev']['data'] = r['data'];

    html = setListHtml(html);
    //set html to table
    $('#' + listContainerID2).find('.listPay tbody').html(html);

    //set page html
    setPageFrame(listContainerID2, r['pageTotal'], r['totalItem']);
    //something to do when set html on table
    afterGetList(listContainerID2);

};

function goToPage() {

}

//list page - get list
function getList() {

    beforeGetList(listContainerID);

    var searchParameters = $('#' + listContainerID).find('.search').serialize();

    if (typeof (listCondition[listContainerID]) == 'undefined') {
        url = baseUrl + '/' + controllerName + '/getList';
    } else {
        url = listCondition[listContainerID]['getListUrl'];
    }

    var currentPage = listCondition[listContainerID]['currentPage'];
    var orderField = listCondition[listContainerID]['orderField'];
    var orderType = listCondition[listContainerID]['orderType'];
    var itemPerPage = $('#' + listContainerID).find('.itemPerPage').val();


    var searchJson = getJsonString(listContainerID, 'search');

    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {
            page: currentPage,
            orderField: orderField,
            orderType: orderType,
            itemPerPage: itemPerPage,
            searchJson: searchJson
        },
        success: getListRespond
    });

}

function initListPage() {
    //set search parameter if exists
    /*
	 if ($.cookie(searchCookieName) != null) {
	 var searchParameter = getQueryParams($.cookie(searchCookieName));
	 setListSearchParameter(listContainerID, searchParameter);
	 }
	 */

    try {
        var search = location.search.substring(1);
        var urlData = JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');

        for (var i in urlData) {
            $('*[name="' + i + '"]').val(urlData[i]);
        }
    } catch (e) {
    }
    setListEvent(listContainerID);
    listConditionInit();
}

function deleteItem(id, e) {
    if (confirm('確認刪除?')) {
        var url = baseUrl + '/' + controllerName + '/DeleteDo';
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                ID: id
            },
            success: function (r) {
                if (r == true) {
                    $(e).parent().parent().remove();
                }

            }
        });
    }
}

function setListEvent(containerID) {
    //set order event
    //setOrderEvent();
    //set search  event
    setSearchEvent();

    //item per page onchange event
    $('#' + containerID).find('.itemPerPage').change(function (e) {
        getList(containerID);
    });

    $('#' + containerID).find('.buttonClear').click(function (e) {
        //clear search field
        $('#' + containerID).find('.search').val('');
    });

    $('#' + containerID).find('.buttonGoToPage').click(function (e) {
        //clear search field
        var goToPage = $('#' + containerID).find('.goToPage').val();
        goToPage = int(goToPage);
        if (goToPage > 0) {
            setPage(containerID, goToPage);
        }
        // $('#' + containerID).find('.search').val('');
    });

    $('#' + listContainerID).find('.goToPage').keypress(function (e) {
        if (e.which == 13) {
            var goToPage = $('#' + containerID).find('.goToPage').val();
            goToPage = int(goToPage);
            if (goToPage > 0) {
                setPage(containerID, goToPage);
            }
        }
    });

    // $('#' + containerID).find('.buttonSearch').click(function(e) {
    // getList(containerID);
    // });

}

function setPageFrame(containerID, pageTotal, totalItem) {

    html = '';

    if (pageTotal != 0) {

        //add prev page html
        var page = 1;
        if (typeof (listCondition[containerID]) != 'undefined' && typeof (listCondition[containerID]['currentPage']) != 'undefined') {
            page = listCondition[containerID]['currentPage'];
        }

        if (page > 1) {

            // if (page >= 1) {
            html += '<li class="prev "  onclick="setPage(\'' + containerID + '\', ' + '1' + ')"><a href="#">第一頁</a></li>';
            // } else {
            // html += '<li class="prev"  onclick="setPage(\'' + containerID + '\', ' + '1' + ')"><a href="#">First page </a></li>';

            // }

            //html += '<li class="prev" onclick="setPage(\'' + containerID + '\', ' + (page - 1) + ')"><a href="#">← 上一頁</a></li>';
            html += '<li class="prev" onclick="setPage(\'' + containerID + '\', ' + (page - 1) + ')"><a href="#">←</a></li>';

        }

        //previous 10 page button
        if (page > 10) {
            //html += '<li class="pageItem" onclick="setPage(\'' + containerID + '\', ' + (page - 10) + ')"><a>上10頁</a></li>';
        }

        var fromPage = page - 4;
        if (fromPage < 1) {
            fromPage = 1;
        }
        var toPage = page + 4;
        if (toPage > pageTotal) {
            toPage = pageTotal;
        }

        for (var i = fromPage; i <= toPage; i++) {

            if (page == i) {
                html += '<li class="active" onclick="setPage(\'' + containerID + '\', ' + i + ')"><a>' + i + '</a></li>';
            } else {
                html += '<li class="" onclick="setPage(\'' + containerID + '\', ' + i + ')"><a>' + i + '</a></li>';
            }
        }

        //next 10 page button
        if ((pageTotal - 10) > page) {
        }

        //add next page html
        if (page < pageTotal) {

            //html += '<li class="next"  onclick="setPage(\'' + containerID + '\', ' + (page + 1) + ')"><a href="#">下一頁 →</a></li>';
            html += '<li class="next"  onclick="setPage(\'' + containerID + '\', ' + (page + 1) + ')"><a href="#">→</a></li>';

            // if (page == pageTotal) {
            // html += '<li class="next disabled" onclick="setPage(\'' + containerID + '\')"><a href="#">Last page</a></li>';
            // } else {
            //
            //
            // }
            html += '<li class="next" onclick="setPage(\'' + containerID + '\', ' + pageTotal + ')"><a href="#">最後頁</a></li>';

        }

        var currentPage = 1;
        if (typeof (listCondition[containerID]) != 'undefined') {
            if (typeof (listCondition[containerID]['currentPage']) != 'undefined') {
                currentPage = listCondition[containerID]['currentPage'];
            }

        }
    }

    $('#' + containerID).find('.pageFrame').html(html);

    if (pageTotal == 0) {
        $('#' + containerID).find('.currentPage').text(0);
    } else {
        $('#' + containerID).find('.currentPage').text(currentPage);
    }

    $('#' + containerID).find('.currentPage').text(currentPage);

    $('#' + containerID).find('.totalPage').text(pageTotal);
    $('#' + containerID).find('.totalItem').text(totalItem);
}

function setPage(containerID, v) {
    //currentPage[containerID] = v;
    listCondition[containerID]['currentPage'] = v;
    getList();
}

function setSearchEvent() {

    $('#' + listContainerID).find('.search').keypress(function (e) {
        if (e.which == 13) {
            listCondition[listContainerID]['currentPage'] = 1;
            getList();
        }
    });
    //search Button event
    $('#' + listContainerID).find('.buttonSearch').click(function () {
        listCondition[listContainerID]['currentPage'] = 1;
        console.log(listContainerID);
        getList();
    });
}

function setOrderEvent() {
    $('#' + listContainerID).find('*[orderField]').click(function () {
        //remove arrow
        // $('.arrowUp').remove();
        // $('.arrowDown').remove();

        $('#' + listContainerID).find('*[orderField]').removeClass('sort-desc');
        $('#' + listContainerID).find('*[orderField]').removeClass('sort-asc');

        var orderType = 'asc';
        if (typeof (listCondition[listContainerID]['orderField']) != 'undefined') {
            orderType = listCondition[listContainerID]['orderType'];
        }
        listCondition[listContainerID]['orderField'] = $(this).attr('orderField');

        if (orderType == 'asc') {
            //orderType = 'desc';
            listCondition[listContainerID]['orderType'] = 'desc';
            // $(this).parent().append('<div class="arrowUp"></div>');
            // $(this).addClass('sorting_desc');
            $(this).addClass('sort-desc');
        } else {
            //orderType = 'asc';
            listCondition[listContainerID]['orderType'] = 'asc';
            // $(this).parent().append('<div class="arrowDown"></div>');

            $(this).addClass('sort-asc');
            // $(this).addClass('sorting_asc');
        }
        getList();

    });


}

// list functions--------------------------------------------------end---------------------------------------------------


function getUrl(a, c) {
    if (typeof (c) == 'undefined') {
        c = controllerName;
    }
    return baseUrl + '/' + c + '/' + a;
}


function getJsonString(containerID, className) {
    var searchObject = {};
    $('#' + containerID).find('.' + className).each(function () {
        searchObject[$(this).attr('name')] = $(this).val()
    });
    return JSON.stringify(searchObject);
}




function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function pinSymbol() {

    return {
        path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
        fillColor: getRandomColor(),
        fillOpacity: 1,
        strokeColor: '#000',
        strokeWeight: 1,
        scale: 1,
        labelOrigin: new google.maps.Point(0,-29)
    };
}